<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

use \uMVC\Router;
use \uDispatch\Route;

class RouterTest extends \PHPUnit_Framework_TestCase {
	/**
	 * @var \uMVC\Router
	 */
	private $router;

	/**
	 * Instantiate the router
	 */
	public function setUp () {
		$this->router = new Router(new Route());
	}

	/**
	 * If the pattern is missing from the config, an exception must be thrown
	 */
	public function testRejectsMissingPattern () {
		$config = [];
		$this->setExpectedException("uMVC\\Router\\Exception\\MissingPattern");
		$this->router->setConfig($config);
	}

	/**
	 * Correct configuration must be accepted
	 */
	public function testCorrectConfig () {
		$config = [
			"pattern" => "/",
			"defaults" => [
				"module" => "foo",
				"controller" => "bar",
				"action" => "baz"
			]
		];
		$this->router->setConfig($config);

		$this->assertTrue($this->router->setUri("/")->match());
		$params = $this->router->getParams();
		$this->assertArrayHasKey("module", $params);
		$this->assertEquals("foo", $params["module"]);
	}

	/**
	 * Incorrect routes need to be rejected; correct routes need to yield correct params
	 */
	public function testRoutes () {
		$config = [
			"pattern" => "/foo/{id}",
			"defaults" => [
				"module" => "foo",
				"controller" => "bar",
				"action" => "baz"
			],
			"reqs" => [
				"id" => "[1-9][0-9]*"
			]
		];
		$this->router->setConfig($config);

		$this->assertFalse($this->router->setUri("/bar")->match());
		$this->assertFalse($this->router->setUri("/foo")->match());
		$this->assertFalse($this->router->setUri("/foo/bar")->match());
		$this->assertFalse($this->router->setUri("/foo/123bar")->match());
		$this->assertFalse($this->router->setUri("/foo/123/bar/234/baz")->match());

		//$this->assertTrue($this->router->setUri("/foo/123")->match());
		$this->router->setUri("/foo/123")->match();

		$this->assertArrayHasKey("module", $this->router->getParams());

		$this->assertArrayHasKey("id", $this->router->getParams());
		$this->assertEquals("123", $this->router->getParams()["id"]);
	}

	/**
	 * The route needs to be assembled correctly
	 */
	public function testRouteAssembly () {
		$config = [
			"pattern" => "/foo/{id}",
			"defaults" => [
				"module" => "foo",
				"controller" => "bar",
				"action" => "baz"
			]
		];
		$this->router->setConfig($config);

		$this->assertEquals("/foo/123", $this->router->assemble(["id" => "123"]));
		$this->assertEquals("/foo/123?bar=baz", $this->router->assemble(["id" => "123", "bar" => "baz"]));

		$this->setExpectedException("uDispatch\\RouteException");
		$this->router->assemble([]);
	}
}
