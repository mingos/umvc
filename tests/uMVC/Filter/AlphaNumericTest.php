<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class AlphaNumericTest extends \PHPUnit_Framework_TestCase {
	/**
	 * @var \uMVC\Filter\AlphaNumeric
	 */
	private $filter;

	/**
	 * Instantiate the filter
	 */
	public function setUp () {
		$this->filter = new \uMVC\Filter\AlphaNumeric();
	}

	/**
	 * We must be able to set which characters are allowed
	 */
	public function testSetAllowedChars () {
		$this->filter->setAllowAlphabetic(true);
		$this->filter->setAllowNumeric(true);

		$this->assertEquals(true, $this->filter->getAllowAlphabetic());
		$this->assertEquals(true, $this->filter->getAllowNumeric());

		$this->filter->setAllowAlphabetic(false);
		$this->filter->setAllowNumeric(false);

		$this->assertEquals(false, $this->filter->getAllowAlphabetic());
		$this->assertEquals(false, $this->filter->getAllowNumeric());
	}

	/**
	 * Only numeric characters allowed
	 */
	public function testNumericOnly () {
		$this->filter->setAllowAlphabetic(false);
		$this->filter->setAllowNumeric(true);

		$this->assertEquals("008", $this->filter->filter("F00 8@R!"));
	}

	/**
	 * Only alphabetic characters allowed
	 */
	public function testAlphabeticOnly () {
		$this->filter->setAllowAlphabetic(true);
		$this->filter->setAllowNumeric(false);

		$this->assertEquals("FR", $this->filter->filter("F00 8@R!"));
	}

	/**
	 * Both alphabetic and numeric allowed
	 */
	public function testAlphabeticAndNumeric () {
		$this->filter->setAllowAlphabetic(true);
		$this->filter->setAllowNumeric(true);

		$this->assertEquals("F008R", $this->filter->filter("F00 8@R!"));
	}
}
