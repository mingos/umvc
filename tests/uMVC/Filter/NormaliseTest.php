<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class NormaliseTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \uMVC\Filter\Normalise
	 */
	private $filter;

	/**
	 * Input string 1
	 * @var string
	 */
	private $input1 = "I cut down trees, I wear high heels, suspenders and a bra.";

	/**
	 * Input string 2
	 * @var string
	 */
	private $input2 = "Kärlekens smärta i skuggornas skydd dväljas vid rosornas grav.";

	/**
	 * Instantiate the filter
	 */
	public function setUp()
	{
		$this->filter = new \uMVC\Filter\Normalise;
	}

	/**
	 * Default settings need to work fine
	 */
	public function testDefaultSettings()
	{
		$result = $this->filter->filter($this->input1);
		$this->assertEquals("i-cut-down-trees-i-wear-high-heels-suspenders-and-a-bra", $result);

		$result = $this->filter->filter($this->input2);
		$this->assertEquals("kaerlekens-smaerta-i-skuggornas-skydd-dvaeljas-vid-rosornas-grav", $result);
	}

	/**
	 * Nehaviour without case transformation
	 */
	public function testNoCaseTransformation()
	{
		$this->filter->transformCase(false);

		$result = $this->filter->filter($this->input1);
		$this->assertEquals("I-cut-down-trees-I-wear-high-heels-suspenders-and-a-bra", $result);

		$result = $this->filter->filter($this->input2);
		$this->assertEquals("Kaerlekens-smaerta-i-skuggornas-skydd-dvaeljas-vid-rosornas-grav", $result);
	}

	/**
	 * Behaviour without punctuation transformation
	 */
	public function testNoPunctuationTransformation()
	{
		$this->filter->transformPunctuation(false);

		$result = $this->filter->filter($this->input1);
		$this->assertEquals("i cut down trees, i wear high heels, suspenders and a bra.", $result);

		$result = $this->filter->filter($this->input2);
		$this->assertEquals("kaerlekens smaerta i skuggornas skydd dvaeljas vid rosornas grav.", $result);
	}

	/**
	 * Behaviour with custom character replacement rules
	 */
	public function testCustomReplacements()
	{
		$this->filter->setPunctuationReplacements([
			" " => "_"
		]);
		$this->filter->setTransliterationReplacements([
			"ä" => "a"
		]);

		$result = $this->filter->filter($this->input1);
		$this->assertEquals("i_cut_down_trees_i_wear_high_heels_suspenders_and_a_bra", $result);

		$result = $this->filter->filter($this->input2);
		$this->assertEquals("karlekens_smarta_i_skuggornas_skydd_dvaljas_vid_rosornas_grav", $result);
	}
}