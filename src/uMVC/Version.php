<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Project versioning, compliant with Semantic Versioning 2.0.0 (http://semver.org)
 *
 * @package Version
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
final class Version
{
	/**
	 * Major version of the project.
	 *
	 * Goes up with a full release cycle or other major milestone is achieved: first stable version release, a stable release after a code rewrite, etc.
	 */
	public static $major = "0";

	/**
	 * Minor version of the project.
	 *
	 * Minor versions go up whenever a new feature or major behaviour is implemented.
	 */
	public static $minor = "17";

	/**
	 * Patch version of the project.
	 *
	 * Patch versions go up when a patch or a bunch thereof is officially added to the project. Patches involve minor improvements and bugfixes.
	 */
	public static $patch = "0";

	/**
	 * Version type for pre-release versions.
	 *
	 * Types mark the release's status (alpha, beta, etc.)
	 */
	public static $preRelease  = "";

	/**
	 * Create a string from the version constants
	 *
	 * @return string Framework version
	 *
	 * @since 0.0.0-dev
	 */
	public static function get()
	{
		return self::$major
			. "." . self::$minor
			. "." . self::$patch
			. (self::$preRelease ? "-" . self::$preRelease : "");
	}
}
