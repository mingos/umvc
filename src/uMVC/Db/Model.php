<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Db;

/**
 * Base class for all  models communicating with a database table.
 *
 * Note that models that don't need a database connection can be created by simply
 * creating a class within a model namespace; it doesn't need to extend this class.
 *
 * @package Core
 * @subpackage Db
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
abstract class Model
{
	use \uMVC\Db;

	/**
	 * Default table name
	 * @var string
	 */
	protected $_table = null;

	/**
	 * Primary key column name
	 * @var string
	 */
	private $primaryKey = null;

	/**
	 * Table structure
	 * @var array
	 */
	private $tableStructure = null;

	/**
	 * Dependent tables
	 * @var array
	 */
	private $dependencies = [];

	/**
	 * Construct the model
	 *
	 * @since 0.15.0-dev
	 */
	final public function __construct()
	{
		$this->_init();
		if (null === $this->_table) {
			throw new \Exception("The model's table name must be defined.",500);
		}
		$this->tableStructure();
	}

	/**
	 * Initialise the model
	 *
	 * @since 0.15.0-dev
	 */
	protected function _init()
	{}

	/**
	 * Set the table structure and set the primary key if needed.
	 *
	 * This method is run when the data map is used for the first time. It will launch a DESCRIBE query to detect the
	 * table's structure. The table structure is cached, so to speed the method up, it's highly recommended to set up
	 * fast caching, such as APC or Memcache(d).
	 *
	 * @since 0.5.2-dev
	 */
	private function tableStructure()
	{
		$metadata = \uMVC\Cache::getInstance()->fetchMetadata($this->_table);
		if (false === $metadata) {
			$metadata = $this->query("DESCRIBE `{$this->_table}`")->fetchAll();
			\uMVC\Cache::getInstance()->storeMetadata($this->_table, $metadata);
		}
		foreach ($metadata as $item) {
			if ($item['Key'] == 'PRI') {
				$this->primaryKey = $item['Field'];
				break;
			}
		}
		$this->tableStructure = $metadata;
	}

	/**
	 * Fetch a record from the database that has the primary key value specified in the parametre.
	 *
	 * @param mixed $id The primary key value
	 *
	 * @return boolean Whether the row has been found
	 *
	 * @since 0.5.0-dev
	 */
	public function find($id)
	{
		$result = $this->query(
			"SELECT * FROM `{$this->_table}` WHERE `{$this->primaryKey}` = :id",
			['id' => $id]
		)->fetch();

		if (!empty($result)) {
			foreach ($result as $column => $value) {
				$this->$column = $value;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fetch a record from the database using an arbitrary WHERE clause.
	 *
	 * @param mixed $where The where clause. If it is a string, it will be used verbatim
	 * (bound parametres can be specified as the second argument). If it is an array, its
	 * keys will be treated as field names and values as field values. If a value is an array,
	 * it will have an IN operator, otherwise the equality operator will be used.
	 * @param array $bind [optional] Bound parametres in case the first parametre is a string
	 *
	 * @return boolean Whether the row has been found
	 *
	 * @since 0.16-0.dev
	 */
	public function findWhere($where, $bind = [])
	{
		if (is_string($where)) {
			$result = $this->query(
				"SELECT * FROM `{$this->_table}` WHERE {$where}",
				$bind
			)->fetch();
		} else if (is_array($where)) {
			$sql = "SELECT * FROM `{$this->_table}` WHERE";
			$wheres = [];
			foreach ($where as $column => $value) {
				$quoted = $this->quote($value);
				if (is_array($value)) {
					$wheres[] = "`{$column}` IN ({$quoted})";
				} else {
					$wheres[] = "`{$column}` = {$quoted}";
				}
			}
			$wheres = implode(" AND ", $wheres);
			$result = $this->query(
				"SELECT * FROM `{$this->_table}` WHERE {$wheres}"
			)->fetch();
		}

		if (!empty($result)) {
			foreach ($result as $column => $value) {
				$this->$column = $value;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save the database record's values to the database
	 *
	 * @since 0.5.0-dev
	 */
	public function save()
	{
		// do an insert or update
		if (!isset($this->{$this->primaryKey}) || empty($this->{$this->primaryKey})) {
			$fields = [];
			$columns = [];
			$values = [];
			foreach ($this->tableStructure as $field) {
				if ($field['Key'] == 'PRI') {
					continue;
				}

				$columns[] = "`{$field['Field']}`";
				if (!isset($this->{$field['Field']})) {
					if ($field['Default'] == null) { // no specified default value
						if ($field['Null'] == 'YES') { // if null is allowed
							$values[] = new \uMVC\Db\Expr("NULL");
						} else { // null not allowed
							if (preg_match('/(^varchar|text)/',$field['Type'])) { // text field
								$values[] = "''";
							} else { // numeric field
								$values[] = '0';
							}
						}
					} else if (preg_match('/^CURRENT_TIMESTAMP/',$field['Default'])) {
						$values[] = "{$field['Default']}";
					} else {
						if (strlen($field['Default']) > 0) {
							$values[] = "'{$field['Default']}'";
						} else {
							if (preg_match('/(^varchar|text)/',$field['Type'])) { // text field
								$values[] = "''";
							} else { // numeric field
								$values[] = '0';
							}
						}
					}
				} else if ($this->{$field['Field']} instanceof \uMVC\Db\Expr) {
					$values[] = strval($this->{$field['Field']});
				} else {
					$values[] = ":{$field['Field']}";
					$fields[$field['Field']] = $this->{$field['Field']};
				}
			}

			$columns = implode(', ',$columns);
			$values = implode(', ',$values);

			$this->query("INSERT INTO `{$this->_table}` ({$columns}) VALUES ({$values})",$fields);
			if ($this->primaryKey != null) {
				$this->{$this->primaryKey} = $this->lastInsertId();
			}
		} else {
			$pairs = [];
			$fields = [];
			foreach ($this->tableStructure as $field) {
				if ($field['Key'] == 'PRI') {
					continue;
				}

				if ($this->{$field['Field']} instanceof \uMVC\Db\Expr) {
					$pairs[] = "`{$field['Field']}` = ".$this->{$field['Field']};
				} else {
					$pairs[] = "`{$field['Field']}` = :".$field['Field'];
					$fields[$field['Field']] = $this->{$field['Field']};
				}
			}

			$pairs = implode(', ',$pairs);

			$this->query("UPDATE `{$this->_table}` SET {$pairs} WHERE `{$this->primaryKey}` = :{$this->primaryKey}",$fields + [$this->primaryKey => $this->{$this->primaryKey}]);
		}
	}

	/**
	 * Convert the mapped columns to an array of column-value pairs
	 *
	 * @return array
	 *
	 * @since 0.5.0-dev
	 */
	public function toArray()
	{
		$return = [];
		foreach ($this->tableStructure as $field) {
			$return[$field['Field']] = $this->{$field['Field']};
		}

		return $return;
	}

	/**
	 * Set the column values
	 *
	 * @param array $array Input array of column-value pairs
	 *
	 * @since 0.5.0-dev
	 */
	public function fromArray($array)
	{
		foreach ($this->tableStructure as $field) {
			if (array_key_exists($field['Field'], $array)) {
				$this->{$field['Field']} = $array[$field['Field']];
			}
		}
	}

	/**
	 * Add a one-to-many relation description.
	 *
	 * @param string $tableName The name of the dependent table
	 * @param string $fkColumn The name of the column in the dependent table that is used to join the data
	 * @param null|string $reference The name of the original table's column that's used to join the data (omitting the
	 * value will result in using the table's primary key).
	 *
	 * @since 0.15.0-dev
	 */
	public function addDependencyOneToMany($tableName, $fkColumn, $reference = null)
	{
		$this->dependencies[$tableName] = [
			"type" => "oneToMany",
			"dependency" => [
				"table" => $tableName,
				"column" => $fkColumn,
				"reference" => $reference
			]
		];
	}

	/**
	 * Add a many-to-many relation description
	 *
	 * @param string $depTableName The name of the dependent table
	 * @param string $depFkColumn The name of the column in the dependent table that is used to join the data with the
	 * relations table
	 * @param string $depReference The name of the column in the relations table used to join with the dependent table
	 * @param string $viaTableName The name of the relations table (the one cointaining the IDs of both the source table
	 * and the dependent table)
	 * @param string $viaFkColumn The name of the relations table's column used to join with the source table
	 * @param null|string $viaReference The name of the original table's column that's used to join the data with the
	 * relations table (omitting the value will result in using the table's primary key).
	 *
	 * @since 0.15.0-dev
	 */
	public function addDependencyManyToMany($depTableName, $depFkColumn, $depReference, $viaTableName, $viaFkColumn, $viaReference = null)
	{
		$this->dependencies[$depTableName] = [
			"type" => "manyToMany",
			"dependency" => [
				"table" => $depTableName,
				"column" => $depFkColumn,
				"reference" => $depReference
			],
			"via" => [
				"table" => $viaTableName,
				"column" => $viaFkColumn,
				"reference" => $viaReference
			]
		];
	}

	/**
	 * Create a Select object containing the query to select dependent rows of the map's default table.
	 *
	 * @param integer $table Dependent table's name
	 *
	 * @return \uMVC\Db\Select The select object
	 *
	 * @throws \Exception if the dependent table hasn't been configured
	 *
	 * @since 0.15.0-dev
	 */
	public function dependentSelect($table)
	{
		if (array_key_exists($table,$this->dependencies)) {
			$dependency = $this->dependencies[$table];
		} else {
			throw new \Exception("Table `{$table}` hasn't been configured as a dependency of `{$this->_table}`.",500);
		}

		$select = $this
			->select()
			->from($dependency["dependency"]["table"])
		;

		switch ($dependency["type"]) {
			case "oneToMany":
				$col = $dependency["dependency"]["column"];
				$ref = $dependency["dependency"]["reference"];
				if (null === $ref) {
					$ref = $this->primaryKey;
				}
				$select->where("`{$col}` = ?", $this->{$ref});
				break;
			case "manyToMany":
				$tbl = $dependency["dependency"]["table"];
				$col = $dependency["dependency"]["column"];
				$ref = $dependency["dependency"]["reference"];
				$vTbl = $dependency["via"]["table"];
				$vCol = $dependency["via"]["column"];
				$vRef = $dependency["via"]["reference"];
				if (null === $ref) {
					$ref = $this->primaryKey;
				}
				$select->join("{$vTbl}","`{$tbl}`.`{$col}` = `{$vTbl}`.`{$ref}`",[]);
				$select->where("`{$vTbl}`.`{$vCol}` = ?", $this->{$vRef});
				break;
		}

		return $select;
	}

	/**
	 *
	 *
	 * @return \uMVC\Db\Select
	 *
	 * @since 0.12.0-dev
	 */
	public function select()
	{
		return new \uMVC\Db\Select($this->_table);
	}

	/**
	 * Fetch all entries from the database table
	 *
	 * @param string $where [optional] Where clause to the select
	 * @param array $bind [optional] Bound SQL parametres
	 *
	 * @return array
	 *
	 * @throws \Exception if the default table name is not set
	 */
	public function selectRows($where = "", $bind = [])
	{
		if (null === $this->_table) {
			throw new \Exception("No table name specified!", 500);
		}

		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($where)) {
			$sql .= " WHERE {$where}";
		}

		return $this->query($sql,$bind)->fetchAll();
	}

	/**
	 * Fetch a single record from the database table
	 *
	 * @param string $where [optional] Where clause to the select
	 * @param array $bind [optional] Bound SQL parametres
	 *
	 * @return array
	 *
	 * @throws \Exception if the default table name is not set
	 */
	public function selectRow($where = "", $bind = [])
	{
		if (null === $this->_table) {
			throw new \Exception("No table name specified!", 500);
		}

		$sql = "SELECT * FROM `{$this->_table}`";
		if (!empty($where)) {
			$sql .= " WHERE {$where}";
		}

		return $this->query($sql,$bind)->fetch();
	}

	/**
	 * Insert a new row into the table.
	 *
	 * @param array $params Column-value pairs. Everything is internally quoted.
	 *
	 * @return int Number of affected rows
	 *
	 * @throws \Exception if the default table name is not set
	 *
	 * @since 0.0.0-dev
	 */
	public function insert($params)
	{
		if (null === $this->_table) {
			throw new \Exception("No table name specified!", 500);
		}

		$keys   = [];
		$values = [];

		foreach ($params as $key => $value) {
			$k        = $this->quote($key);
			$k        = "`" . trim($k, "\"'") . "`";
			$keys[]   = $k;
			$values[] = $this->quote($value);
		}

		$keys   = implode(", ", $keys);
		$values = implode(", ", $values);

		$sql = $this->query("INSERT INTO `{$this->_table}` ({$keys}) VALUES ({$values})");

		return $sql->rowCount();
	}

	/**
	 * Update the table.
	 *
	 * @param array  $params Column-value pairs. Everything is internally quoted.
	 * @param string $where  The where statement
	 * @param array  $bind   [optional] Bound SQL parametres
	 *
	 * @return int The number of affected rows
	 *
	 * @throws \Exception if the default table name is not set
	 *
	 * @since 0.0.0-dev
	 */
	public function update($params, $where, $bind = [])
	{
		if (null === $this->_table) {
			throw new \Exception("No table name specified!", 500);
		}

		$values = [];

		foreach ($params as $key => $value) {
			$k        = $this->quote($key);
			$k        = "`" . trim($k, "\"'") . "`";
			$v        = $this->quote($value);
			$values[] = "{$k} = {$v}";
		}

		$values = implode(", ", $values);

		$sql = $this->query("UPDATE `{$this->_table}` SET {$values} WHERE {$where}",$bind);

		return $sql->rowCount();
	}

	/**
	 * Delete rows from the table.
	 *
	 * @param string $where Where statement
	 * @param array  $bind  [optional] Bound SQL parametres
	 *
	 * @return int Number of affected rows
	 *
	 * @throws \Exception if the default table name is not set
	 *
	 * @since 0.0.0-dev
	 */
	public function delete($where, $bind = [])
	{
		if (null === $this->_table) {
			throw new \Exception("No table name specified!", 500);
		}

		$sql = $this->query("DELETE FROM `{$this->_table}` WHERE {$where}",$bind);

		return $sql->rowCount();
	}
}
