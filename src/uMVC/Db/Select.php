<?php
	/* uMVC
	 * Copyright (c) 2012-2013 Dominik Marczuk
	 * All rights reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without
	 * modification, are permitted provided that the following conditions are met:
	 *     * Redistributions of source code must retain the above copyright
	 *       notice, this list of conditions and the following disclaimer.
	 *     * Redistributions in binary form must reproduce the above copyright
	 *       notice, this list of conditions and the following disclaimer in the
	 *       documentation and/or other materials provided with the distribution.
	 *     * The name of Dominik Marczuk may not be used to endorse or promote products
	 *       derived from this software without specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
	 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
	 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	 */
namespace uMVC\Db;

/**
 * Generation of SELECT queries using a series of function calls.
 *
 * @package Core
 * @package Db
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.12.0-dev
 */
class Select
{
	use \uMVC\Db;

	/**#@+
	 * Query index name constants
	 */
	const DISTINCT = "distinct";
	const COLUMNS = "cols";
	const JOINCOLUMNS = "joincols";
	const FROM = "from";
	const JOIN = "join";
	const WHERE = "where";
	const LIMIT = "limit";
	const GROUP = "group";
	const HAVING = "having";
	const ORDER = "order";
	/**#@-*/

	/**
	 * All the query parts, from which the final query will be built
	 * @var array
	 */
	private $query;

	/**
	 * Construct the Select connection object
	 *
	 * @param string $tableName Table name to use in the FROM part
	 *
	 * @since 0.12.0-dev
	 */
	public function __construct($tableName = "")
	{
		$this->query = [
			self::DISTINCT => false,
			self::COLUMNS => [],
			self::JOINCOLUMNS => [],
			self::FROM => [$tableName],
			self::JOIN => [],
			self::WHERE => [],
			self::LIMIT => "",
			self::GROUP => "",
			self::HAVING => [],
			self::ORDER => []
		];
	}

	/**
	 * Set to use the DISTINCT clause or not
	 *
	 * @param boolean $flag [optional] Whether to use the DISTINCT clause
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function distinct($flag = true)
	{
		$this->query[self::DISTINCT] = $flag;
		return $this;
	}

	/**
	 * Set the FROM table name
	 *
	 * @param $table
	 * @param array $columns
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function from($table, $columns = [])
	{
		if (!is_array($table)) {
			$this->query[self::FROM] = [$table];
		} else {
			$this->query[self::FROM] = $table;
		}


		if (!empty($columns)) {
			if (!is_array($columns)) {
				$columns = [$columns];
			}
			$this->query[self::COLUMNS] = array_merge($this->query[self::COLUMNS], $columns);
		}

		return $this;
	}

	/**
	 * Add an INNER JOIN clause
	 *
	 * @param string|array $table Joined table, either as a string or array with the alias as the index
	 * @param string $condition The ON condition
	 * @param array|null $columns [optional] Array of columns to fetch; omit to fetch all
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function join($table, $condition, $columns = null)
	{
		$this->_join("INNER JOIN", $table, $condition, $columns);
		return $this;
	}

	/**
	 * Add an LEFT JOIN clause
	 *
	 * @param string|array $table Joined table, either as a string or array with the alias as the index
	 * @param string $condition The ON condition
	 * @param array|null $columns [optional] Array of columns to fetch; omit to fetch all
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function joinLeft($table, $condition, $columns = null)
	{
		$this->_join("LEFT JOIN", $table, $condition, $columns);
		return $this;
	}

	/**
	 * Add an RIGHT JOIN clause
	 *
	 * @param string|array $table Joined table, either as a string or array with the alias as the index
	 * @param string $condition The ON condition
	 * @param array|null $columns [optional] Array of columns to fetch; omit to fetch all
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function joinRight($table, $condition, $columns = null)
	{
		$this->_join("RIGHT JOIN", $table, $condition, $columns);
		return $this;
	}

	/**
	 * Create a join clause
	 *
	 * @param string $type join type ("INNER JOIN"|"LEFT JOIN"|"RIGHT JOIN")
	 * @param string|array $table Table name with optional alias
	 * @param string $condition the joining condition
	 * @param array $columns [optional] array of columns that should be fetched from the joined table
	 *
	 * @since 0.12.0-dev
	 */
	private function _join($type, $table, $condition, $columns = null)
	{
		$join = [
			"type" => $type
		];

		// joined table name
		if (!is_array($table)) {
			$join["table"] = [$table];
		} else {
			$join["table"] = $table;

		}

		// join condition
		$join["on"] = $condition;

		// table name/alias
		$alias = key($join["table"]);
		if (!is_string($alias)) {
			$alias = $join["table"][$alias];
		}

		// fetched columns
		if (is_null($columns)) {
			$this->query[self::JOINCOLUMNS] = array_merge($this->query[self::JOINCOLUMNS], ["`".trim($alias,"`")."`.*"]);
		} else {
			if (!is_array($columns)) {
				$columns = [$columns];
			}

			// @todo fix the `table`.`expr` behaviour. Ideally, merge this block and the FROM tables tratment.
			foreach ($columns as $key => $val) {
				if (!is_string($key)) {
					if ($val instanceof \uMVC\Db\Expr) {
						$this->query[self::JOINCOLUMNS] = array_merge($this->query[self::JOINCOLUMNS], [$val]);
					} else {
						$this->query[self::JOINCOLUMNS] = array_merge($this->query[self::JOINCOLUMNS], ["`".trim($alias,"`")."`.`".trim($val,"`")."`"]);
					}
				} else {
					if ($val instanceof \uMVC\Db\Expr) {
						$this->query[self::JOINCOLUMNS] = array_merge($this->query[self::JOINCOLUMNS], ["{$val} AS `".trim($key,"`")."`"]);
					} else {
						$this->query[self::JOINCOLUMNS] = array_merge($this->query[self::JOINCOLUMNS], ["`".trim($alias,"`")."`.`".trim($val,"`")."` AS `".trim($key,"`")."`"]);
					}
				}
			}
		}

		$this->query[self::JOIN][] = $join;
	}

	/**
	 * Set the GROUP BY clause
	 *
	 * @param string $column Column name to group by
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function group($column)
	{
		$this->query[self::GROUP] = strval($column);
		return $this;
	}

	/**
	 * Add a HAVING condition (AND)
	 *
	 * @param string $condition The condition
	 * @param mixed $value [optional] Value to be quoted
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function having($condition, $value = null)
	{
		$condition = str_replace("?", $this->quote($value), $condition);
		$this->query[self::HAVING][] = [
			"type" => "AND",
			"condition" => $condition
		];

		return $this;
	}

	/**
	 * Add a HAVING condition (OR)
	 *
	 * @param string $condition The condition
	 * @param mixed $value [optional] Value to be quoted
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function orHaving($condition, $value = null)
	{
		$condition = str_replace("?", $this->quote($value), $condition);
		$this->query[self::HAVING][] = [
			"type" => "OR",
			"condition" => $condition
		];

		return $this;
	}

	/**
	 * Add a WHERE condition (AND)
	 *
	 * @param string $condition The condition
	 * @param mixed $value [optional] Value to be quoted
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function where($condition, $value = null)
	{
		$condition = str_replace("?", $this->quote($value), $condition);
		$this->query[self::WHERE][] = [
			"type" => "AND",
			"condition" => $condition
		];

		return $this;
	}

	/**
	 * Add a WHERE condition (OR)
	 *
	 * @param string $condition The condition
	 * @param mixed $value [optional] Value to be quoted
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function orWhere($condition, $value = null)
	{
		$condition = str_replace("?", $this->quote($value), $condition);
		$this->query[self::WHERE][] = [
			"type" => "OR",
			"condition" => $condition
		];

		return $this;
	}

	/**
	 * Add an ORDER BY clause
	 *
	 * @param string $condition the complete clause excluding the ORDER BY string
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function order($condition)
	{
		$this->query[self::ORDER][] = $condition;
		return $this;
	}

	/**
	 * Set a limit clause
	 *
	 * @param integer $limit Number of _items to be fetched
	 * @param integer $offset [optional] Offset
	 *
	 * @return \uMVC\Db\Select provides a fluent interface
	 *
	 * @since 0.12.0-dev
	 */
	public function limit($limit, $offset = 0)
	{
		$this->query[self::LIMIT] = "{$offset},{$limit}";
		return $this;
	}

	/**
	 * Output the query as a string
	 *
	 * @param string $indent Additional indentation when creating the SQL string
	 *
	 * @throws \Exception if the FROM table is missing
	 *
	 * @return string
	 *
	 * @since 0.12.0-dev
	 */
	public function getQuery($indent = "")
	{
		// build the from clause
		if (!empty($this->query[self::FROM])) {
			$key = key($this->query[self::FROM]);
			$tableName = trim($this->query[self::FROM][$key]);
			$from = "FROM\n{$indent}\t"."`".trim($this->query[self::FROM][$key],"`")."`";
			if (is_string($key)) {
				$from .= " AS `".trim($key,"`")."`";
				$tableName = trim($key,"`");
			}
		} else {
			throw new \Exception("The FROM clause contains no table name.",500);
		}

		// build the columns:
		$columns = [];

		// get table name
		$table = key($this->query[self::FROM]);
		if (!is_string($table)) {
			$table = $this->query[self::FROM][$table];
		}

		if (empty($this->query[self::COLUMNS])) {
			$columns[] = "`".trim($table,"`")."`.*";
		} else {
			foreach ($this->query[self::COLUMNS] as $alias => $column) {
				if ($column instanceof \uMVC\Db\Select) {
					$column = "(".$column.")";
				} else if ($column instanceof \uMVC\Db\Expr || strstr($column,"(")) {
					$column = strval($column);
					if ($column == "*") {
						$column = "`{$tableName}`.*";
					}
				} else if ($column == "*") {
					$column = "`{$table}`.*";
				} else {
					$column = "`{$table}`.`".trim($column,"`")."`";
				}

				if (is_string($alias)) {
					$columns[] = "{$column} AS `".trim($alias,"`")."`";
				} else {
					$columns[] = $column;
				}
			}
		}
		$columns = array_merge($columns,$this->query[self::JOINCOLUMNS]);

		// distinct
		$distinct = !empty($this->query[self::DISTINCT]) ? " DISTINCT" : "";

		$query[] = "SELECT{$distinct}\n{$indent}\t".implode(",\n{$indent}\t",$columns);
		$query[] = $from;

		// build the joins
		if (!empty($this->query[self::JOIN])) {
			$joins = [];
			foreach ($this->query[self::JOIN] as $item) {
				$key = key($item["table"]);
				$join = "{$item["type"]}\n{$indent}\t";
				if ($item["table"][$key] instanceof \uMVC\Db\Expr) {
					$join .= $item["table"][$key];
				} else if ($item["table"][$key] instanceof \uMVC\Db\Select) {
					$join .= "(".$item["table"][$key]->getQuery("{$indent}\t").")";
				} else {
					$join .= "`".trim($item["table"][$key],"`")."`";
				}
				if (is_string($key)) {
					$join .= " AS `".trim($key,"`")."`";
				}

				$join .= "\n{$indent}\tON {$item["on"]}";

				$joins[] = $join;
			}
			$query[] = implode("\n{$indent}",$joins);
		}

		// build the where clause
		if (!empty($this->query[self::WHERE])) {
			$where = [];
			foreach ($this->query[self::WHERE] as $item) {
				$type = $item["type"];
				$condition = $item["condition"];

				if ($type == "OR") {
					if (!empty($where)) {
						$where[] = "OR ({$condition})";
					} else {
						$where[] = "({$condition})";
					}
				} else {
					if (!empty($where)) {
						$where[] = "AND ({$condition})";
					} else {
						$where[] = "({$condition})";
					}
				}
			}
			$query[] = "WHERE\n{$indent}\t".implode("\n{$indent}\t",$where);
		}

		// add the group by statement, if applicable
		if (!empty($this->query[self::GROUP])) {
			$query[] = "GROUP BY\n{$indent}\t{$this->query[self::GROUP]}";
		}

		// build the having clause
		if (!empty($this->query[self::HAVING])) {
			$where = [];
			foreach ($this->query[self::HAVING] as $item) {
				$type = $item["type"];
				$condition = $item["condition"];

				if ($type == "OR") {
					if (!empty($where)) {
						$where[] = "OR ({$condition})";
					} else {
						$where[] = "({$condition})";
					}
				} else {
					if (!empty($where)) {
						$where[] = "AND ({$condition})";
					} else {
						$where[] = "({$condition})";
					}
				}
			}
			$query[] = "HAVING\n{$indent}\t".implode("\n{$indent}\t",$where);
		}

		// order
		if (!empty($this->query[self::ORDER])) {
			$query[] = "ORDER BY\n{$indent}\t".implode(",\n{$indent}\t",$this->query[self::ORDER]);
		}

		// limit
		if (!empty($this->query[self::LIMIT])) {
			$query[] = "LIMIT\n{$indent}\t{$this->query[self::LIMIT]}";
		}

		return implode("\n{$indent}",$query)."\n";
	}

	/**
	 * Output the query as a string
	 *
	 * @return string
	 *
	 * @since 0.12.0-dev
	 */
	public function __toString()
	{
		return $this->getQuery();
	}
}
