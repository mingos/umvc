<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Base class for all validators
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
abstract class Validator
{
	/**
	 * The code for a valid value.
	 * @var integer
	 */
	const VALID = 0;

	/**
	 * The error string
	 * @var string
	 */
	protected $_error = null;

	/**
	 * Message templates
	 * @var array
	 */
	protected $_messages = [];

	/**
	 * Whether the validator should break the validator chain upon failing.
	 * @var boolean
	 */
	private $breakOnFailure = true;

	/**
	 * The constructor
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct()
	{
		$this->_init();
	}

	/**
	 * Initialise the validator variables
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{}

	/**
	 * Check the validity of a value and set the error message.
	 *
	 * @param mixed $value The value to be validated
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	abstract public function isValid($value);

	/**
	 * Set the error message
	 *
	 * @param string $error The error message
	 *
	 * @return \uMVC\Validator Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setError($error)
	{
		$this->_error = $error;
		return $this;
	}

	/**
	 * Fetch the error message
	 *
	 * @return string The error message (or null if no message is set).
	 *
	 * @since 0.0.0-dev
	 */
	public function getError()
	{
		return $this->_error;
	}

	/**
	 * Set a message template
	 *
	 * @param string $index   The message template index
	 * @param string $message The new message template
	 *
	 * @return \uMVC\Validator Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setMessage($index, $message)
	{
		$this->_messages[$index] = strval($message);
		return $this;
	}

	/**
	 * Defines whether the validator allows the other validator that are chained after it to validate the value should it fail.
	 *
	 * @param boolean $flag true or omit the value to break the validator chain; false to let other validators run despite a failure. True is the preset default.
	 *
	 * @return \uMVC\Validator Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setBreakOnFailure($flag = true)
	{
		$this->breakOnFailure = (boolean)$flag;
		return $this;
	}

	/**
	 * Checks whether the validator chain should be broken on failure
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function getBreakOnFailure()
	{
		return $this->breakOnFailure;
	}
}
