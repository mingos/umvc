<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Xml\Item;

/**
 * An item inside an RSS feed
 *
 * @package Xml
 * @subpackage Item
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class Rss extends \uMVC\Xml\Item {
	/**
	 * The title of the item
	 * @var string
	 */
	private $title;

	/**
	 * The URL of the item
	 * @var string
	 */
	private $link;

	/**
	 * The item synopsis
	 * @var string
	 */
	private $description;

	/**
	 * Email address of the author of the item
	 * @var string
	 */
	private $author;

	/**
	 * Indicates when the item was published
	 * @var \DateTime
	 */
	private $pubDate;

	/**
	 * Construct the object
	 *
	 * @param string $title The title of the item
	 * @param string $link The URL of the item
	 * @param string $description The item synopsis
	 *
	 * @since 0.2.0-dev
	 */
	public function __construct($title = '', $link = '', $description = '')
	{
		$this->title = $title;
		$this->link = $link;
		$this->description = $description;
		$this->author = null;
		$this->pubDate = null;
	}

	/**
	 * Render the item's XML
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function render()
	{
		$result = "\t<item>\n";
		$result .= "\t\t<title><![CDATA[{$this->title}]]></title>\n";
		$result .= "\t\t<link>{$this->link}</link>\n";
		$result .= "\t\t<description><![CDATA[{$this->description}]]></description>\n";
		if (null !== $this->author) {
			$result .= "\t\t<author>{$this->author}</author>\n";
		}
		if (null !== $this->pubDate) {
			$result .= "\t\t<pubDate>".$this->pubDate->format(\DateTime::RFC822)."</pubDate>\n";
		}
		$result .= "\t</item>\n";

		return $result;
	}

	/**
	 * Get the title
	 *
	 * @return string The title of the item
	 *
	 * @since 0.2.0-dev
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set the title
	 *
	 * @param string $title The title of the item
	 *
	 * @return \uMVC\Xml\Item\Rss Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * Fetch the link
	 *
	 * @return string The URL of the item
	 *
	 * @since 0.2.0-dev
	 */
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * Set the link
	 *
	 * @param string $link The URL of the item
	 *
	 * @return \uMVC\Xml\Item\Rss Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function setLink($link)
	{
		$this->link = $link;
		return $this;
	}

	/**
	 * Get the description
	 *
	 * @return string The item synopsis
	 *
	 * @since 0.2.0-dev
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set the description
	 *
	 * @param string $description The item synopsis
	 *
	 * @return \uMVC\Xml\Item\Rss Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * Fetch the item's author
	 *
	 * @return null|string Email address of the author of the item or null if the author is not set.
	 *
	 * @since 0.2.0-dev
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * Set the item's author
	 *
	 * @param string $author Email address of the author of the item
	 *
	 * @return \uMVC\Xml\Item\Rss Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
		return $this;
	}

	/**
	 * Fetch the item's published date
	 *
	 * @return \DateTime
	 *
	 * @since 0.3.2-dev
	 */
	public function getPubDate()
	{
		return $this->pubDate;
	}

	/**
	 * Set the item's published date
	 *
	 * @param \DateTime $pubDate
	 *
	 * @return \uMVC\Xml\Item\Rss Provides a fluent interface
	 *
	 * @since 0.3.2-dev
	 */
	public function setPubDate($pubDate)
	{
		if ($pubDate instanceof \DateTime) {
			$this->pubDate = $pubDate;
		} else {
			$this->pubDate = new \DateTime($pubDate);
		}
		return $this;
	}
}
