<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Xml\Item;

/**
 * A URL inside a sitemap
 *
 * @package Xml
 * @subpackage Item
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class Sitemap extends \uMVC\Xml\Item {
	/**
	 * The location (URL) of the item
	 * @var string
	 */
	private $loc;

	/**
	 * Last modification date
	 * @var \Datetime
	 */
	private $lastmod;

	/**
	 * Change frequency
	 * @var string
	 */
	private $changefreq;

	/**
	 * Priority
	 * @var string
	 */
	private $priority;

	/**#@+
	 * Sitemap item change frequency accepted values
	 * @var string
	 */
	const CHANGEFREQ_ALWAYS = "always";
	const CHANGEFREQ_HOURLY = "hourly";
	const CHANGEFREQ_DAILY = "daily";
	const CHANGEFREQ_WEEKLY = "weekly";
	const CHANGEFREQ_MONTHLY = "monthly";
	const CHANGEFREQ_YEARLY = "yearly";
	const CHANGEFREQ_NEVER = "never";
	/**#@-*/

	/**#@+
	 * Sitemap item priority accepted values
	 * @var string
	 */
	const PRIORITY_00 = "0.0";
	const PRIORITY_01 = "0.1";
	const PRIORITY_02 = "0.2";
	const PRIORITY_03 = "0.3";
	const PRIORITY_04 = "0.4";
	const PRIORITY_05 = "0.5";
	const PRIORITY_06 = "0.6";
	const PRIORITY_07 = "0.7";
	const PRIORITY_08 = "0.8";
	const PRIORITY_09 = "0.9";
	const PRIORITY_10 = "1.0";
	/**#@-*/

	/**
	 * Construct the object
	 *
	 * @param string    $loc        [optional] The item's URL
	 * @param \Datetime $lastmod    [optional] Last modified date
	 * @param string    $changefreq [optional] Change frequency
	 * @param string    $priority   [optional] Item priority
	 *
	 * @since 0.6.0-dev
	 */
	public function __construct($loc = null, $lastmod = null, $changefreq = null, $priority = null)
	{
		$this->setLoc($loc);
		$this->setLastmod($lastmod);
		$this->setChangefreq($changefreq);
		$this->setPriority($priority);
	}

	/**
	 * Render the item's XML
	 *
	 * @return string
	 *
	 * @since 0.6.0-dev
	 */
	public function render()
	{
		if (null === $this->loc) {
			throw new \Exception(__METHOD__.": Location is required.",500);
		}

		$result = "\t\t<loc>{$this->loc}</loc>\n";
		if (null !== $this->lastmod) {
			$lastmod = $this->lastmod->format(\Datetime::W3C);
			$result .= "\t\t<lastmod>{$lastmod}</lastmod>\n";
		}
		if (null !== $this->changefreq) {
			$result .= "\t\t<changefreq>{$this->changefreq}</changefreq>\n";
		}
		if (null !== $this->priority) {
			$result .= "\t\t<priority>{$this->priority}</priority>\n";
		}
		$result = "\t<url>\n{$result}\t</url>\n";

		return $result;
	}

	/**
	 * Fetch the item's URL
	 *
	 * @return string|null The item's URL or null if it hasn't been set
	 *
	 * @since 0.6.0-dev
	 */
	public function getLoc()
	{
		return $this->loc;
	}

	/**
	 * Set the item's URL
	 *
	 * @param string $loc Item's URL
	 *
	 * @return \uMVC\Xml\Item\Sitemap Provides a fluent interface
	 *
	 * @since 0.6.0-dev
	 */
	public function setLoc($loc)
	{
		$this->loc = strval($loc);
		return $this;
	}

	/**
	 * Fetch the item's last modification date
	 *
	 * @return \Datetime|null The Datetime with the last modification date or null if the date was not set
	 *
	 * @since 0.6.0-dev
	 */
	public function getLastmod()
	{
		return $this->lastmod;
	}

	/**
	 * Set the last modification date
	 *
	 * @param \Datetime|string|null $lastmod Last modification date.<br>
	 * It should be either a DateTime object, a string that can be understood by DateTime's constructor.<br>
	 * If null is passed, the last modification will be removed.
	 *
	 * @return \uMVC\Xml\Item\Sitemap Provides a fluent interface
	 *
	 * @throws \Exception if the input format is incorrect
	 *
	 * @since 0.6.0-dev
	 */
	public function setLastmod($lastmod)
	{
		if ($lastmod instanceof \DateTime) {
			$this->lastmod = $lastmod;
		} else if (is_string($lastmod)) {
			$this->lastmod = new \DateTime($lastmod);
		} else if (null === $lastmod) {
			$this->lastmod = $lastmod;
		} else {
			throw new \Exception(__METHOD__.": unexpected input format.",500);
		}

		return $this;
	}

	/**
	 * Fetch the change frequency.
	 *
	 * @return string|null The change frequency or null if it is not set
	 *
	 * @since 0.6.0-dev
	 */
	public function getChangefreq()
	{
		return $this->changefreq;
	}

	/**
	 * Set the change frequency.
	 *
	 * @param string|null $changefreq Either the correct change frequency or null if it should not be displayed.
	 *
	 * @return \uMVC\Xml\Item\Sitemap Provides a fluent interface
	 *
	 * @throws \Exception if the input value is incorrect
	 *
	 * @since 0.6.0-dev
	 */
	public function setChangefreq($changefreq)
	{
		if (null === $changefreq) {
			$this->changefreq = $changefreq;
		} else if (in_array($changefreq, [
			self::CHANGEFREQ_ALWAYS,
			self::CHANGEFREQ_DAILY,
			self::CHANGEFREQ_HOURLY,
			self::CHANGEFREQ_MONTHLY,
			self::CHANGEFREQ_NEVER,
			self::CHANGEFREQ_WEEKLY,
			self::CHANGEFREQ_YEARLY
		])) {
			$this->changefreq = $changefreq;
		} else {
			throw new \Exception(__METHOD__.": unexpected input format.",500);
		}
		return $this;
	}

	/**
	 * Fetch the item's priority
	 *
	 * @return string|null Priority or null if not set
	 *
	 * @since 0.6.0-dev
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * Set the item's priority
	 *
	 * @param string|float|null $priority Either a string denoting the priority value, a numeric value equivalent to one of the available values or null to disable the priority display.
	 *
	 * @return \uMVC\Xml\Item\Sitemap Provides a fluent interface
	 *
	 * @throws \Exception if a wrong value is passed
	 *
	 * @since 0.6.0-dev
	 */
	public function setPriority($priority)
	{
		if (is_numeric($priority)) {
			$priority = number_format($priority,1);
		}
		if (is_string($priority)) {
			if (in_array($priority, [
				self::PRIORITY_00,
				self::PRIORITY_01,
				self::PRIORITY_02,
				self::PRIORITY_03,
				self::PRIORITY_04,
				self::PRIORITY_05,
				self::PRIORITY_06,
				self::PRIORITY_07,
				self::PRIORITY_08,
				self::PRIORITY_09,
				self::PRIORITY_10
			])) {
				$this->priority = $priority;
			} else {
				throw new \Exception(__METHOD__.": unexpected input format.",500);
			}
		} else if (null === $priority) {
			$this->priority = $priority;
		} else {
			throw new \Exception(__METHOD__.": unexpected input format.",500);
		}
		return $this;
	}
}
