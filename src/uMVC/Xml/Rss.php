<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Xml;

/**
 * An RSS feed
 *
 * @package Xml
 * @subpackage Feed
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class Rss extends \uMVC\Xml {
	/**
	 * The name of the channel
	 * @var string
	 */
	private $title;

	/**
	 * The URL to the HTML website corresponding to the channel
	 * @var string
	 */
	private $link;

	/**
	 * Phrase or sentence describing the channel
	 * @var string
	 */
	private $description;

	/**
	 * Feed _items
	 * @var array
	 */
	private $items;

	/**
	 * The language the channel is written in, e.g. "en-gb"
	 * @var string|null
	 */
	private $language = null;

	/**
	 * Copyright notice for content in the channel
	 * @var string|null
	 */
	private $copyright = null;

	/**
	 * The publication date for the content in the channel
	 * @var \DateTime|null
	 */
	private $pubDate = null;

	/**
	 * Time to live, in minutes
	 * @var int|null
	 */
	private $ttl = null;

	/**
	 * Construct the feed object
	 *
	 * @param string $title The channel's name
	 * @param string $link The channel's URL
	 * @param string $description Phrase or sentence describing the channel
	 *
	 * @since 0.2.0-dev
	 */
	public function __construct($title = '', $link = '', $description = '')
	{
		$this->title = $title;
		$this->link = $link;
		$this->description = $description;
	}

	/**
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param $title
	 * @return \uMVC\Xml\Rss
	 *
	 * @since 0.2.0-dev
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * @param $link
	 * @return \uMVC\Xml\Rss
	 *
	 * @since 0.2.0-dev
	 */
	public function setLink($link)
	{
		$this->link = $link;
		return $this;
	}

	/**
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param $description
	 * @return \uMVC\Xml\Rss
	 *
	 * @since 0.2.0-dev
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return mixed
	 *
	 * @since 0.2.0-dev
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * Append a new feed item
	 *
	 * @param \uMVC\Xml\Item\Rss $item
	 *
	 * @return Rss
	 *
	 * @since 0.2.0-dev
	 */
	public function addItem(\uMVC\Xml\Item\Rss $item)
	{
		$this->items[] = $item;
		return $this;
	}

	/**
	 * Render the complete XML for the feed
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function render()
	{
		$result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<rss xmlns:content=\"http://purl.org/rss/1.0/modules/content/\" version=\"2.0\">\n<channel>\n<generator>uMVC\Xml\Rss</generator>\n";
		$result .= "<title><![CDATA[{$this->title}]]></title>\n";
		$result .= "<link>{$this->link}</link>\n";
		$result .= "<description><![CDATA[{$this->description}]]></description>\n";
		$result .= "<docs>http://blogs.law.harvard.edu/tech/rss</docs>\n";
		if (null !== $this->pubDate) {
			$result .= "<pubDate>".$this->pubDate->format(\DateTime::RFC822)."</pubDate>\n";
		}
		if (null !== $this->language) {
			$result .= "<language>{$this->language}</language>\n";
		}
		if (null !== $this->copyright) {
			$result .= "<copyright>{$this->copyright}</copyright>\n";
		}
		if (null !== $this->ttl) {
			$result .= "<ttl>{$this->ttl}</ttl>\n";
		}
		if (!empty($this->items)) {
			foreach ($this->items as $item) {
				$result .= $item;
			}
		}
		$result .= "</channel>\n</rss>";

		return $result;
	}

	/**
	 * Fetch the channel's language
	 *
	 * @return null|string The language, or null if not set
	 *
	 * @since 0.3.2-dev
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * Set the channel's language. The languages must follow the format described in RSS specification.
	 *
	 * @param string|null $language The channel's language or null to unset it
	 *
	 * @return \uMVC\Xml\Rss Provides a fluent interface
	 *
	 * @since 0.3.2-dev
	 *
	 * @link http://cyber.law.harvard.edu/rss/languages.html
	 */
	public function setLanguage($language)
	{
		if (null === $language) {
			$this->language = null;
		} else {
			$this->language = strval($language);
		}
		return $this;
	}

	/**
	 * Fetch the feed's published date
	 *
	 * @return \DateTime The publication date for the content in the channel
	 *
	 * @since 0.3.2-dev
	 */
	public function getPubDate()
	{
		return $this->pubDate;
	}

	/**
	 * Set the feed's published date
	 *
	 * @param \DateTime|string $pubDate The publication date for the content in the channel
	 *
	 * @return \uMVC\Xml\Rss Provides a fluent interface
	 *
	 * @since 0.3.2-dev
	 */
	public function setPubDate($pubDate)
	{
		if ($pubDate instanceof \DateTime) {
			$this->pubDate = $pubDate;
		} else {
			$this->pubDate = new \DateTime($pubDate);
		}
		return $this;
	}

	/**
	 * Fetch the copyright notice for the content in the channel
	 *
	 * @return null|string The copyright notice or null if not set
	 *
	 * @since 0.7.0-dev
	 */
	public function getCopyright()
	{
		return $this->copyright;
	}

	/**
	 * Set the copyright notice for the content in the channel
	 *
	 * @param string|null $copyright The copyright notice string or null to unset it
	 *
	 * @return \uMVC\Xml\Rss Provides a fluent interface
	 *
	 * @since 0.7.0-dev
	 */
	public function setCopyright($copyright)
	{
		if (null === $copyright) {
			$this->copyright = null;
		} else {
			$this->copyright = strval($copyright);
		}
		return $this;
	}

	/**
	 * Fetch the channel's time to live
	 *
	 * @return int|null Minutes to live or null if not set
	 *
	 * @since 0.7.0-dev
	 */
	public function getTtl()
	{
		return $this->ttl;
	}

	/**
	 * Set the channel's time to live
	 *
	 * @param int|null $ttl Time to live, in minutes, or null to unset it
	 *
	 * @return \uMVC\Xml\Rss Provides a fluent interface
	 *
	 * @since 0.7.0-dev
	 */
	public function setTtl($ttl)
	{
		if (null === $ttl) {
			$this->ttl = null;
		} else {
			$this->ttl = intval($ttl);
		}
		return $this;
	}

	/**
	 * Consume an RSS feed
	 *
	 * @param string $xml The XML string
	 *
	 * @return \uMVC\Xml\Rss Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function parse($xml)
	{
		$xml = new \SimpleXMLElement($xml);

		// get channel title
		$this->setTitle(strval($xml->channel->title));

		// get channel link
		$this->setLink(strval($xml->channel->link));

		// get channel description
		$this->setDescription(strval($xml->channel->description));

		// get channel author
		// @todo add author

		// get channel ttl
		if (!empty($xml->channel->ttl)) {
			$this->setTtl($xml->channel->ttl);
		}

		// get pubDate
		if (!empty($xml->channel->pubDate)) {
			$this->setPubDate($xml->channel->pubDate);
		}

		// get channel language
		if (!empty($xml->channel->language)) {
			$this->setLanguage($xml->channel->language);
		}

		// get channel copyright
		if (!empty($xml->channel->copyright)) {
			$this->setCopyright($xml->channel->copyright);
		}

		// get channel items
		foreach ($xml->channel->item as $item) {
			$rssItem = new \uMVC\Xml\Item\Rss();

			// mandatory elements
			$rssItem->setTitle(strval($item->title));
			$rssItem->setDescription(strval($item->description));
			$rssItem->setLink($item->link);

			// optional elements
			if (!empty($item->pubDate)) {
				$rssItem->setPubDate($item->pubDate);
			}

			if (!empty($item->author)) {
				$rssItem->setAuthor($item->author);
			}

			// add the item to the feed
			$this->addItem($rssItem);
		}

		return $this;
	}
}
