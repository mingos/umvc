<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Xml;

/**
 * An XML sitemap
 *
 * @package Xml
 * @subpackage Feed
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.6.0-dev
 */
class Sitemap extends \uMVC\Xml {
	private $items = [];

	/**
	 * Append a new sitemap URL item
	 *
	 * @param \uMVC\Xml\Item\Sitemap $item Sitemap URL item to add
	 *
	 * @return \uMVC\Xml\Sitemap Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function appendUrl(\uMVC\Xml\Item\Sitemap $item)
	{
		$this->items[] = $item;
		return $this;
	}

	/**
	 * Prepend a new sitemap URL item
	 *
	 * @param \uMVC\Xml\Item\Sitemap $item Sitemap URL tem to add
	 *
	 * @return \uMVC\Xml\Sitemap Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function prependUrl(\uMVC\Xml\Item\Sitemap $item)
	{
		$this->items = array_merge([$item],$this->items);
		return $this;
	}

	/**
	 * Render the complete sitemap XML
	 *
	 * @return string
	 *
	 * @since 0.6.0-dev
	 */
	public function render()
	{
		$result = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
		$result .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		foreach ($this->items as $item) {
			$result .= $item;
		}
		$result .= "</urlset>";

		return $result;
	}

	/**
	 * Do nothing (no sitemap parser).
	 *
	 * @param string $xml bogus param
	 *
	 * @since 0.14.0-dev
	 */
	public function parse($xml)
	{}
}
