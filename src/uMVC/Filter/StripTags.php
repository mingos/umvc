<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Filter;

/**
 * HTML stripping filter
 *
 * @package Filter
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class StripTags extends \uMVC\Filter {
	/**
	 * Whether comments should be left in
	 * @var boolean
	 */
	private $allowComments = false;

	/**
	 * An array of HTML tags that are not stripped
	 * @var array
	 */
	private $allowedTags = [];

	/**
	 * An array of HTML attributes that are not stripped
	 * @var array
	 */
	private $allowedAttributes = [];

	/**
	 * Strip the undesired HTML markup
	 *
	 * @param string $value The input string with HTML markup
	 *
	 * @return string Filtered string
	 *
	 * @since 0.0.0-dev
	 */
	public function filter($value)
	{
		// start by stripping the comments, if necessary
		if (!$this->allowComments) {
			$value = preg_replace('/<!\-\-.*\-\->/U', '', $value);
		}

		// strip unallowed tags
		$allowed = '';
		foreach ($this->allowedTags as $tag) {
			$allowed .= "<{$tag}>";
		}
		$value = strip_tags($value, $allowed);

		// strip unallowed attributes - only if there are allowed tags,
		// otherwise all attributes have already been removed with the tags
		if (!empty($this->allowedTags)) {
			$allowed = $this->allowedAttributes;
			do {
				$old   = $value;
				$value = preg_replace_callback('/<[a-zA-Z]+ *(([a-zA-Z_:][\-a-zA-Z0-9_:\.]+) *=.*["\'].*["\']).*>/U', function($matches) use ($allowed) {
					if (in_array($matches[2], $allowed)) {
						return $matches[0];
					} else {
						return str_replace(' ' . $matches[1], '', $matches[0]);
					}
				}, $value);
			} while ($old != $value);
		}

		// we're left with the filtered value
		return $value;
	}

	/**
	 * Set the HTML tags that should be left in the input string
	 *
	 * @param array|string $tags The allowed tags: either an array or a string with a single tag.
	 *
	 * @return \uMVC\Filter\StripTags Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAllowedTags($tags)
	{
		if (!is_array($tags)) {
			$tags = [$tags];
		}
		$this->allowedTags = $tags;
		return $this;
	}

	/**
	 * Set the HTML attributes that should be left in the unstripped tags in the input string
	 *
	 * @param array $attributes The allowed attributes: either an array or a string with a single attribute.
	 *
	 * @return \uMVC\Filter\StripTags Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAllowedAttributes($attributes)
	{
		if (!is_array($attributes)) {
			$attributes = [$attributes];
		}
		$this->allowedAttributes = $attributes;
		return $this;
	}

	/**
	 * Choose whether HTML comments should be left in or stripped.
	 *
	 * By default, the filter removes HTML comments, so there's no need to explicitly set this option to false.
	 *
	 * @param boolean $flag True or omit to leave the comments intact, false otherwise.
	 *
	 * @return \uMVC\Filter\StripTags Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAllowComments($flag = true)
	{
		$this->allowComments = (boolean)$flag;
		return $this;
	}
}
