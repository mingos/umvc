<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Filter;

/**
 * Replace substring(s) using a regex
 *
 * @package Filter
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.5.2-dev
 */
class PregReplace extends \uMVC\Filter {
	/**
	 * The regex
	 * @var string
	 */
	private $match;

	/**
	 * Replacement string
	 * @var string
	 */
	private $replacement;

	/**
	 * Construct the filter
	 *
	 * @param string $match The regex
	 * @param string $replacement Replacement string
	 */
	public function __construct($match = '/^$/', $replacement = '')
	{
		$this->match = strval($match);
		$this->replacement = strval($replacement);
	}

	/**
	 * Run the regex replacement
	 *
	 * @param string $value
	 *
	 * @return string
	 */
	public function filter($value)
	{
		return preg_replace($this->match, $this->replacement, $value);
	}
}
