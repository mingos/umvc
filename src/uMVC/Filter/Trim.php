<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Filter;

/**
 * String trimming filter
 *
 * @package Filter
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.14.0-dev
 */
class Trim extends \uMVC\Filter {
	/**
	 * Characters to be trimmed
	 * @var string
	 */
	private $chars;

	/**
	 * Trimming type to be used (left, right, normal)
	 * @var string
	 */
	private $type;

	/**
	 * Instantiate the filter
	 *
	 * @param string $chars [optional] Characters to be trimmed
	 * @param string $type [optional] Trimming type to be used (left, right, normal)
	 *
	 * @since 0.14.0-dev
	 */
	public function __construct($chars = null, $type = "normal")
	{
		$this->chars = $chars;
		$this->type = $type;
	}

	/**
	 * Trim the string
	 *
	 * @param string $value The input string
	 *
	 * @return string Filtered string
	 *
	 * @since 0.14.0-dev
	 */
	public function filter($value)
	{
		// perform trimming according to the specified criteria
		switch ($this->type) {
			case "left":
				$value = ltrim($value, $this->chars);
				break;
			case "right":
				$value = rtrim($value,$this->chars);
				break;
			default:
				$value = trim($value,$this->chars);
				break;
		}

		// we're left with the filtered value
		return $value;
	}

	/**
	 * Set the charlist for the trim function
	 *
	 * @param string $chars
	 *
	 * @return \uMVC\Filter\Trim Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setChars($chars = null)
	{
		$this->chars = $chars;
		return $this;
	}

	/**
	 * Set the trimming type (left, right or normal)
	 *
	 * @param string $type
	 *
	 * @return \uMVC\Filter\Trim Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setType($type = "normal")
	{
		$this->type = $type;
		return $this;
	}
}
