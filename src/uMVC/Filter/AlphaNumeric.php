<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Filter;

use uMVC\Filter;

/**
 * Filter reducing a string to alphabetic, numeric or alphanumeric characters only
 *
 * @package Filter
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class AlphaNumeric extends Filter
{
	/**
	 * Whether to allow alphabetic characters
	 * @var boolean
	 */
	private $allowAlphabetic = true;

	/**
	 * Whether to allow numeric characters
	 * @var boolean
	 */
	private $allowNumeric = true;

	/**
	 * Strip all non-alphabetic and/or non-numeric characters
	 *
	 * @param string $value The input string
	 *
	 * @return string Filtered string
	 *
	 * @since 0.5.2-dev
	 */
	public function filter($value)
	{
		$regex = '/[\W_]/u';

		if ($this->allowAlphabetic && !$this->allowNumeric) {
			$regex = '/[\W_\d]/u';
		} else if ($this->allowNumeric && !$this->allowAlphabetic) {
			$regex = '/[\D]/u';
		} else if (!$this->allowAlphabetic && !$this->allowNumeric) {
			return '';
		}

		return preg_replace($regex, '', $value);
	}

	/**
	 * Check whether alphabetic characters are allowed
	 *
	 * @return boolean
	 *
	 * @since 0.5.2-dev
	 */
	public function getAllowAlphabetic()
	{
		return $this->allowAlphabetic;
	}

	/**
	 * Set alphabetic character allowing
	 *
	 * @param boolean $allowAlphabetic
	 *
	 * @return \uMVC\Filter\AlphaNumeric Provides a fluent interface
	 *
	 * @since 0.5.2-dev
	 */
	public function setAllowAlphabetic($allowAlphabetic)
	{
		$this->allowAlphabetic = (boolean)$allowAlphabetic;
		return $this;
	}

	/**
	 * Check whether digits are allowed
	 *
	 * @return boolean
	 *
	 * @since 0.5.2-dev
	 */
	public function getAllowNumeric()
	{
		return $this->allowNumeric;
	}

	/**
	 * Set digit allowing
	 *
	 * @param boolean $allowNumeric
	 *
	 * @return \uMVC\Filter\AlphaNumeric Provides a fluent interface
	 *
	 * @since 0.5.2-dev
	 */
	public function setAllowNumeric($allowNumeric)
	{
		$this->allowNumeric = (boolean)$allowNumeric;
		return $this;
	}
}
