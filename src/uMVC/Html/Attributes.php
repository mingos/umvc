<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Html;

/**
 * Attributes list
 *
 * @package	Html
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Attributes
{
	/**
	 * Array containing the attributes list
	 * @var array
	 */
	private $attributes = [];

	/**
	 * Add an attribute or an array thereof. If the attribute already exists, the specified values will be added to it, without overwriting the previous ones. Duplicate values are removed.
	 *
	 * @param string|array|\uMVC\Html\Attributes $attribute The name of the attribute to add, a name-value array of attributes or an attributes object
	 * @param string|array $value In case the first parametre is a string, value or array of values for the added attribute
	 *
	 * @return \uMVC\Html\Attributes Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function add($attribute, $value = [])
	{
		if (is_array($attribute)) {
			foreach ($attribute as $k => $v) {
				$this->add($k, $v);
			}
		} else {
			if ($attribute instanceof \uMVC\Html\Attributes) {
				$this->add($attribute->getArray());
			} else {
				$attribute = strval($attribute);
				if (!array_key_exists($attribute, $this->attributes)) {
					$this->attributes[$attribute] = [];
				}

				if (is_array($value)) {
					foreach ($value as $k => $v) {
						$value[$k] = strval($v);
					}
				} else {
					if (empty($value) && $value !== '0') {
						$value = [];
					} else {
						$value = [strval($value)];
					}
				}

				foreach ($value as $v) {
					$this->attributes[$attribute][] = $v;
				}
				$this->attributes[$attribute] = array_unique($this->attributes[$attribute]);
			}
		}

		return $this;
	}

	/**
	 * Set the value or values of an attribute or an array thereof. Already existent attributes are overwritten.
	 *
	 * @param string|array|\uMVC\Html\Attributes $attribute The name of the attribute to set, a name-value array of attributes or an attributes object
	 * @param string|array $value In case the first parametre is a string, value or array of values for the set attribute
	 *
	 * @return \uMVC\Html\Attributes Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function set($attribute, $value = [])
	{
		if (is_array($attribute)) {
			$this->attributes = [];
			foreach ($attribute as $k => $v) {
				$this->set($k, $v);
			}
		} else {
			if ($attribute instanceof \uMVC\Html\Attributes) {
				$this->attributes = $attribute->getArray();
			} else {
				$attribute = strval($attribute);
				if (is_array($value)) {
					foreach ($value as $k => $v) {
						$value[$k] = strval($v);
					}
				} else {
					if (empty($value) && $value !== '0') {
						$value = [];
					} else {
						$value = [strval($value)];
					}
				}

				$this->attributes[$attribute] =array_unique($value);
			}
		}

		return $this;
	}

	/**
	 * Remove an attribute or a value
	 *
	 * @param string $attribute The attribute name to remove (or to remove a value from)
	 * @param string $value The value to remove from the attribute. Omit the parametre to remove the entire attribute.
	 *
	 * @return \uMVC\Html\Attributes Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function remove($attribute, $value = null)
	{
		$attribute = strval($attribute);

		if (array_key_exists($attribute, $this->attributes)) {
			if (null === $value) {
				unset($this->attributes[$attribute]);
			} else {
				$value = strval($value);
				foreach ($this->attributes[$attribute] as $k => $v) {
					if ($v == $value) {
						unset($this->attributes[$attribute][$k]);
					}
				}
			}
		}

		return $this;
	}

	/**
	 * Get the entire attributes array or the array of values for a single attribute.
	 *
	 * @param string $attribute The attribute whose values are to be retrieved. Omit the parametre to fetch the entire array of attributes.
	 *
	 * @return mixed The attribute or attributes or null in case a nonexistent attribute is requested
	 *
	 * @since 0.0.0-dev
	 */
	public function getArray($attribute = null)
	{
		if (null === $attribute) {
			return $this->attributes;
		} else {
			$attribute = strval($attribute);
			if (array_key_exists($attribute, $this->attributes)) {
				return $this->attributes[$attribute];
			}
		}

		return null;
	}

	/**
	 * Generate the HTML code for the attributes
	 *
	 * @param string $attribute The attribute for which HTML code is to be generated. Omit the parametre to generate HTML code for all attributes.
	 *
	 * @return string HTML code
	 *
	 * @since 0.0.0-dev
	 */
	public function getHtml($attribute = null)
	{
		if (null !== $attribute) {
			$attribute = strval($attribute);
			if (array_key_exists($attribute, $this->attributes)) {
				return $attribute . '="' . implode(' ', $this->attributes[$attribute]) . '"';
			}
		} else {
			$return = [];
			foreach (array_keys($this->attributes) as $attrib) {
				$return[] = $this->getHtml($attrib);
			}
			return implode(' ', $return);
		}

		return '';
	}

	/**
	 * Generate the HTML code for all attributes
	 *
	 * @return string HTML code
	 *
	 * @since 0.0.0-dev
	 */
	public function __toString()
	{
		return $this->getHtml();
	}

	/**
	 * Check whether a given attribute or attribute value exists
	 *
	 * @param string $attribute Attribute name whose existence is to be checked
	 * @param string $value The attribute's value to be checked. Omit the parametre to check the existence of the attribute.
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function exists($attribute, $value = null)
	{
		$attribute = strval($attribute);

		if (array_key_exists($attribute, $this->attributes)) {
			if (null === $value || in_array(strval($value), $this->attributes[$attribute])) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Clear all attributes
	 *
	 * @return \uMVC\Html\Attributes Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function clear()
	{
		$this->attributes = [];
		return $this;
	}
}
