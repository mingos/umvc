<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Handles form behaviour
 *
 * @package Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Form
{
	/**#@+
	 * Form methods
	 */
	const METHOD_DELETE = 'DELETE';
	const METHOD_GET = 'GET';
	const METHOD_POST = 'POST';
	const METHOD_PUT = 'PUT';
	/**#@-*/

	/**#@+
	 * Available enctype values
	 * @var string
	 */
	const ENCTYPE_APPLICATION = 'application/x-www-form-urlencoded';
	const ENCTYPE_MULTIPART = 'multipart/form-data';
	const ENCTYPE_TEXT = 'text/plain';
	/**#@-*/

	/**
	 * Form elements
	 * @var array
	 */
	private $elements = [];

	/**
	 * The view used to render the form
	 * @var \uMVC\View
	 */
	private $view = null;

	/**
	 * The element's HTML attributes
	 * @var \uMVC\Html\Attributes
	 */
	private $attributes;

	/**
	 * Construct the object
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct()
	{
		$this->attributes = new \uMVC\Html\Attributes();
		$this->setAction(REQUEST_URI_FULL);
		$this->setMethod(self::METHOD_POST);
		$this->_init();
	}

	/**
	 * Initialise the values
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{}

	/**
	 * Set the form action
	 *
	 * @param string $action
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAction($action)
	{
		// if it's a relative path, correct it!
		if (substr($action,0,1) == "/") {
			$action = DOCROOT_CORRECT.'/'.trim($action,'/');
		}

		$this->setAttr("action", strval($action));
		return $this;
	}

	/**
	 * Fetch the form action
	 *
	 * @return string|null The form action or null if not set.
	 *
	 * @since 0.0.0-dev
	 */
	public function getAction()
	{
		return $this->getAttr("action");
	}

	/**
	 * Set the form method
	 *
	 * @param string $method
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setMethod($method)
	{
		$this->setAttr("method", $method);
		return $this;
	}

	/**
	 * Fetch the form method
	 *
	 * @return string The form method
	 *
	 * @since 0.0.0-dev
	 */
	public function getMethod()
	{
		return $this->getAttr("method");
	}

	/**
	 * Set the form name
	 *
	 * @param string $name
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function setName($name)
	{
		$this->setAttr("name", $name);
		return $this;
	}

	/**
	 * Fetch the form name
	 *
	 * @return mixed The form name
	 *
	 * @since 0.16.0-dev
	 */
	public function getName()
	{
		return $this->getAttr("name");
	}

	/**
	 * Reset the values of all elements of a form.
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function reset()
	{
		/**
		 * @var \uMVC\Form\Element $element
		 */
		foreach ($this->getElements() as $element) {
			$element->reset();
		}

		return $this;
	}

	/**
	 * Render all elements of the form.
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function render()
	{
		$view = $this->view;

		if (null === $view) {
			$view = new \uMVC\View\Form();
		}
		$view->open = $this->getOpenTag();
		$view->elements = $this->elements;
		$view->close = $this->getCloseTag();

		ob_start();
		$view->render();
		return ob_get_clean();
	}

	/**
	 * Get the form opening HTML tag
	 *
	 * @return string
	 *
	 * @since 0.12.2-dev
	 */
	public function getOpenTag()
	{
		$attributes = $this->attributes->getHtml();
		return "<form {$attributes}>";
	}

	/**
	 * Get the form closing HTML tag
	 *
	 * @return string
	 *
	 * @since 0.12.2-dev
	 */
	public function getCloseTag()
	{
		return "</form>";
	}

	/**
	 * Check all form elements' validity
	 *
	 * @param array $values [optional] Array of data to populate the form with
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isValid($values = null)
	{
		if (is_array($values)) {
			$this->populate($values);
		}

		$result = true;
		/**
		 * @var \uMVC\Form\Element|\uMVC\Form\Fieldset $element
		 */
		foreach ($this->getElements() as $element) {
			$result &= $element->isValid();
		}
		return $result;
	}

	/**
	 * Fetch a single element value from the form. If an element is in fact an array thereof, an array of values will be returned.
	 *
	 * @param string $name The element's name
	 *
	 * @return mixed The element's value
	 *
	 * @since 0.0.0-dev
	 */
	public function getValue($name)
	{
		if (!array_key_exists($name, $this->elements)) {
			foreach ($this->getElements() as $element) {
				if ($element instanceof \uMVC\Form\Fieldset) {
					$value = $element->getValue($name);
					if (null !== $value) {
						return $value;
					}
				}
			}
			return null;
		} else {
			return $this->getElement($name)->getValue();
		}
	}

	/**
	 * Fetch all values from all of the form's elements.
	 *
	 * @return array All values of the form elements
	 *
	 * @since 0.0.0-dev
	 */
	public function getValues()
	{
		$values = [];
		/**
		 * @var \uMVC\Form\Element|\uMVC\Form\Fieldset $item
		 */
		foreach ($this->getElements() as $item) {
			if ($item instanceof \uMVC\Form\Fieldset) {
				$values += $item->getValues();
			} else {
				if ($item->getNameRender()) {
					$value   = $item->getValue();
					$indices = array_merge([$item->getName()], $item->getArrayIndices());
					$current = &$values;
					foreach ($indices as $index) {
						// is there a key?
						if ($index == '') {
							$current[] = $value;
						} else {
							if (!is_array($current)) {
								$current = [$index => $current];
							} else if (!array_key_exists($index, $current)) {
								$current[$index] = $value;
							}
							$current = &$current[$index];
						}
					}
				}
			}
		}
		return $values;
	}

	/**
	 * Fetch all values from all of the form's elements, without applying filtering.
	 *
	 * @return array All values of the form elements
	 *
	 * @since 0.0.0-dev
	 */
	public function getUnfilteredValues()
	{
		$values = [];
		/**
		 * @var \uMVC\Form\Element|\uMVC\Form\Fieldset $item
		 */
		foreach ($this->getElements() as $item) {
			if ($item instanceof \uMVC\Form\Fieldset) {
				$values += $item->getUnfilteredValues();
			} else {
				if ($item->getNameRender()) {
					$value   = $item->getUnfilteredValue();
					$indices = array_merge([$item->getName()], $item->getArrayIndices());
					$current = &$values;
					foreach ($indices as $index) {
						// is there a key?
						if ($index == '') {
							$current[] = $value;
						} else {
							if (!is_array($current)) {
								$current = [$index => $current];
							} else if (!array_key_exists($index, $current)) {
								$current[$index] = $value;
							}
							$current = &$current[$index];
						}
					}
				}
			}
		}
		return $values;
	}

	/**
	 * Fetch a single element.
	 *
	 * @param string $name The element's name
	 *
	 * @return \uMVC\Form\Element The requested element
	 *
	 * @since 0.0.0-dev
	 */
	public function getElement($name)
	{
		$name = strval($name);
		/**
		 * @var \uMVC\Form\Element|\uMVC\Form\Fieldset $item
		 */
		foreach ($this->elements as $item) {
			if ($item instanceof \uMVC\Form\Fieldset) {
				$result = $item->getElement($name);
				if (null !== $result) {
					return $result;
				}
			} else {
				if (array_key_exists($name, $this->elements)) {
					return $this->elements[$name];
				}
			}
		}

		return null;
	}

	/**
	 * Fetch all form elements
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getElements()
	{
		return $this->elements;
	}

	/**
	 * Add an element to the form
	 *
	 * @param \uMVC\Form\Element|\uMVC\Form\Fieldset $element
	 * @param string|array $position [optional] where to insert the element: "append" (default), "prepend",
	 * ["after" => <elementName>] or ["before" => <elementName>]
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @throws \Exception if the provided element has a dulicated index name
	 *
	 * @since 0.0.0-dev
	 */
	public function addElement($element, $position = "append")
	{
		$name = $element->getFullName();

		if (is_array($position) && array_key_exists("after",$position)) {
			$this->addElementAfter($element,$position["after"]);
		} else if (is_array($position) && array_key_exists("before",$position)) {
			$this->addElementBefore($element,$position["before"]);
		} else {
			if (substr($name, -2) == '[]') {
				$name = substr($name, 0, -2);
				$i    = -1;
				do {
					++$i;
				} while (array_key_exists("{$name}[{$i}]", $this->getElements()));
				$name = "{$name}[{$i}]";
			}

			if (array_key_exists($name, $this->getElements())) {
				throw new \Exception(__METHOD__.': Duplicated element name: ' . $name, 500);
			}

			// insert the element
			if ($position == "prepend") {
				$this->elements = array_merge([$name => $element],$this->elements);
			} else {
				$this->elements[$name] = $element;
			}

			// if it's a file input, set the form's enctype
			if ($element instanceof \uMVC\Form\Element\File) {
				$this->setEnctype(self::ENCTYPE_MULTIPART);
			} else if ($element instanceof \uMVC\Form\Fieldset) {
				if ($element->getEnctype() != self::ENCTYPE_APPLICATION) {
					$this->setEnctype($element->getEnctype());
				}
			}
		}

		return $this;
	}

	/**
	 * Explicitly add an element after another element
	 *
	 * @param \uMVC\Form\Element $element Element to add
	 * @param string $after The name of the preceding element
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @throws \Exception if the specified preceding element name is invalid
	 *
	 * @since 0.14.0-dev
	 */
	public function addElementAfter($element, $after)
	{
		$name = $element->getFullName();
		$after = strval($after);

		if (array_key_exists($after,$this->elements)) {
			$tmp = $this->elements;
			$this->elements = [];
			foreach ($tmp as $key => $item) {
				$this->elements[$key] = $item;
				if ($key == $after) {
					$this->addElement($element);
				}
			}
			return $this;
		} else {
			foreach ($this->elements as $item) {
				if ($item instanceof \uMVC\Form\Fieldset) {
					$result = $item->getElement($name);
					if (null !== $result) {
						$item->addElementAfter($element, $after);
						return $this;
					}
				}
			}
		}

		throw new \Exception("Could not find element with the name {$after}.", 500);
	}

	/**
	 * Explicitly add an element before another element
	 *
	 * @param \uMVC\Form\Element $element Element to add
	 * @param string $before The name of the following element
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @throws \Exception if the specified following element name is invalid
	 *
	 * @since 0.14.0-dev
	 */
	public function addElementBefore($element, $before)
	{
		$name = $element->getFullName();
		$before = strval($before);

		if (array_key_exists($before,$this->elements)) {
			$tmp = $this->elements;
			$this->elements = [];
			foreach ($tmp as $key => $item) {
				if ($key == $before) {
					$this->addElement($element);
				}
				$this->elements[$key] = $item;
			}
			return $this;
		} else {
			/**
			 * @var \uMVC\Form\Element|\uMVC\Form\Fieldset $item
			 */
			foreach ($this->elements as $item) {
				if ($item instanceof \uMVC\Form\Fieldset) {
					$result = $item->getElement($name);
					if (null !== $result) {
						$item->addElementBefore($element, $before);
						return $this;
					}
				}
			}
		}

		throw new \Exception("Could not find element with the name {$before}.", 500);
	}

	/**
	 * Remove an element
	 *
	 * @param string $name Name of the element to be removed
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function removeElement($name)
	{
		$name = strval($name);

		$elements = $this->elements;
		$this->elements = [];

		/**
		 * @var \uMVC\Form\Element|\uMVC\Form\Fieldset $item
		 */
		foreach ($elements as $key => $item) {
			if (in_array($name,[$item->getName(), $item->getFullName()])) {
				continue;
			} else if ($item instanceof \uMVC\Form\Fieldset) {
				$item->removeElement($name);
			}
			$this->elements[$key] = $item;
		}

		return $this;
	}

	/**
	 * Populate the form elements
	 *
	 * @param array $values Name-value keyed array of form element names and their corresponding values
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @throws \Exception if the provided parametre is not an array
	 *
	 * @since 0.0.0-dev
	 */
	public function populate($values)
	{
		if (!is_array($values)) {
			throw new \Exception(__METHOD__ . ' expects an array as parametre, ' . gettype($values) . ' given.', 500);
		}

		$prefix = func_num_args() > 1 ? func_get_arg(1) : null;

		foreach ($values as $name => $value) {
			if ($prefix !== null) {
				$name = "{$prefix}[{$name}]";
			}
			if (is_array($value)) {
				$this->populate($value, $name);
			} else {
				$element = $this->getElement($name);
				if ($element) {
					$element->setValue($value);
				}
			}
		}

		return $this;
	}

	/**
	 * Renders the form along with its elements.
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Set the view to render the form
	 *
	 * @param \uMVC\View $view The view used to render the form
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.4.4-dev
	 */
	public function setView(\uMVC\View $view)
	{
		$this->view = $view;
		return $this;
	}

	/**
	 * Fetch the view rendering the form
	 *
	 * @return \uMVC\View\Form The view object or null
	 *
	 * @since 0.4.4-dev
	 */
	public function getView()
	{
		return $this->view;
	}

	/**
	 * Fetch the form's enctype
	 *
	 * @return mixed
	 *
	 * @since 0.10.0-dev
	 */
	public function getEnctype()
	{
		return (null === $this->getAttr("enctype")) ? self::ENCTYPE_APPLICATION : $this->getAttr("enctype");
	}

	/**
	 * Set the form's enctype
	 *
	 * @param string $enctype
	 *
	 * @throws \Exception in case an invalid enctype is specified
	 *
	 * @since 0.10.0-dev
	 */
	public function setEnctype($enctype)
	{
		if (in_array($enctype,[
			self::ENCTYPE_APPLICATION,
			self::ENCTYPE_MULTIPART,
			self::ENCTYPE_TEXT
		])) {
			$this->setAttr("enctype", $enctype);
		} else {
			throw new \Exception("Invalid enctype \"{$enctype}\"",500);
		}
	}

	/**
	 * Adds an additional value to an HTML attribute
	 *
	 * @param string $name The attribute's name
	 * @param string $value The attribute's value
	 *
	 * @return string Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function addAttr($name, $value)
	{
		$this->attributes->add($name, $value);
		return $this;
	}

	/**
	 * Fetch the form's HTML attributes as an attributes object
	 *
	 * @return \uMVC\Html\Attributes
	 *
	 * @since 0.16.0-dev
	 */
	public function attr()
	{
		return $this->attributes;
	}

	/**
	 * Fetch a single HTML attribute or all attributes from the form.
	 *
	 * @param string|null $name The attribute's name; if null, all attributes will be fetched as an array
	 *
	 * @return mixed
	 *
	 * @since 0.16.0-dev
	 */
	public function getAttr($name = null)
	{
		return $this->attributes->getArray($name);
	}

	/**
	 * Set an HTML attribute
	 *
	 * @param string $name The attribute's name
	 * @param string $value The attribute's value
	 *
	 * @return string Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function setAttr($name, $value)
	{
		$this->attributes->set($name, $value);
		return $this;
	}

	/**
	 * Remove an HTML attribute
	 *
	 * @param string $name The attribute's name
	 *
	 * @return \uMVC\Form Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function removeAttr($name)
	{
		$this->attributes->remove($name);
		return $this;
	}
}
