<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Base class for all routers
 *
 * @package Core
 * @subpackage Router
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Router
{
	/**
	 * The matched URI
	 * @var string
	 */
	private $uri = null;

	/**
	 * Resolved parametres
	 * @var array|null
	 */
	private $params;

	/**
	 * Correction of the docroot
	 * @var string
	 */
	private $docrootCorrect = "";

	/**
	 * Matched request method
	 * @var array
	 */
	private $method;

	/**
	 * The router
	 * @var \uDispatch\Route
	 */
	private $route;

	/**
	 * Instantiate the router
	 *
	 * @param \uDispatch\Route $route
	 */
	public function __construct(\uDispatch\Route $route)
	{
		$this->route = $route;
	}

	/**
	 * Set the URI
	 *
	 * @param string $uri The URI to be parsed
	 *
	 * @return \uMVC\Router Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setUri($uri)
	{
		$this->uri = $uri;

		return $this;
	}

	/**
	 * Set the docroot correction prefix
	 *
	 * @param string $docrootCorrect
	 *
	 * @return Router Provide a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setDocrootCorrect($docrootCorrect)
	{
		$this->docrootCorrect = $docrootCorrect;

		return $this;
	}

	/**
	 * Set configuration variables
	 *
	 * @param array $config
	 *
	 * @return \uMVC\Router\Token Provides a fluent interface
	 *
	 * @throws \uMVC\Router\Exception\MissingPattern if the route does not define a pattern
	 *
	 * @since 0.0.0-dev
	 */
	public function setConfig($config)
	{
		if (array_key_exists("pattern", $config)) {
			$this->route->match($config['pattern']);
		} else {
			throw new Router\Exception\MissingPattern();
		}

		if (array_key_exists("defaults", $config)) {
			foreach ($config["defaults"] as $param => $value) {
				$this->route->value($param, $value);
			}
		}

		if (array_key_exists("reqs", $config)) {
			foreach ($config["reqs"] as $param => $pattern) {
				$this->route->assert($param, $pattern);
			}
		}

		if (array_key_exists("method", $config)) {
			$this->route->method($config["method"]);
		}

		return $this;
	}

	/**
	 * Set a new route pattern
	 *
	 * @param string $pattern The new route pattern
	 *
	 * @return \uMVC\Router Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setPattern($pattern)
	{
		$this->route->match($pattern);

		return $this;
	}

	/**
	 * Check if the URI and the method match the route
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function match()
	{
		$this->params = $this->route->run($this->uri, $this->method);
		return is_array($this->params);
	}

	/**
	 * Assemble an uri that matches the route
	 *
	 * @param array $params Parametres
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function assemble($params)
	{
		return $this->docrootCorrect . $this->route->assemble($params);
	}

	/**
	 * Fetch the params resolved by route matching
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * Set the request method to be matched
	 *
	 * @param string Request method, such as "POST" or "GET"
	 *
	 * @return \uMVC\Router Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function setMethod($method)
	{
		$this->method = $method;
		return $this;
	}
}
