<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Cache adapter frontend
 *
 * @package Core
 * @subpackage Cache
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.7.1-dev
 */
class Cache
{
	/**
	 * Registry cache key
	 */
	const REG_KEY = "uMVC_Cache_Adapter";

	/**
	 * A singleton instance of the object
	 * @var \uMVC\Cache
	 */
	protected static $_instance = null;

	/**
	 * Prevent object instantiation
	 *
	 * @since 0.7.1-dev
	 */
	protected function __construct()
	{}

	/**
	 * Retrieve an instance of the object
	 *
	 * @return \uMVC\Cache
	 *
	 * @since 0.10.0-dev
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Fetch a cached index
	 *
	 * @param string $key
	 * @param string $namespace [optional]
	 *
	 * @return mixed
	 *
	 * @since 0.7.1-dev
	 */
	public function get($key, $namespace = '')
	{
		if ($this->isEnabled()) {
			return \uMVC\Registry::get(self::REG_KEY)->get($key, $namespace);
		}
		return false;
	}

	/**
	 * Store a value in cache
	 *
	 * @param mixed $value
	 * @param string $key
	 * @param string $namespace [optional]
	 *
	 * @return \uMVC\Cache Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function set($value, $key, $namespace = '')
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->set($value, $key, $namespace);
		}
		return $this;
	}

	/**
	 * Remove a cached index
	 *
	 * @param string $key
	 * @param string $namespace [optional]
	 *
	 * @return \uMVC\Cache Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function delete($key, $namespace = '')
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->delete($key, $namespace);
		}
		return $this;
	}

	/**
	 * Clear the entire cache or a given namespace
	 *
	 * @param string $namespace [optional]
	 *
	 * @return \uMVC\Cache Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function clear($namespace = '')
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->clear($namespace);
		}
		return $this;
	}

	/**
	 * Check if resolved URI caching is on
	 *
	 * @return boolean
	 *
	 * @since 0.7.1-dev
	 */
	public function getCacheUri()
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->getCacheUri();
		}
		return false;
	}

	/**
	 * Check if table metadata caching is on
	 *
	 * @return boolean
	 *
	 * @since 0.7.1-dev
	 */
	public function getCacheMetadata()
	{
		if ($this->isEnabled()) {
			return \uMVC\Registry::get(self::REG_KEY)->getCacheMetadata();
		}
		return false;
	}

	/**
	 * Turn resolved URI caching on or off
	 *
	 * @param boolean $cacheUri
	 *
	 * @return \uMVC\Cache Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function setCacheUri($cacheUri)
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->setCetCacheUri($cacheUri);
		}
		return $this;
	}

	/**
	 * Turn table metadata caching on or off
	 *
	 * @param boolean $cacheMetadata
	 *
	 * @return \uMVC\Cache Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function setCacheMetadata($cacheMetadata)
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->setCetCacheMetadata($cacheMetadata);
		}
		return $this;
	}

	/**
	 * Fetch the parametres of a resolved URI
	 *
	 * @param string $uri
	 *
	 * @return array|boolean Parametres array or false if they haven't been stored or if URI caching is disabled
	 *
	 * @since 0.7.1-dev
	 */
	public function fetchUri($uri)
	{
		if ($this->isEnabled()) {
			return \uMVC\Registry::get(self::REG_KEY)->fetchUri($uri);
		}
		return false;
	}

	/**
	 * Store the parametres of a resolved URI
	 *
	 * @param string $uri The URI that has been resolved
	 * @param array $params Resolved parametres
	 *
	 * @return \uMVC\Cache Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function storeUri($uri, $params)
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->storeUri($uri,$params);
		}
		return $this;
	}

	/**
	 * Fetch a database table's metadata
	 *
	 * @param string $table Table name
	 *
	 * @return array|boolean Requested table metadata or false if either the metadata isn't stored or metadata caching is disabled
	 *
	 * @since 0.7.1-dev
	 */
	public function fetchMetadata($table)
	{
		if ($this->isEnabled()) {
			return \uMVC\Registry::get(self::REG_KEY)->fetchMetadata($table);
		}
		return false;
	}

	/**
	 * Store a table's metadata
	 *
	 * @param string $table Table name
	 * @param array $metadata The table's metadata
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function storeMetadata($table, $metadata)
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->storeMetadata($table,$metadata);
		}
		return $this;
	}

	/**
	 * Set the cache key prefix
	 *
	 * @param string $prefix
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function setPrefix($prefix)
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->setPrefix($prefix);
		}
		return $this;
	}

	/**
	 * Fetch the cache key prefix
	 *
	 * @return string
	 *
	 * @since 0.7.1-dev
	 */
	public function getPrefix()
	{
		if ($this->isEnabled()) {
			return \uMVC\Registry::get(self::REG_KEY)->getPrefix();
		}
		return '';
	}

	/**
	 * Fetch the cache entries Time To Live
	 *
	 * @return int TTL in seconds; 0 for unlimited
	 *
	 * @since 0.7.1-dev
	 */
	public function getTtl()
	{
		if ($this->isEnabled()) {
			return \uMVC\Registry::get(self::REG_KEY)->getTtl();
		}
		return 0;
	}

	/**
	 * Set the cache entries Time To Live
	 *
	 * @param int $ttl TTL in seconds; 0 for unlimited
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.7.1-dev
	 */
	public function setTtl($ttl)
	{
		if ($this->isEnabled()) {
			\uMVC\Registry::get(self::REG_KEY)->setTtl($ttl);
		}
		return $this;
	}

	/**
	 * Check whether the cache is enabled
	 *
	 * @return boolean
	 *
	 * @since 0.15.0-dev
	 */
	public function isEnabled()
	{
		return \uMVC\Registry::exists(self::REG_KEY);
	}
}
