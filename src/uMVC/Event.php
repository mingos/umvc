<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * A way of triggering events, adding observers and hooks.
 *
 * @package Core
 * @subpackage Event
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.13.0-dev
 */
trait Event {
	/**
	 * Trigger an event
	 *
	 * @param string $event Event name
	 * @param mixed $data [optional] Optional data propagated along with the event
	 *
	 * @since 0.13.0-dev
	 */
	public function triggerEvent($event, $data = null)
	{
		// get the registered observers
		$observers = \uMVC\Registry::get("uMVC_Registered_Observers");
		if (empty($observers) || !is_array($observers)) {
			return;
		}

		if (array_key_exists($event,$observers)) {
			foreach ($observers[$event] as $observer) {
				// observer is empty
				if (empty($observer)) {
					if (function_exists($event)) {
						$event($data, []);
					}
				}

				// observer is a function name
				else if (is_string($observer)) {
					if (function_exists($observer)) {
						$observer($data, []);
					}
				}

				// observer is a closure
				else if (is_callable($observer)) {
					$observer($data, []);
				}

				// observer is an object
				else if (is_object($observer)) {
					if (method_exists($observer,$event)) {
						$observer->$event($data, []);
					}
				}

				// observer is a function name with additional arguments
				else if (is_array($observer) && is_string($observer[0])) {
					$func = array_shift($observer);
					if (function_exists($func)) {
						$func($data,$observer);
					}
				}

				// observer is an object
				else if (is_array($observer) && is_object($observer[0])) {
					$obj = array_shift($observer);

					// no method name
					if (empty($observer) || !is_string($observer[0])) {
						if (method_exists($obj,$event)) {
							$obj->$event($data,$observer);
						}
					}

					// method name is specified
					else {
						$func = array_shift($observer);
						if (method_exists($obj,$func)) {
							$obj->$func($data,$observer);
						}
					}
				}
			}
		}
	}

	/**
	 * Add an event observer with a callback function information.
	 *
	 * When an event is triggered, the observer's callback function will be determined and executed (if possible).
	 * The called function will receive one or two arguments.
	 * The first argument is always the data passed along with the event when it's triggered.
	 * The second argument is the remainder of the $observer parametre to this method - but only if it's an array and after the object/method/function information, additional array elements appear.
	 * For example, if the $observer param is <code>["funcName", "foo", "bar"]</code>, then the function call will look like this: <code>funcName($data, ["foo", "bar"]);</code>
	 *
	 * @param string $event Event name to listen to
	 * @param string $observer The data of the observer function. The data can have multiple forms.
	 * If it's empty, the callback will be a function with the name equal to the event name.
	 * If it's a string, function with that name will be called.
	 * If it's a closure, it will be called.
	 * If it's an object, a method with the name equal to the event name will be called.
	 * If it's an array and the first element is a string, function with that name will be called and the other elements will be passed as an argument.
	 * If it's an array, the first element is an object reference and there are no other elements, a method in that object with the name equal to the event name will be called.
	 * If it's an array, the first element is an object and the second element is a string, the string will be treated as the method name in the object, remaining array elements will be passed as argument.
	 *
	 * @since 0.13.0-dev
	 */
	public function observeEvent($event, $observer)
	{
		$observers = \uMVC\Registry::get("uMVC_Registered_Observers");
		if (empty($observers) || !is_array($observers)) {
			$observers = [];
		}

		if (!array_key_exists($event,$observers)) {
			$observers[$event] = [];
		}

		$observers[$event][] = $observer;

		\uMVC\Registry::set("uMVC_Registered_Observers",$observers);
	}
}
