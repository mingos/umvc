<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Class used for building navigation trees, menus and breadcrumb trails
 *
 * @package Navigator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.10.0-dev
 */
class Navigator
{
	/**
	 * Pages structure
	 * @var array
	 */
	private $pages = [];

	/**
	 * Navigator's HTML attributes
	 * @var \uMVC\Html\Attributes
	 */
	private $attributes;

	/**
	 * View used to render the object
	 * @var \uMVC\View\Navigator
	 */
	private $view = null;

	/**
	 * Render only the active trail?
	 * @var boolean
	 */
	private $renderActiveOnly = false;

	/**
	 * Construct the navigator object
	 *
	 * @since 0.10.0-dev
	 */
	public function __construct()
	{
		$this->attributes = new \uMVC\Html\Attributes();
	}

	/**
	 * Append a new page to the navigator's pages list
	 *
	 * @param \uMVC\Navigator\Page $page The page to append
	 *
	 * @return \uMVC\Navigator Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function appendPage(\uMVC\Navigator\Page $page)
	{
		$this->pages[] = $page->setNavigator($this);
		return $this;
	}

	/**
	 * Render the navigator
	 *
	 * @return string
	 *
	 * @since 0.10.0-dev
	 */
	public function render()
	{
		// get only the required pages
		$pages = [];
		foreach ($this->pages as $page) {
			if ($page->isVisible()) {
				$pages[] = $page;
			}
		}

		// set the view
		$view = $this->getView();
		$view->attributes = $this->attributes;
		$view->pages = $pages;

		ob_start();
		$view->render();
		return ob_get_clean();
	}

	/**
	 * Render the navigator
	 *
	 * @return string
	 *
	 * @since 0.10.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Fetch a page with a matching href
	 *
	 * @param string $href
	 *
	 * @return null|\uMVC\Navigator\Page The page matching the specified href or null if no such page is found
	 *
	 * @since 0.10.0-dev
	 */
	public function getByHref($href)
	{
		foreach ($this->pages as $page) {
			$page->prepare();
			$return = $page->getByHref($href);
			if (null !== $return) {
				return $return;
			}
		}
		return null;
	}

	/**
	 * Fetch the active page
	 *
	 * @return null|\uMVC\Navigator\Page The active page or null if no active page is present
	 *
	 * @since 0.10.0-dev
	 */
	public function getActive()
	{
		foreach ($this->pages as $page) {
			$page->prepare();
			$return = $page->getActive();
			if (null !== $return) {
				return $return;
			}
		}
		return null;
	}

	/**
	 * Fetch the attributes list
	 *
	 * @return \uMVC\Html\Attributes
	 *
	 * @since 0.10.0-dev
	 */
	public function attr()
	{
		return $this->attributes;
	}

	/**
	 * Set the view that will be used to render the navigator
	 *
	 * @param \uMVC\View\Navigator $view View used to render the object
	 *
	 * @return \uMVC\Navigator Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setView(\uMVC\View\Navigator $view)
	{
		$this->view = $view;
		return $this;
	}

	/**
	 * Fetch the view used for object rendering
	 *
	 * @return \uMVC\View\Navigator
	 *
	 * @since 0.10.0-dev
	 */
	public function getView()
	{
		if (null === $this->view) {
			$this->view = new \uMVC\View\Navigator();
		}
		return $this->view;
	}

	/**
	 * Set the view used to render all pages
	 *
	 * @param \uMVC\View\Navigator\Page $view View instance
	 *
	 * @return \uMVC\Navigator Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setPagesView(\uMVC\View\Navigator\Page $view)
	{
		foreach ($this->pages as $page) {
			$page->setView($view);
			$page->setPagesView($view);
		}
		return $this;
	}

	/**
	 * Set rendering to just the active trail or all available pages
	 *
	 * @param boolean $renderActiveOnly
	 *
	 * @return \uMVC\Navigator Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setRenderActiveOnly($renderActiveOnly = true)
	{
		$this->renderActiveOnly = $renderActiveOnly;
		return $this;
	}

	/**
	 * Check whether only the active trail should be rendered
	 *
	 * @return boolean
	 *
	 * @since 0.14.0-dev
	 */
	public function getRenderActiveOnly()
	{
		return $this->renderActiveOnly;
	}
}
