<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Output;

/**
 * Output data as a JSON string
 *
 * @package Core
 * @subpackage Output
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.16.0-dev
 */
class Json extends \uMVC\Output implements \IteratorAggregate, \Countable, \ArrayAccess {
	/**
	 * Data that will be serialised to a JSON string on output
	 * @var array
	 */
	protected $_data = [];

	/**
	 * Construct the output
	 *
	 * @param boolean $setJsonHeader Set to false if a JSON header should not be sent
	 *
	 * @since 0.16.0-dev
	 */
	public function __construct($setJsonHeader = true)
	{
		if ($setJsonHeader) {
			header("Content-type: application/json");
		}
	}

	/**
	 * Set a named variable (the output will be an object)
	 *
	 * @param string $key variable name
	 * @param mixed $value variable value
	 *
	 * @return \uMVC\Output\Json Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function setVar($key, $value)
	{
		$this->_data[$key] = $value;
		return $this;
	}

	/**
	 * Unset a variable
	 *
	 * @param string $key key to unset
	 *
	 * @return \uMVC\Output\Json Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function unsetVar($key)
	{
		if (array_key_exists($key, $this->data)) {
			unset($this->_data[$key]);
		}
		return $this;
	}

	/**
	 * Push a variable without setting a name for it (e.g. when outputting an array)
	 *
	 * @param mixed $value variable value
	 *
	 * @return \uMVC\Output\Json Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function pushVar($value)
	{
		$this->_data[] = $value;
		return $this;
	}

	/**
	 * Fetch the entire data array
	 *
	 * @return array
	 *
	 * @since 0.16.0-dev
	 */
	public function getData()
	{
		return $this->_data;
	}

	/**
	 * Set the entire data array
	 *
	 * @param array|object $data Data to be set
	 *
	 * @return \uMVC\Output\Json Provides a fluent interface
	 *
	 * @throws \Exception when the passed in data is not an array
	 *
	 * @since 0.16.0-dev
	 */
	public function setData($data)
	{
		if (is_array($data)) {
			$this->_data = $data;
		} else if (is_object($data)) {
			foreach ($data as $key => $value) {
				$this->data[$key] = $value;
			}
		} else {
			throw new \Exception ("Input data is expected to be an array or an object.", 500);
		}
		return $this;
	}

	/**
	 * Echo the output
	 *
	 * @return string JSON-encoded output
	 *
	 * @since 0.16.0-dev
	 */
	public function render()
	{
		return json_encode($this->_data);
	}

	/**
	 * Conversion to string
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Fetch a foreach-compatible data array
	 *
	 * @return \uMVC\Iterator
	 *
	 * @since 0.16.0-dev
	 */
	public function getIterator()
	{
		return new \uMVC\Iterator($this->_data);
	}

	/**
	 * Count the elements in the paginator.<br>
	 * Makes it possible to use the count() function on the paginator object.
	 *
	 * @return int Item count on the current page
	 *
	 * @since 0.0.0-dev
	 */
	public function count()
	{
		return count($this->_data);
	}

	/**
	 * Implementation of array_key_exists
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetExists($offset)
	{
		return array_key_exists($offset, $this->_data);
	}

	/**
	 * Implementation of array getter (accessing array elements via array index)
	 *
	 * @return mixed
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetGet($offset)
	{
		return $this->_data[$offset];
	}

	/**
	 * Implementation of array setter (setting array elements via array index)
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			$this->_data[] = $value;
		} else {
			$this->_data[$offset] = $value;
		}
	}

	/**
	 * Implementation of unset
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetUnset($offset)
	{
		unset($this->_data[$offset]);
	}

	/**
	 * Allow setting data indices as if they were object properties
	 *
	 * @since 0.16.0-dev
	 */
	public function __set($name, $value)
	{
		$this->_data[$name] = $value;
	}

	/**
	 * Allow fetching data indices as if they were object properties
	 *
	 * @return mixed
	 *
	 * @since 0.16.0-dev
	 */
	public function __get($name)
	{
		return $this->_data[$name];
	}

	/**
	 * Allow checking the existence of data indices as if they were object properties
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function __isset($name)
	{
		return array_key_exists($name, $this->_data);
	}

	/**
	 * Allow unsetting data indices as if they were object properties
	 *
	 * @since 0.16.0-dev
	 */
	public function __unset($name)
	{
		unset($this->_data[$name]);
	}
}
