<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Output;

/**
 * Handling output using uTemplate (requires the uTemplpate class, please include it before instantiating this class).
 *
 * @package Core
 * @subpackage Output
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.14.0-dev
 */
class UTemplate extends \uMVC\Output {
	/**
	 * The Smarty object
	 * @var \uTemplate
	 */
	private $uTemplate;

	/**
	 * Template name to render
	 * @var string
	 */
	private $layout = null;

	/**
	 * Instantiate the class
	 *
	 * @since 0.14.0-dev
	 */
	public function __construct()
	{
		$this->uTemplate = \uTemplate::getInstance();
	}

	/**
	 * Set the layout's template name to render
	 *
	 * @param string $name Template name
	 *
	 * @return \uMVC\Output\UTemplate
	 *
	 * @since 0.14.0-dev
	 */
	public function setLayoutName($name)
	{
		$this->layout = $name;
		return $this;
	}

	/**
	 * Fetch the uTemplate object.
	 *
	 * @return \uTemplate
	 *
	 * @since 0.14.0-dev
	 */
	public function getUTemplate()
	{
		return $this->uTemplate;
	}

	/**
	 * Echo the output
	 *
	 * @return string
	 *
	 * @throws \Exception if attempting to render the output without having set the layout name
	 *
	 * @since 0.14.0-dev
	 */
	public function render()
	{
		if (null === $this->layout) {
			throw new \Exception("Cannot render the output without specifying the layout view script name.",500);
		}
		return $this->uTemplate->render($this->layout);
	}
}
