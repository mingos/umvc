<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Autoloader. Handles the source file loading
 *
 * @package Core
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Autoloader
{
	/**
	 * Singleton instance
	 * @var \uMVC\Autoloader
	 */
	protected static $_instance;

	/**
	 * Resolved patch cache
	 * @var array
	 */
	private $cache;

	/**
	 * File name of the file containing the cached routes
	 * @var string
	 */
	private $cacheFile;

	/**
	 * Additional namespaces
	 * @var array
	 */
	private $namespaces = [];

	/**
	 * Retrieve singleton instance
	 *
	 * @return \uMVC\Autoloader
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Construct the autoloader
	 *
	 * @since 0.11.0-dev
	 */
	private function __construct()
	{
		$this->cacheFile = ROOT_PATH."/cache/autoload.cache";
		$this->cache = $this->getCache();

		spl_autoload_register([$this, "autoload"]);
	}

	/**
	 * Autoload a class
	 *
	 * @param string $class Class name
	 *
	 * @throws \Exception if the class cannot be loaded
	 *
	 * @since 0.11.0-dev
	 */
	public function autoload($class)
	{
		$ok = false;

		// autoload from cache...
		if ((!defined("uMVC_DEBUG") || !uMVC_DEBUG) && array_key_exists($class,$this->cache)) {
			$file = $this->cache[$class];
			if (is_file($file)) {
				require_once $file;
			} else {
				unset($this->cache[$class]);
				$this->autoload($class);
			}
		} else {
			$parts = explode('\\', $class);
			$ok = false;

			// let's just try the include path...
			$include = explode(PATH_SEPARATOR,get_include_path());
			foreach ($include as $item) {
				$file = $item."/".implode("/",$parts).".php";
				if (is_file($file)) {
					require_once $file;
					if (class_exists($class) || interface_exists($class) || trait_exists($class)) {
						if ((!defined("uMVC_DEBUG") || !uMVC_DEBUG)) {
							$this->savePath($class,$file);
						}
						$ok = true;
						break;
					}
				}
			}

			// if not in the include path, check the application directory
			if (!$ok) {
				$path = $parts;
				$file = APPLICATION_PATH."/".implode("/", $path).".php";

				if (is_file($file)) {
					require_once $file;
					if (class_exists($class) || interface_exists($class) || trait_exists($class)) {
						if ((!defined("uMVC_DEBUG") || !uMVC_DEBUG)) {
							$this->savePath($class,$file);
						}
						$ok = true;
					}
				}
			}

			// if not in the application directory either, check additional namespaces
			if (!$ok) {
				$path = $parts;
				$namespace = array_shift($path);

				if (array_key_exists($namespace,$this->namespaces)) {
					$file = $this->namespaces[$namespace]."/".implode("/", $path).".php";

					if (is_file($file)) {
						require_once $file;
						if (class_exists($class) || interface_exists($class) || trait_exists($class)) {
							if ((!defined("uMVC_DEBUG") || !uMVC_DEBUG)) {
								$this->savePath($class,$file);
							}
							$ok = true;
						}
					}
				}
			}
		}

		// save debug info
		if (defined("uMVC_DEBUG") && uMVC_DEBUG) {
			$debugAutoloader = \uMVC\Registry::get('uMVC_DEBUG_AUTOLOADER');
			if (null === $debugAutoloader) {
				$debugAutoloader = [];
			}
			$debugAutoloader[] = ['class' => $class, 'file' => $file];
			\uMVC\Registry::set('uMVC_DEBUG_AUTOLOADER',$debugAutoloader);
		}
	}

	/**
	 * Fetch resolved routes from cache
	 *
	 * @return array
	 *
	 * @since 0.11.0-dev
	 */
	public function getCache()
	{
		if (defined("uMVC_DEBUG") && uMVC_DEBUG) {
			return [];
		}
		if (empty($this->cache)) {
			if (!is_file($this->cacheFile)) {
				$this->cache = [];
				file_put_contents($this->cacheFile,serialize($this->cache));
			} else {
				$this->cache = unserialize(file_get_contents($this->cacheFile));
			}
		}

		return $this->cache;
	}

	/**
	 * Save a file name containing a given class
	 *
	 * @param string $className Name of a class to load
	 * @param string $fileName File where the class can be found
	 *
	 * @throws \Exception If the provided filename is invalid
	 *
	 * @return \uMVC\Autoloader Provides a fluent interface
	 *
	 * @since 0.11.0-dev
	 */
	public function savePath($className, $fileName)
	{
		$cache = $this->getCache();

		$this->cache[$className] = $fileName;
		if (!defined("uMVC_DEBUG") || !uMVC_DEBUG) {
			file_put_contents($this->cacheFile,serialize($this->cache));
		}

		return $this;
	}

	/**
	 * Add an extra namespace
	 *
	 * @param string $namespace The custom class namespace
	 * @param string $path Path to namespaced class files
	 *
	 * @since 0.14.0-dev
	 */
	public function addNamespace($namespace, $path)
	{
		$this->namespaces[$namespace] = $path;
	}
}
