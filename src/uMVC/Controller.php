<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Base class for all controllers
 *
 * @package Core
 * @subpackage Controller
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
abstract class Controller
{
	/**
	 * The request object
	 * @var \uMVC\Request
	 */
	protected $_request = null;

	/**
	 * The application config
	 * @var \uMVC\Config
	 */
	protected $_config = null;

	/**
	 * The helper broker
	 * @var \uMVC\Helper\Broker
	 */
	protected $_helper = null;

	/**
	 * Parametres received from the request or set externally
	 * @var array
	 */
	private $params = [];

	/**
	 * Whether the controller has been instantiated by the front controller during the request dispatching
	 * @var boolean
	 */
	private $main = false;

	/**
	 * Construct the object
	 *
	 * @param boolean $main Whether the controller is the main controller (created throught the request dispatch)
	 * or not (called to render an additional view).
	 *
	 * @since 0.0.0-dev
	 *
	 */
	public function __construct($main = false)
	{
		$this->_request = \uMVC\Request::getInstance();
		$this->_config  = \uMVC\Config::get();
		$this->_helper  = \uMVC\Helper\Broker::getInstance();

		$this->setParams($this->_request->getParams());

		$this->main = (boolean)$main;

		$this->_init();
	}

	/**
	 * Initialise properties
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{}

	/**
	 * Send a header redirect to a different page
	 *
	 * @param string $url
	 *
	 * @since 0.0.0-dev
	 * @deprecated since 0.14.0-dev
	 */
	protected function _redirect($url)
	{
		$this->_request->redirect($url);
	}

	/**
	 * Send a header redirect to a different page
	 *
	 * @param string $url
	 * @param integer $type
	 *
	 * @since 0.14.0-dev
	 */
	public function redirect($url, $type = 302)
	{
		$this->_request->redirect($url, $type);
	}

	/**
	 * Fetch the front controller
	 *
	 * @return \uMVC\Controller\Front
	 *
	 * @since 0.0.0-dev
	 */
	public function getFrontController()
	{
		return \uMVC\Controller\Front::getInstance();
	}

	/**
	 * Set a parametre
	 *
	 * @param string $key The parametre's key
	 * @param mixed $value The parametre's value
	 *
	 * @return \uMVC\Controller Provides a fluent interface
	 *
	 * @since 0.4.1-dev
	 */
	public function setParam($key, $value)
	{
		$this->params[strval($key)] = $value;
		return $this;
	}

	/**
	 * Set multiple parametres
	 *
	 * @param array $params key-value pairs of parametres to be set
	 *
	 * @return \uMVC\Controller Provides a fluent interface
	 *
	 * @throws \Exception in case the passed parametre is not an array
	 *
	 * @since 0.4.1-dev
	 */
	public function setParams($params)
	{
		if (is_array($params)) {
			foreach ($params as $key => $value) {
				$this->setParam($key, $value);
			}
		} else {
			throw new \Exception (__METHOD__." expects parametre to be an array, ".gettype($params)." given.",500);
		}
		return $this;
	}

	/**
	 * Fetch a single parametre
	 *
	 * @param string $key Parametre name
	 * @param mixed $default Default value. Returned in case the parametre is not found.
	 *
	 * @return mixed Either the parametre's value or the provided default value.
	 *
	 * @since 0.4.1-dev
	 */
	public function getParam($key, $default = null)
	{
		$key = strval($key);
		if (array_key_exists($key, $this->params)) {
			return $this->params[$key];
		} else {
			return $default;
		}
	}

	/**
	 * Fetch all parametres set for the controller
	 *
	 * @return array
	 *
	 * @since 0.4.1-dev
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * Check whether the controller has been instantiated during the request dispatching or not
	 *
	 * @return bool
	 *
	 * @since 0.4.1-dev
	 */
	public function isMain()
	{
		return $this->main;
	}

	/**
	 * Function run right before executing every action
	 *
	 * @since 0.11.1-dev
	 */
	public function dispatchStart()
	{}

	/**
	 * Function run immediately after executing every action
	 *
	 * @since 0.11.1-dev
	 */
	public function dispatchEnd()
	{}

	/**
	 * Dispatch the correct action
	 *
	 * @param string $action [optional] Action to force
	 *
	 * @since 0.16.0-dev
	 */
	public function dispatch($action = null)
	{
		$this->dispatchStart();

		// get the action name
		if (null === $action) {
			$action = $this->_request->getParam("action");
		}

		$requestMethod = mb_convert_case($this->_request->getRequestMethod(), MB_CASE_TITLE, "UTF-8");

		$method = "{$action}Action{$requestMethod}";
		if (!method_exists($this, $method)) {
			$method = "{$action}Action";
			if (!method_exists($this, $method)) {
				throw new \Exception("Action \"{$action}\" not found.",404);
			}
		}

		$this->$method();

		$this->dispatchEnd();
	}
}
