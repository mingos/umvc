<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Flash messages support (messages stored in the session)
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class Flash implements \Countable, \IteratorAggregate {
	/**
	 * Clear the message buffer
	 *
	 * @return \uMVC\Helper\Flash Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function clear()
	{
		$_SESSION['uMVC_FLASH'] = [];
		return $this;
	}

	/**
	 * Fetch all messages from the message buffer
	 *
	 * @return array
	 *
	 * @since 0.2.0-dev
	 */
	public function get()
	{
		if (array_key_exists('uMVC_FLASH', $_SESSION)) {
			return $_SESSION['uMVC_FLASH'];
		} else {
			return [];
		}
	}

	/**
	 * Check whether the message buffer is empty
	 *
	 * @return boolean
	 *
	 * @since 0.2.0-dev
	 */
	public function isEmpty()
	{
		if (array_key_exists('uMVC_FLASH', $_SESSION)) {
			return (count($_SESSION['uMVC_FLASH']) == 0);
		} else {
			return true;
		}
	}

	/**
	 * Append a new message to the message buffer
	 *
	 * @param mixed $message The message to append
	 *
	 * @return \uMVC\Helper\Flash Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function add($message)
	{
		if (!array_key_exists('uMVC_FLASH', $_SESSION)) {
			$_SESSION['uMVC_FLASH'] = [];
		}
		$_SESSION['uMVC_FLASH'][] = $message;

		return $this;
	}

	/**
	 * Count the messages in the message buffer
	 *
	 * @return int
	 *
	 * @since 0.2.0-dev
	 */
	public function count()
	{
		if (array_key_exists('uMVC_FLASH', $_SESSION)) {
			return count($_SESSION['uMVC_FLASH']);
		} else {
			return 0;
		}
	}

	/**
	 * Allow iterating over the message buffer using a foreach loop
	 *
	 * @return \uMVC\Iterator
	 *
	 * @since 0.2.0-dev
	 */
	public function getIterator()
	{
		return new \uMVC\Iterator($this->get());
	}
}
