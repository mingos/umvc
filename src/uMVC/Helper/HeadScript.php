<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Controls the adding and rendering of JavaScript inline code in the page's header
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.1-dev
 */
class HeadScript extends \uMVC\Helper\Script {
	/**
	 * A static instance
	 * @var \uMVC\Helper\HeadScript
	 */
	protected static $_instance = null;

	/**
	 * Fetch a static instance
	 * @return \uMVC\Helper\HeadScript
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Rende the scripts
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function render()
	{
		$return = '';
		foreach ($this->_script as $script) {
			$s = "/* <![CDATA[ */\n{$script['script']}\n/* ]]> */\n";
			if ($this->_frontController->getDoctype() == 'HTML5') {
				$s = "<script>\n{$s}</script>\n";
			} else {
				$s = "<script type=\"text/javascript\">\n{$s}</script>\n";
			}
			if (null !== $script['conditional']) {
				$s = "<!--[if {$script['conditional']}]>\n{$s}<![endif]-->\n";
			}
			$return .= $s;
		}

		return $return;
	}
}
