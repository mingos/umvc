<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Controls the adding and rendering of style tags in the page's header
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.1-dev
 */
class HeadStyle
{
	private $style = [];

	/**
	 * A static instance
	 * @var \uMVC\Helper\HeadStyle
	 */
	protected static $_instance = null;

	/**
	 * The front controller
	 * @var \uMVC\Controller\Front
	 */
	protected $_frontController = null;

	/**
	 * Initialise the variables; prevent instantiation
	 *
	 * @since 0.3.1-dev
	 */
	protected function __construct()
	{
		$this->_frontController = \uMVC\Controller\Front::getInstance();
	}

	/**
	 * Fetch a static instance
	 *
	 * @return \uMVC\Helper\HeadStyle
	 *
	 * @since 0.3.1-dev
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Append a new link. Repeated links will be ignored.
	 *
	 * @param string $style The CSS rules to be appended
	 * @param string $conditional [optional]: The condition of the IE conditional comment, e.g. "lte IE 7"
	 *
	 * @return \uMVC\Helper\HeadStyle Provides a fluent interface
	 *
	 * @since 0.3.1-dev
	 */
	public function append($style, $conditional = null)
	{
		foreach ($this->style as $item) {
			if ($style == $item['style']) {
				return $this;
			}
		}
		$this->style[] = ['style' => $style, 'conditional' => $conditional];
		return $this;
	}

	/**
	 * Prepend a new stylesheet. Repeated links will be ignored.
	 *
	 * @param string $style The CSS rules to be prepended
	 * @param string $conditional [optional]: The condition of the IE conditional comment, e.g. "lte IE 7"
	 *
	 * @return \uMVC\Helper\HeadStyle Provides a fluent interface
	 *
	 * @since 0.3.1-dev
	 */
	public function prepend($style, $conditional = null)
	{
		foreach ($this->style as $item) {
			if ($style == $item['style']) {
				return $this;
			}
		}
		$this->style[] = array_merge([['style' => $style, 'conditional' => $conditional]],$this->style);
		return $this;
	}

	/**
	 * Render the link tags
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function render()
	{
		$return = '';
		foreach ($this->style as $item) {
			$s = $item['style'];

			if ($this->_frontController->getDoctype() == 'HTML5') {
				$s = "<style>\n{$s}\n</style>\n";
			} else {
				$s = "<style type=\"text/css\">\n{$s}\n</style>\n";
			}

			if (!is_null($item['conditional'])) {
				$return .= "<!--[if {$item['conditional']}]>\n{$s}<![endif]-->\n";
			} else {
				$return .= "{$s}\n";
			}
		}

		return $return;
	}

	/**
	 * Convert the object to string
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function __toString()
	{
		return $this->render();
	}
}
