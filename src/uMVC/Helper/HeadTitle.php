<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Rendering of the title tag in the page's header
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.1-dev
 */
class HeadTitle
{
	/**
	 * Title segments
	 * @var array
	 */
	protected $_title = [];

	/**
	 * The separator between title segments
	 * @var string
	 */
	protected $_separator = ' | ';

	/**
	 * A static instance
	 * @var \uMVC\Helper\HeadTitle
	 */
	protected static $_instance = null;

	/**
	 * Fetch a static instance
	 *
	 * @return \uMVC\Helper\HeadTitle
	 *
	 * @since 0.3.1-dev
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Private constructor; prevents instantiation
	 *
	 * @since 0.3.1-dev
	 */
	private function __construct()
	{}

	/**
	 * Set the separator to be used to separate title sections
	 *
	 * @param string $separator The separator. The default is " | "
	 *
	 * @return \uMVC\Helper\HeadTitle Provides a fluent interface
	 *
	 * @since 0.3.1-dev
	 */
	public function setSeparator($separator)
	{
		$this->_separator = strval($separator);
		return $this;
	}

	/**
	 * Append a new title section
	 *
	 * @param string $title The title section to append
	 *
	 * @return \uMVC\Helper\HeadTitle Provides a fluent interface
	 *
	 * @since 0.3.1-dev
	 */
	public function append($title)
	{
		foreach ($this->_title as $item) {
			if ($title == $item) {
				return $this;
			}
		}
		$this->_title[] = strval($title);
		return $this;
	}

	/**
	 * Prepend a new title section
	 *
	 * @param string $title The title section to prepend
	 *
	 * @return \uMVC\Helper\HeadTitle Provides a fluent interface
	 *
	 * @since 0.3.1-dev
	 */
	public function prepend($title)
	{
		foreach ($this->_title as $item) {
			if ($title == $item) {
				return $this;
			}
		}
		$this->_title = array_merge([$title],$this->_title);
		return $this;
	}

	/**
	 * Render the title tag HTML
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function render()
	{
		return "<title>".implode($this->_separator,$this->_title)."</title>\n";
	}

	/**
	 * Render the title tag HTML
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function __toString()
	{
		return $this->render();
	}
}
