<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Setting and rendering the doctype
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.1-dev
 */
class Doctype
{
	/**#@+
	 * Available doctypes
	 */
	const XHTML11             = 'XHTML11';
	const XHTML1_STRICT       = 'XHTML1_STRICT';
	const XHTML1_TRANSITIONAL = 'XHTML1_TRANSITIONAL';
	const XHTML1_RDFA         = 'XHTML1_RDFA';
	const XHTML_BASIC1        = 'XHTML_BASIC1';
	const HTML4_STRICT        = 'HTML4_STRICT';
	const HTML4_LOOSE         = 'HTML4_LOOSE';
	const HTML5               = 'HTML5';
	/**#@-*/

	/**
	 * Doctype strings
	 * @var array
	 */
	private $doctype;

	/**
	 * The front controller
	 * @var \uMVC\Controller\Front
	 */
	protected $_frontController = null;

	/**
	 * Initialise the variables
	 *
	 * @since 0.3.1-dev
	 */
	public function __construct()
	{
		$this->_frontController = \uMVC\Controller\Front::getInstance();
		$this->doctype = [
			self::XHTML11             => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
			self::XHTML1_STRICT       => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
			self::XHTML1_TRANSITIONAL => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
			self::XHTML1_RDFA         => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">',
			self::XHTML_BASIC1        => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd">',
			self::HTML4_STRICT        => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">',
			self::HTML4_LOOSE         => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
			self::HTML5               => '<!DOCTYPE html>'
		];
	}

	/**
	 * Get all available doctype strings
	 *
	 * @return array
	 *
	 * @since 0.3.1-dev
	 */
	public function getDoctypes()
	{
		return [
			self::XHTML11,
			self::XHTML1_STRICT,
			self::XHTML1_TRANSITIONAL,
			self::XHTML1_RDFA,
			self::XHTML_BASIC1,
			self::HTML4_STRICT,
			self::HTML4_LOOSE,
			self::HTML5
		];
	}

	/**
	 * Set the doctype to use
	 *
	 * @param string $doctype The doctype to use
	 *
	 * @return \uMVC\Helper\Doctype Provides a fluent interface
	 *
	 * @since 0.3.1-dev
	 */
	public function set($doctype)
	{
		$this->_frontController->setDoctype($doctype);
		return $this;
	}

	/**
	 * Render the doctype declaration
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function render()
	{
		return $this->doctype[$this->_frontController->getDoctype()]."\n";
	}

	/**
	 * Render the doctype declaration
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function __toString()
	{
		return $this->render();
	}
}
