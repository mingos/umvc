<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Controls the adding and rendering of link tags in the page's header
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class HeadLink
{
	/**
	 * The array of links
	 * @var array
	 */
	private $link = [];

	/**
	 * The front controller
	 * @var \uMVC\Controller\Front
	 */
	protected $_frontController = null;

	/**
	 * Initialise the variables; prevent instantiation
	 *
	 * @since 0.2.0-dev
	 */
	protected function __construct()
	{
		$this->_frontController = \uMVC\Controller\Front::getInstance();
	}

	/**
	 * A static instance
	 * @var \uMVC\Controller\Front
	 */
	protected static $_instance = null;

	/**
	 * Fetch a static instance
	 *
	 * @return \uMVC\Helper\Script
	 *
	 * @since 0.2.0-dev
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Append a new stylesheet. Repeated links will be ignored.
	 *
	 * @param string $href The stylesheet href to be added
	 * @param array $attributes Optional attributes (the rel attribute will be added automatically)
	 * @param string $conditional [optional]: The condition of the IE conditional comment, e.g. "lte IE 7"
	 *
	 * @return \uMVC\Helper\HeadLink Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function appendStylesheet($href, $attributes = [], $conditional = null)
	{
		return $this->appendLink($href, ['rel' => 'stylesheet'] + $attributes, $conditional);
	}

	/**
	 * Prepend a new stylesheet. Repeated links will be ignored.
	 *
	 * @param string $href The stylesheet href to be added
	 * @param array $attributes Optional attributes (the rel attribute will be added automatically)
	 * @param string $conditional [optional]: The condition of the IE conditional comment, e.g. "lte IE 7"
	 *
	 * @return \uMVC\Helper\HeadLink Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function prependStylesheet($href, $attributes = [], $conditional = null)
	{
		return $this->prependLink($href, ['rel' => 'stylesheet'] + $attributes, $conditional);
	}

	/**
	 * Append a new link. Repeated links will be ignored.
	 *
	 * @param string $href The link href to be added
	 * @param array $attributes Optional attributes
	 * @param string $conditional [optional]: The condition of the IE conditional comment, e.g. "lte IE 7"
	 *
	 * @return \uMVC\Helper\HeadLink Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function appendLink($href, $attributes = [], $conditional = null)
	{
		foreach ($this->link as $item) {
			if ($href == $item['href']) {
				return $this;
			}
		}
		$this->link[] = ['href' => $href, 'conditional' => $conditional] + $attributes;
		return $this;
	}

	/**
	 * Prepend a new stylesheet. Repeated links will be ignored.
	 *
	 * @param string $href The link href to be added
	 * @param array $attributes Optional attributes
	 * @param string $conditional [optional]: The condition of the IE conditional comment, e.g. "lte IE 7"
	 *
	 * @return \uMVC\Helper\HeadLink Provides a fluent interface
	 *
	 * @since 0.2.0-dev
	 */
	public function prependLink($href, $attributes = [], $conditional = null)
	{
		foreach ($this->link as $item) {
			if ($href == $item['href']) {
				return $this;
			}
		}
		$this->link = array_merge([['href' => $href, 'conditional' => $conditional] + $attributes],$this->link);
		return $this;
	}

	/**
	 * Render the link tags
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function render()
	{
		$return = '';
		foreach ($this->link as $item) {
			$conditional = $item['conditional'];
			unset ($item['conditional']);
			$item['href'] = DOCROOT_CORRECT_FILES."/".trim($item['href'],'/');

			$attributes = new \uMVC\Html\Attributes();
			$attributes->set($item);
			$link = "<link {$attributes} />";

			if (!is_null($conditional)) {
				$return .= "<!--[if {$conditional}]>{$link}<![endif]-->\n";
			} else {
				$return .= "{$link}\n";
			}
		}

		return $return;
	}

	/**
	 * Convert the object to string
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}
}
