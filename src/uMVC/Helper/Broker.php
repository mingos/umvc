<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * A helper broker. Used to fetch helpers, dynamically instantiating them if needed.
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class Broker
{
	/**
	 * A static instance of the object
	 * @var \uMVC\Controller\Front
	 */
	protected static $_instance = null;

	/**
	 * Array of helpers registered in the broker
	 * @var array
	 */
	private $helpers = [];

	/**
	 * Private constructor - prevents instantiation
	 *
	 * @since 0.2.0-dev
	 */
	private function __construct()
	{}

	/**
	 * Fetch a static instance of the object
	 *
	 * @return \uMVC\Helper\Broker
	 *
	 * @since 0.2.0-dev
	 */
	public static function getInstance()
	{
		if (self::$_instance == null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Fetch the Doctype helper
	 *
	 * @return \uMVC\Helper\Doctype
	 *
	 * @since 0.3.1-dev
	 */
	public function doctype()
	{
		if (!array_key_exists("doctype",$this->helpers)) {
			$this->helpers["doctype"] = new \uMVC\Helper\Doctype();
		} else if (is_string($this->helpers["doctype"])) {
			$class = $this->helpers["doctype"];
			$this->helpers["doctype"] = new $class();
		}

		return $this->helpers["doctype"];
	}

	/**
	 * Fetch the Flash helper
	 *
	 * @return \uMVC\Helper\Flash
	 *
	 * @since 0.2.0-dev
	 */
	public function flash()
	{
		if (!array_key_exists("flash",$this->helpers)) {
			$this->helpers["flash"] = new \uMVC\Helper\Flash();
		} else if (is_string($this->helpers["flash"])) {
			$class = $this->helpers["flash"];
			$this->helpers["flash"] = new $class();
		}

		return $this->helpers["flash"];
	}

	/**
	 * Fetch the FootScript helper
	 *
	 * @return \uMVC\Helper\FootScript
	 *
	 * @since 0.3.1-dev
	 */
	public function footScript()
	{
		if (!array_key_exists("footScript",$this->helpers)) {
			$this->helpers["footScript"] = \uMVC\Helper\FootScript::getInstance();
		} else if (is_string($this->helpers["footScript"])) {
			$class = $this->helpers["footScript"];
			$this->helpers["footScript"] = new $class();
		}

		return $this->helpers["footScript"];
	}

	/**
	 * Fetch the FootScript helper
	 *
	 * @return \uMVC\Helper\FootScriptSrc
	 *
	 * @since 0.3.1-dev
	 */
	public function footScriptSrc()
	{
		if (!array_key_exists("footScriptSrc",$this->helpers)) {
			$this->helpers["footScriptSrc"] = \uMVC\Helper\FootScriptSrc::getInstance();
		} else if (is_string($this->helpers["footScriptSrc"])) {
			$class = $this->helpers["footScriptSrc"];
			$this->helpers["footScriptSrc"] = new $class();
		}

		return $this->helpers["footScriptSrc"];
	}

	/**
	 * Fetch the Gravatar helper
	 *
	 * @return \uMVC\Helper\Gravatar
	 *
	 * @since 0.2.0-dev
	 */
	public function gravatar()
	{
		if (!array_key_exists("gravatar",$this->helpers)) {
			$this->helpers["gravatar"] = new \uMVC\Helper\Gravatar();
		} else if (is_string($this->helpers["gravatar"])) {
			$class = $this->helpers["gravatar"];
			$this->helpers["gravatar"] = new $class();
		}

		return $this->helpers["gravatar"];
	}

	/**
	 * Fetch the HeadLink helper
	 *
	 * @return \uMVC\Helper\HeadLink
	 *
	 * @since 0.2.0-dev
	 */
	public function headLink()
	{
		if (!array_key_exists("headLink",$this->helpers)) {
			$this->helpers["headLink"] = \uMVC\Helper\HeadLink::getInstance();
		} else if (is_string($this->helpers["headLink"])) {
			$class = $this->helpers["headLink"];
			$this->helpers["headLink"] = new $class();
		}

		return $this->helpers["headLink"];
	}

	/**
	 * Fetch the HeadMeta helper
	 *
	 * @return \uMVC\Helper\HeadMeta
	 *
	 * @since 0.2.0-dev
	 */
	public function headMeta()
	{
		if (!array_key_exists("headMeta",$this->helpers)) {
			$this->helpers["headMeta"] = \uMVC\Helper\HeadMeta::getInstance();
		} else if (is_string($this->helpers["headMeta"])) {
			$class = $this->helpers["headMeta"];
			$this->helpers["headMeta"] = new $class();
		}

		return $this->helpers["headMeta"];
	}

	/**
	 * Fetch the HeadScript helper
	 *
	 * @return \uMVC\Helper\HeadScript
	 *
	 * @since 0.3.1-dev
	 */
	public function headScript()
	{
		if (!array_key_exists("headScript",$this->helpers)) {
			$this->helpers["headScript"] = \uMVC\Helper\HeadScript::getInstance();
		} else if (is_string($this->helpers["headScript"])) {
			$class = $this->helpers["headScript"];
			$this->helpers["headScript"] = new $class();
		}

		return $this->helpers["headScript"];
	}

	/**
	 * @var \uMVC\Helper\HeadScriptSrc
	 */
	private $headScriptSrc;

	/**
	 * Fetch the HeadScriptSrc helper
	 *
	 * @return \uMVC\Helper\HeadScriptSrc
	 *
	 * @since 0.3.1-dev
	 */
	public function headScriptSrc()
	{
		if (!array_key_exists("headScriptSrc",$this->helpers)) {
			$this->helpers["headScriptSrc"] = \uMVC\Helper\HeadScriptSrc::getInstance();
		} else if (is_string($this->helpers["headScriptSrc"])) {
			$class = $this->helpers["headScriptSrc"];
			$this->helpers["headScriptSrc"] = new $class();
		}

		return $this->helpers["headScriptSrc"];
	}

	/**
	 * Fetch the HeadStyle helper
	 *
	 * @return \uMVC\Helper\HeadStyle
	 *
	 * @since 0.3.1-dev
	 */
	public function headStyle()
	{
		if (!array_key_exists("headStyle",$this->helpers)) {
			$this->helpers["headStyle"] = \uMVC\Helper\HeadStyle::getInstance();
		} else if (is_string($this->helpers["headStyle"])) {
			$class = $this->helpers["headStyle"];
			$this->helpers["headStyle"] = new $class();
		}

		return $this->helpers["headStyle"];
	}

	/**
	 * Fetch the HeadTitle helper
	 *
	 * @return \uMVC\Helper\HeadTitle
	 *
	 * @since 0.3.1-dev
	 */
	public function headTitle()
	{
		if (!array_key_exists("headTitle",$this->helpers)) {
			$this->helpers["headTitle"] = \uMVC\Helper\HeadTitle::getInstance();
		} else if (is_string($this->helpers["headTitle"])) {
			$class = $this->helpers["headTitle"];
			$this->helpers["headTitle"] = new $class();
		}

		return $this->helpers["headTitle"];
	}

	/**
	 * Fetch the Html helper
	 *
	 * @return \uMVC\Helper\Html
	 *
	 * @since 0.2.0-dev
	 */
	public function html()
	{
		if (!array_key_exists("html",$this->helpers)) {
			$this->helpers["html"] = new \uMVC\Helper\Html();
		} else if (is_string($this->helpers["html"])) {
			$class = $this->helpers["html"];
			$this->helpers["html"] = new $class();
		}

		return $this->helpers["html"];
	}

	/**
	 * Fetch the IniFile helper
	 *
	 * @return \uMVC\Helper\IniFile
	 *
	 * @since 0.2.0-dev
	 */
	public function iniFile()
	{
		if (!array_key_exists("iniFile",$this->helpers)) {
			$this->helpers["iniFile"] = new \uMVC\Helper\IniFile();
		} else if (is_string($this->helpers["iniFile"])) {
			$class = $this->helpers["iniFile"];
			$this->helpers["iniFile"] = new $class();
		}

		return $this->helpers["iniFile"];
	}

	/**
	 * Fetch the URL helper
	 *
	 * @return \uMVC\Helper\Url
	 *
	 * @since 0.2.0-dev
	 */
	public function url()
	{
		if (!array_key_exists("url",$this->helpers)) {
			$this->helpers["url"] = new \uMVC\Helper\Url();
		} else if (is_string($this->helpers["url"])) {
			$class = $this->helpers["url"];
			$this->helpers["url"] = new $class();
		}

		return $this->helpers["url"];
	}

	/**
	 * Register a custom helper class
	 *
	 * @param string $name Helper's name, e.g. "myHelper"
	 * @param string $class Helper's class name, e.g. "\Helper\MyHelper"
	 *
	 * @return \uMVC\Helper\Broker Provides a fluent interface
	 *
	 * @since 0.12.2-dev
	 */
	public function register($name, $class)
	{
		$this->helpers[$name] = $class;
		return $this;
	}

	/**
	 * Call a custom helper
	 *
	 * @param string $name Helper name
	 * @param array $arguments
	 *
	 * @return object
	 *
	 * @throws \Exception In case the requested helper doesn't exist
	 *
	 * @since 0.12.2-dev
	 */
	public function __call($name, $arguments)
	{
		if (!array_key_exists($name,$this->helpers)) {
			throw new \Exception("Helper \"{$name}\" not found.",500);
		} else if (is_string($this->helpers[$name])) {
			$class = $this->helpers[$name];
			$this->helpers[$name] = new $class();
		}

		return $this->helpers[$name];
	}
}
