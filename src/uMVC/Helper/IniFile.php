<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Alternative ini file parser, supporting multiple nesting, hexadecimal integer values
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class IniFile
{
	/**
	 * Parse an ini file.
	 *
	 * The function supports multiple nesting, as well as keywords and hexadecimal number parsing. The following file contents:
	 * <code>
	 * [section]
	 * foo = 10
	 * bar[] = 0x64
	 * bar[] = 101
	 * baz[nest] = on
	 * bat[nest][bat1] = null
	 * bat[nest][bat2] = yes
	 * bat[nest][bat3] = bat
	 * </code>
	 * will be parsed into this:
	 * <code>
	 * array (
	 *   'section' => array (
	 *     'foo' => (int)10,
	 *     'bar' => array (
	 *       0 => (int)100,
	 *       1 => (int)101
	 *     ),
	 *     'baz' => array (
	 *       'nest' => (boolean)true,
	 *     ),
	 *     'bat' => array (
	 *       'nest' => array (
	 *         'bat1' => (null),
	 *         'bat2' => (boolean)true,
	 *         'bat3' => (string)'bat'
	 *       )
	 *     )
	 *   )
	 * )
	 * </code>
	 *
	 * @param string $filename File name, with complete path, to the ini file
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function parse($filename)
	{
		$output          = [];
		$current_section = null;

		// open file
		$input = fopen($filename, 'r');

		while (($line = fgets($input)) !== false) {
			// ignore empty lines and comments
			$line = trim($line);
			if (empty($line) || substr($line, 0, 1) == ';') {
				continue;
			}

			// first, remove comments and whitespace
			$line = preg_replace('/^;$/U', '$1', $line);

			// if there are escaped semicolons, unescape them
			$line = str_replace('\;', ';', $line);

			// check if the line is a section name
			if (preg_match('/^\[.*\]$/', $line)) {
				$current_section = substr($line, 1, strlen($line) - 2);
				if (!array_key_exists($current_section, $output)) {
					$output[$current_section] = [];
				}
				continue;
			} else {
				// separate the key and value
				$ex             = explode('=', $line);
				$key            = trim($ex[0]);
				$original_value = trim(array_key_exists(1, $ex) ? $ex[1] : '');

				// pre-parse the value :)
				list($value) = parse_ini_string("0 = {$original_value}");

				// check if it's a keyword or a numeric value...
				if (in_array($value, ['1', ''])) {
					if (substr(strtolower($original_value), 0, 4) == 'true' || substr(strtolower($original_value), 0, 3) == 'yes' || substr(strtolower($original_value), 0, 2) == 'on') {
						$value = true;
					} else {
						if (substr(strtolower($original_value), 0, 5) == 'false' || substr(strtolower($original_value), 0, 2) == 'no' || substr(strtolower($original_value), 0, 3) == 'off') {
							$value = false;
						} else {
							if (substr(strtolower($original_value), 0, 4) == 'null') {
								$value = null;
							}
						}
					}
				} else {
					if (strlen($value) > 0 && !in_array(substr($original_value, 0, 1), ['"', "'"]) && preg_match('/^(\d*\.?\d+|0\d+|0x[0-9a-fA-F]+)$/', $value)
					) {
						// octal?
						if (substr($value, 0, 1) == '0') {
							// hexadecimal?
							if (substr($value, 0, 2) == '0x') {
								$value = (int)hexdec(substr($value, 2, strlen($value) - 2));
							} else {
								$value = (int)octdec($value);
							}
						} else {
							// floating point?
							if (strpos($value, '.') !== false) {
								$value = (float)$value;
							} else {
								$value = (int)$value;
							}
						}
					}
				}

				// parse the key
				// nested?
				if (preg_match('/^.+\[.*\]$/U', $key)) {
					// set initial key
					$current_key = &$output;
					if ($current_section !== null) {
						$current_key = &$output[$current_section];
					}
					// process the keys
					$keys = explode('[', $key);
					foreach ($keys as $k) {
						$k = trim($k, ']');
						if (!is_array($current_key)) {
							$current_key = [];
						}
						// is there a key?
						if (empty($k)) {
							$current_key[] = $value;
						} else {
							if (!array_key_exists($k, $current_key)) {
								$current_key[$k] = $value;
								$current_key     = &$current_key[$k];
							} else {
								$current_key = &$current_key[$k];
							}
						}
					}
				} else {
					if ($current_section !== null) {
						$output[$current_section][$key] = $value;
					} else {
						$output[$key] = $value;
					}
				}
			}
		}

		fclose($input);
		return $output;
	}
}
