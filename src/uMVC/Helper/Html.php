<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Helper;

/**
 * Class for outputting commonly used HTML
 *
 * @package Helper
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Html
{
	/**
	 * Generate a HTML link
	 *
	 * @param string $href	   The link's href attribute
	 * @param string $label	  The anchor text
	 * @param array  $attributes Additional HTML attributes, if needed
	 *
	 * @return string The HTML for the link
	 *
	 * @since 0.0.0-dev
	 */
	public function link($href, $label, $attributes = [])
	{
		if ('http' != substr($href, 0, 4)) {
			$href = DOCROOT_CORRECT . '/' . trim($href, '/');
		}
		return '<a href="' . $href . '"' . $this->attributes($attributes) . '>' . $label . '</a>';
	}

	/**
	 * Generate a form element label
	 *
	 * @param string $label	  The label's text
	 * @param string $for		The label's for attribute, if needed. Leave empty for no attribute.
	 * @param array  $attributes Additional HTML attributes, if needed
	 *
	 * @return string The HTML for the label
	 *
	 * @since 0.0.0-dev
	 */
	public function label($label, $for = '', $attributes = [])
	{
		return '<label' . (!empty($for) ? ' for="' . $for . '"' : '') . $this->attributes($attributes) . '>' . $label . '</label>';
	}

	/**
	 * Generate a form input
	 *
	 * @param string $type	   Input type attribute
	 * @param string $name	   Input name attribute
	 * @param string $value	  The input's value attribute, if needed
	 * @param array  $attributes Additional HTML attributes, if needed
	 *
	 * @return string The HTML for the input
	 *
	 * @since 0.0.0-dev
	 */
	public function input($type, $name, $value = '', $attributes = [])
	{
		return '<input type="' . $type . '"' . (!empty($name) ? ' name="' . $name . '"' : '') . $this->attributes($attributes) . ' value="' . $value . '" />';
	}

	/**
	 * Generate a textarea
	 *
	 * @param string $name	   Textarea name attribute
	 * @param string $value	  Textarea value, if needed
	 * @param array  $attributes Additional HTML attributes, if needed
	 *
	 * @return string The HTML for the textarea
	 *
	 * @since 0.0.0-dev
	 */
	public function textarea($name, $value = '', $attributes = [])
	{
		return '<textarea name="' . $name . '"' . $this->attributes($attributes) . '>' . $value . '</textarea>';
	}

	/**
	 * Generate an img tag
	 *
	 * @param string $src image src attribute
	 * @param array $attributes [optional] additional HTML attributes, if needed
	 *
	 * @return string The HTML img tag
	 *
	 * @since 0.14.0-dev
	 */
	public function img($src, $attributes = [])
	{
		return '<img src="'.$src.'" '.$this->attributes($attributes).' />';
	}

	/**
	 * Construct the HTML attributes from an array
	 *
	 * @param array $attributes
	 *
	 * @return string Attributes HTML
	 *
	 * @since 0.0.0-dev
	 */
	private function attributes($attributes)
	{
		$html = '';
		if (!empty($attributes)) {
			foreach ($attributes as $attr => $value) {
				$html .= ' ' . $attr . '="' . htmlspecialchars($value) . '"';
			}
		}
		return $html;
	}
}
