<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Image;

/**
 * Image processor adapter
 *
 * @package Image
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.16.0-dev
 */
abstract class Adapter
{
	/**
	 * The image being processed
	 * @var resource|\Imagick
	 */
	protected $_image;

	/**
	 * Image width
	 * @var integer
	 */
	protected $_width;

	/**
	 * Image height
	 * @var integer
	 */
	protected $_height;

	/**
	 * Settings array
	 * @var array
	 */
	protected $_settings;

	/**
	 * Construct a new image
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function __construct($filename, $settings = []);

	/**
	 * Set default settings and extend them with the passed in settings
	 *
	 * @param array $settings the settings array
	 *
	 * @since 0.16.0-dev
	 */
	protected function _initSettings($settings)
	{
		$this->_settings = [
			\uMVC\Image::SETTING_GRAVITY => \uMVC\Image::GRAVITY_CENTRE,
			\uMVC\Image::SETTING_COMPRESSION_QUALITY => 75
		];

		foreach ($settings as $setting => $value) {
			$this->_settings[$setting] = $value;
		}
	}

	/**
	 * Resize image without maintaining aspect ratio
	 *
	 * @param integer $w target width; null = leave at original width
	 * @param integer $h target height; null = leave at original height
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function resize($w, $h);

	/**
	 * Scale an image proportionally
	 *
	 * @param integer $w Width
	 * @param integer $h Height
	 * @param boolean $fitInside Whether to fit inside the specified dimensions
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function scale($w, $h, $fitInside = false);

	/**
	 * Crop the image using either the current gravity setting or manually specified crop start position
	 *
	 * @param integer $w
	 * @param integer $h
	 * @param null|integer $x
	 * @param null|integer $y
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function crop($w, $h, $x = null, $y = null);

	/**
	 * Crop the image with the crop start position specified as relative to the current gravity setting
	 *
	 * @param integer $w
	 * @param integer $h
	 * @param integer $x
	 * @param integer $y
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function cropRelative($w, $h, $x, $y);

	/**
	 * Output the image as file
	 *
	 * @param string $filename target file name
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function save($filename);

	/**
	 * Set gravity for cropping images
	 *
	 * @param integer $gravity
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function setGravity($gravity)
	{
		$this->_settings[\uMVC\Image::SETTING_GRAVITY] = intval($gravity);
		return $this;
	}

	/**
	 * Overlay images, placing the one passed in as parametre on top
	 *
	 * @param \uMVC\Image\Adapter $imageAdapter An image processor adapter with the desired overlay image
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function overlay($imageAdapter);

	/**
	 * Calculate where the cropping should start at
	 *
	 * @param integer $w Cropped image width
	 * @param integer $h Cropped image height
	 * @param integer $x Cropping start X position
	 * @param integer $y Cropping start Y position
	 *
	 * @since 0.16.0-dev
	 */
	protected function _calculateCropStartingPoint($w, $h, &$x, &$y)
	{
		switch ($this->_settings[\uMVC\Image::SETTING_GRAVITY]) {
			case \uMVC\Image::GRAVITY_NORTHWEST:
				$x = is_null($x) ? 0 : $x;
				$y = is_null($y) ? 0 : $y;
				break;
			case \uMVC\Image::GRAVITY_NORTH:
				$x = is_null($x) ? floor(($this->_width - $w) / 2) : $x;
				$y = is_null($y) ? 0 : $y;
				break;
			case \uMVC\Image::GRAVITY_NORTHEAST:
				$x = is_null($x) ? $this->_width - $w : $x;
				$y = is_null($y) ? 0 : $y;
				break;
			case \uMVC\Image::GRAVITY_WEST:
				$x = is_null($x) ? 0 : $x;
				$y = is_null($y) ? floor(($this->_height - $h) / 2) : $y;
				break;
			case \uMVC\Image::GRAVITY_CENTRE:
				$x = is_null($x) ? floor(($this->_width - $w) / 2) : $x;
				$y = is_null($y) ? floor(($this->_height - $h) / 2) : $y;
				break;
			case \uMVC\Image::GRAVITY_EAST:
				$x = is_null($x) ? $this->_width - $w : $x;
				$y = is_null($y) ? floor(($this->_height - $h) / 2) : $y;
				break;
			case \uMVC\Image::GRAVITY_SOUTHWEST:
				$x = is_null($x) ? 0 : $x;
				$y = is_null($y) ? $this->_height - $h : $y;
				break;
			case \uMVC\Image::GRAVITY_SOUTH:
				$x = is_null($x) ? floor(($this->_width - $w) / 2) : $x;
				$y = is_null($y) ? $this->_height - $h : $y;
				break;
			case \uMVC\Image::GRAVITY_SOUTHEAST:
				$x = is_null($x) ? $this->_width - $w : $x;
				$y = is_null($y) ? $this->_height - $h : $y;
				break;
		};
	}

	/**
	 * Fetch the processed image resource
	 *
	 * @return resource|\Imagick
	 *
	 * @since 0.16.0-dev
	 */
	public function getImage()
	{
		return $this->_image;
	}

	/**
	 * Fetch the current image width
	 *
	 * @return integer
	 *
	 * @since 0.16.0-dev
	 */
	public function getWidth()
	{
		return $this->_width;
	}

	/**
	 * Fetch the current image height
	 *
	 * @return integer
	 *
	 * @since 0.16.0-dev
	 */
	public function getHeight()
	{
		return $this->_height;
	}

	/**
	 * Get adpater type
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	abstract public function getType();
}
