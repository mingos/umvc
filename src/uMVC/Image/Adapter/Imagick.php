<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Image\Adapter;

/**
 * Image processor adapter using ImageMagick
 *
 * @package Image
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.16.0-dev
 */
class Imagick extends \uMVC\Image\Adapter {
	/**
	 * Construct a new image
	 *
	 * @since 0.16.0-dev
	 */
	public function __construct($filename, $settings = [])
	{
		$this->_image = new \Imagick($filename);
		$this->_image->stripImage();

		$this->_width = $this->_image->getImageWidth();
		$this->_height = $this->_image->getImageHeight();

		$this->_initSettings($settings);
	}

	/**
	 * Resize image without maintaining aspect ratio
	 *
	 * @param integer $w target width; null = leave at original width
	 * @param integer $h target height; null = leave at original height
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function resize($w, $h)
	{
		$w = $w > 0 ? $w : $this->_width;
		$h = $h > 0 ? $h : $this->_height;

		$this->_image->scaleImage($w, $h);
		$this->_width = $w;
		$this->_height = $h;

		return $this;
	}

	/**
	 * Scale an image proportionally
	 *
	 * @param integer $w Width
	 * @param integer $h Height
	 * @param boolean $fitInside Whether to fit inside the specified dimensions
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function scale($w, $h, $fitInside = false)
	{
		if ($w > 0 && $h > 0) {
			$proportionW = $this->_width / $w;
			$proportionH = $this->_height / $h;

			if ($proportionW > $proportionH) {
				$this->_image->scaleImage($fitInside ? $w : 0, $fitInside ? 0 : $h);
			} else {
				$this->_image->scaleImage($fitInside ? 0 : $w, $fitInside ? $h : 0);
			}
		} else {
			$this->_image->scaleImage($w, $h);
		}

		return $this;
	}

	/**
	 * Crop the image using either the current gravity setting or manually specified crop start position
	 *
	 * @param integer $w
	 * @param integer $h
	 * @param null|integer $x
	 * @param null|integer $y
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function crop($w, $h, $x = null, $y = null)
	{
		$this->_calculateCropStartingPoint($w, $h, $x, $y);
		$this->_image->cropImage($w, $h, $x, $y);
		$this->_width = $w;
		$this->_height = $h;
		return $this;
	}

	/**
	 * Crop the image with the crop start position specified as relative to the current gravity setting
	 *
	 * @param integer $w
	 * @param integer $h
	 * @param integer $x
	 * @param integer $y
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function cropRelative($w, $h, $x, $y)
	{
		$outX = null;
		$outY = null;
		$this->_calculateCropStartingPoint($w, $h, $outX, $outY);

		$x += $outX;
		$y += $outY;

		$x = max(0, min($this->_width - $w, $x));
		$y = max(0, min($this->_height - $h, $y));

		$this->_image->cropImage($w, $h, $x, $y);
		$this->_width = $w;
		$this->_height = $h;

		return $this;
	}

	/**
	 * Overlay images, placing the one passed in as parametre on top
	 *
	 * @param \uMVC\Image\Adapter $imageAdapter An image processor adapter with the desired overlay image
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function overlay($imageAdapter)
	{
		$this->_image->compositeImage($imageAdapter->scale($this->_width, $this->_height)->getImage(), \Imagick::COMPOSITE_DEFAULT, 0, 0);
		return $this;
	}

	/**
	 * Output the image as file
	 *
	 * @param string $filename target file name
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function save($filename)
	{
		$this->_image->setCompressionQuality($this->_settings[\uMVC\Image::SETTING_COMPRESSION_QUALITY]);
		$this->_image->writeImage($filename);
	}

	/**
	 * Get adpater type
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function getType()
	{
		return "Imagick";
	}
}
