<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Image\Adapter;

/**
 * Image processor adapter using GD2
 *
 * @package Image
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.16.0-dev
 */
class Gd extends \uMVC\Image\Adapter {
	/**
	 * Construct a new image
	 *
	 * @since 0.16.0-dev
	 */
	public function __construct($filename, $settings = [])
	{
		$imagesize = getimagesize($filename);
		$mimeType = isset($imagesize['mime']) ? $imagesize['mime'] : null;

		switch ($mimeType) {
			case 'image/gif':
				$this->_image = imagecreatefromgif($filename);
				break;
			case 'image/jpeg':
				$this->_image = imagecreatefromjpeg($filename);
				break;
			case 'image/png':
				$this->_image = imagecreatefrompng($filename);
				break;
			default:
				throw new \Exception("Unsupported image format.");
		}

		$this->_width = $imagesize[0];
		$this->_height = $imagesize[1];

		$this->_initSettings($settings);
	}

	/**
	 * Resize image without maintaining aspect ratio
	 *
	 * @param integer $w target width; null = leave at original width
	 * @param integer $h target height; null = leave at original height
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function resize($w, $h)
	{
		$w = $w > 0 ? $w : $this->_width;
		$h = $h > 0 ? $h : $this->_height;

		$newImage = imagecreatetruecolor($w, $h);

		imagealphablending($newImage, false);
		$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 0);
		imagefill($newImage, 0, 0, $transparent);
		imagesavealpha($newImage, true);

		imagecopyresampled($newImage, $this->_image, 0, 0, 0, 0, $w, $h, $this->_width,$this->_height);

		imagedestroy($this->_image);
		$this->_image = $newImage;
		$this->_width = $w;
		$this->_height = $h;

		return $this;
	}

	/**
	 * Scale an image proportionally
	 *
	 * @param integer $w Width
	 * @param integer $h Height
	 * @param boolean $fitInside Whether to fit inside the specified dimensions
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function scale($w, $h, $fitInside = false)
	{
		// get expected dimensions with one = 0
		if ($w > 0 && $h > 0) {
			$proportionW = $this->_width / $w;
			$proportionH = $this->_height / $h;

			if ($proportionW > $proportionH) {
				$w = $fitInside ? $w : 0;
				$h = $fitInside ? 0 : $h;
			} else {
				$w = $fitInside ? 0 : $w;
				$h = $fitInside ? $h : 0;
			}
		}

		// calculate the missing dimension
		if ($w == 0) {
			$w = floor($this->_width * $h / $this->_height);
		} else {
			$h = floor($this->_height * $w / $this->_width);
		}

		return $this->resize($w, $h);
	}

	/**
	 * Crop the image using either the current gravity setting or manually specified crop start position
	 *
	 * @param integer $w
	 * @param integer $h
	 * @param null|integer $x
	 * @param null|integer $y
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function crop($w, $h, $x = null, $y = null)
	{
		$this->_calculateCropStartingPoint($w, $h, $x, $y);

		$newImage = imagecreatetruecolor($w, $h);

		imagealphablending($newImage, false);
		$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 0);
		imagefill($newImage, 0, 0, $transparent);
		imagesavealpha($newImage, true);

		imagecopy($newImage, $this->_image, 0, 0, $x, $y, $w, $h);

		imagedestroy($this->_image);
		$this->_image = $newImage;
		$this->_width = $w;
		$this->_height = $h;

		return $this;
	}

	/**
	 * Crop the image with the crop start position specified as relative to the current gravity setting
	 *
	 * @param integer $w
	 * @param integer $h
	 * @param integer $x
	 * @param integer $y
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function cropRelative($w, $h, $x, $y)
	{
		$outX = null;
		$outY = null;
		$this->_calculateCropStartingPoint($w, $h, $outX, $outY);

		$x += $outX;
		$y += $outY;

		$x = max(0, min($this->_width - $w, $x));
		$y = max(0, min($this->_height - $h, $y));

		$newImage = imagecreatetruecolor($w, $h);

		imagealphablending($newImage, false);
		$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 0);
		imagefill($newImage, 0, 0, $transparent);
		imagesavealpha($newImage, true);

		imagecopy($newImage, $this->_image, 0, 0, $x, $y, $w, $h);

		imagedestroy($this->_image);
		$this->_image = $newImage;
		$this->_width = $w;
		$this->_height = $h;

		return $this;
	}

	/**
	 * Overlay images, placing the one passed in as parametre on top
	 *
	 * @param \uMVC\Image\Adapter $imageAdapter An image processor adapter with the desired overlay image
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function overlay($imageAdapter)
	{
		// original background image
		$background = $this->_image;
		imagealphablending($this->_image, true);
		$tw = $this->_width;
		$th = $this->_height;

		// overlay image
		$overlay = $imageAdapter->getImage();
		$sw = $imageAdapter->getWidth();
		$sh = $imageAdapter->getHeight();

		// canvas to put images together on
		$canvas = imagecreatetruecolor($tw, $th);
		imagealphablending($canvas, false);
		imagesavealpha($canvas,true);
		$transparent = imagecolorallocatealpha($canvas, 255, 255, 255, 127);
		imagefill($canvas, 0, 0, $transparent);
		imagecopy($canvas, $overlay, 0, 0, 0, 0, $sw, $sh);

		//overlay the images
		imagecopy($background, $canvas, 0, 0, 0, 0, $tw, $th);

		//clean up
		imagedestroy($canvas);
		imagedestroy($overlay);

		return $this;
	}

	/**
	 * Output the image as file
	 *
	 * @param string $filename target file name
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @throws \Exception if the file extension can't be used to correctly determine a valid file format
	 *
	 * @since 0.16.0-dev
	 */
	public function save($filename)
	{
		$format = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

		switch ($format) {
			case "jpg":
			case "jpeg":
				imagejpeg($this->_image, $filename, $this->_settings[\uMVC\Image::SETTING_COMPRESSION_QUALITY]);
				break;
			case "png":
				imagepng($this->_image, $filename);
				break;
			case "gif":
				imagegif($this->_image, $filename);
				break;
			default:
				throw new \Exception("Unsupported output format.");
		}
	}

	/**
	 * Get adpater type
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function getType()
	{
		return "Gd";
	}
}
