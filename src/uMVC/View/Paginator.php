<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\View;

/**
 * The default paginator view
 *
 * @package Paginator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.4.3-dev
 */
class Paginator extends \uMVC\View {
	/**
	 * Current page number
	 * @var int
	 */
	public $page;

	/**
	 * Previous page number
	 * @var int
	 */
	public $previous;

	/**
	 * Next page number
	 * @var int
	 */
	public $next;

	/**
	 * All the page numbers in the current range
	 * @var array
	 */
	public $pagesInRange;

	/**
	 * The number of the first page in range
	 * @var int
	 */
	public $firstInRange;

	/**
	 * The number of the last page in range
	 * @var int
	 */
	public $lastInRange;

	/**
	 * Total number of pages (or the number of the last page)
	 * @var int
	 */
	public $totalPages;

	/**
	 * The query string renderer
	 * @var \uMVC\Request\QueryString
	 */
	public $query;

	/**
	 * Default render function for the paginator view
	 *
	 * @since 0.4.3-dev
	 */
	public function render()
	{ ?>
		<?php if ($this->totalPages > 1) { ?>
			<div class="paginator">
				<?php if ($this->page > 1) { ?>
				<a href="?<?= $this->query->renderWith(["page" => 1]); ?>">&laquo;</a>
				<a href="?<?= $this->query->renderWith(["page" => $this->previous]); ?>">&lsaquo;</a>
				<?php } else { ?>
				<span>&laquo;</span>
				<span>&lsaquo;</span>
				<?php } ?>

				<?php foreach ($this->pagesInRange as $p) { ?>
				<?php if ($p == $this->page) { ?>
					<span><?= $p; ?></span>
					<?php } else { ?>
					<a href="?<?= $this->query->renderWith(["page" => $p]); ?>"><?= $p; ?></a>
					<?php } ?>
				<?php } ?>

				<?php if ($this->page < $this->totalPages) { ?>
				<a href="?<?= $this->query->renderWith(["page" => $this->next]); ?>">&rsaquo;</a>
				<a href="?<?= $this->query->renderWith(["page" => $this->totalPages]); ?>">&raquo;</a>
				<?php } else { ?>
				<span>&rsaquo;</span>
				<span>&raquo;</span>
				<?php } ?>
			</div>
		<?php } ?>
	<?php }
}
