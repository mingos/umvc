<?php
/* uMVC
* Copyright (c) 2012-2013 Dominik Marczuk
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * The name of Dominik Marczuk may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace uMVC\View\Form;

/**
 * HTML form fieldset
 *
 * @package Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.4.4-dev
 */
class Fieldset extends \uMVC\View {
	/**
	 * Fieldset legend
	 * @var string
	 */
	public $legend;

	/**
	 * Form elements
	 * @var array
	 */
	public $elements;

	/**
	 * Render the fieldset
	 *
	 * @since 0.4.4-dev
	 */
	public function render()
	{
?>
	<fieldset>
		<?php if (!empty($this->legend)) { ?>
		<legend><?= $this->legend; ?></legend>
		<?php } ?>
		<?php foreach ($this->elements as $element) { ?>
			<?= $element->render(); ?>
		<?php } ?>
	</fieldset>
<?php
	}
}
