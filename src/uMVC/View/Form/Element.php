<?php
/* uMVC
* Copyright (c) 2012-2013 Dominik Marczuk
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * The name of Dominik Marczuk may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace uMVC\View\Form;

/**
 * HTML form element
 *
 * @package Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.4.4-dev
 */
class Element extends \uMVC\View {
	/**
	 * The element object
	 * @var \uMVC\Form\Element
	 */
	public $element;

	/**
	 * Render the form element
	 *
	 * @since 0.4.4-dev
	 */
	public function render()
	{
		$errors = $this->element->getErrorsCount();
		$required = $this->element->isRequired();
		$label = $this->element->getLabel();
		$description = $this->element->getDescription();
?>
	<div class="element<?php if ($errors) { echo ' error'; } if ($required) { echo ' required'; } ?>">
		<?php if ($label) { ?>
		<div class="label<?php if ($errors) { echo ' error'; } if ($required) { echo ' required'; } ?>">
			<?php echo $this->element->getLabelHtml(); ?>
		</div>
		<?php } ?>

		<div class="input<?php if ($errors) { echo ' error'; } if ($required) { echo ' required'; } ?>">
			<?= $this->element->getElementHtml(); ?>
		</div>

		<?php if ($description) { ?>
		<p class="description"><?= $description; ?></p>
		<?php } ?>

		<?php if ($errors) { ?>
		<ul class="errors">
			<?php foreach ($this->element->getErrors() as $e) { ?>
			<li><?= $e; ?></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</div>
<?php
	}
}
