<?php
/* uMVC
* Copyright (c) 2012-2013 Dominik Marczuk
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * The name of Dominik Marczuk may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace uMVC\View;

/**
 * Debug data output
 *
 * @package Core
 * @subpackage View
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.4.4-dev
 */
class Debug extends \uMVC\View {
	/**
	 * Bootstrap execution time
	 * @var float
	 */
	public $bootstrap;

	/**
	 * Routing time
	 * @var float
	 */
	public $route;

	/**
	 * Module-specific bootstrap execution time
	 * @var float
	 */
	public $moduleBootstrap;

	/**
	 * Dispatching time
	 * @var float
	 */
	public $dispatch;

	/**
	 * Rendering time
	 * @var float
	 */
	public $render;

	/**
	 * Total processing and rendering time
	 * @var float
	 */
	public $page;

	/**
	 * Database communications times
	 * @var array
	 */
	public $sql;

	/**
	 * Autoloaded classes and their respective files
	 * @var array
	 */
	public $files;

	/**
	 * Peak memory usage, in bytes
	 * @var int
	 */
	public $memory;

	/**
	 * Render the debug output
	 *
	 * @since 0.4.4-dev
	 */
	public function render()
	{
?>
	<table style="color:black;font:normal 10px monospace;border-collapse:collapse;margin:0 auto;">
		<tr>
			<th style="background-color:black;color:white;border:1px solid black;" colspan="3">Memory usage</th>
		</tr>
		<tr>
			<td style="background-color:white;border:1px solid black;" colspan="3"><?php echo $this->memory / 1024; ?> kiB</td>
		</tr>
		<tr>
			<th style="background-color:black;color:white;border:1px solid black;" colspan="3">Page generation</th>
		</tr>
		<tr>
			<td style="background-color:white;border:1px solid black;" colspan="2">Bootstrapping</td>
			<td style="background-color:white;border:1px solid black;"><?php echo number_format(floatval($this->bootstrap), 5); ?> s.</td>
		</tr>
		<tr>
			<td style="background-color:white;border:1px solid black;" colspan="2">URI resolving</td>
			<td style="background-color:white;border:1px solid black;"><?php echo number_format(floatval($this->route), 5); ?> s.</td>
		</tr>
		<tr>
			<td style="background-color:white;border:1px solid black;" colspan="2">Module-specific bootstrapping</td>
			<td style="background-color:white;border:1px solid black;"><?php echo number_format(floatval($this->moduleBootstrap), 5); ?> s.</td>
		</tr>
		<tr>
			<td style="background-color:white;border:1px solid black;" colspan="2">Request dispatching</td>
			<td style="background-color:white;border:1px solid black;"><?php echo number_format(floatval($this->dispatch), 5); ?> s.</td>
		</tr>
		<tr>
			<td style="background-color:white;border:1px solid black;" colspan="2">Layout and view rendering</td>
			<td style="background-color:white;border:1px solid black;"><?php echo number_format(floatval($this->render), 5); ?> s.</td>
		</tr>
		<tr>
			<td style="background-color:#cccccc;border:1px solid black;" colspan="2">Total processing and rendering time</td>
			<td style="background-color:#cccccc;border:1px solid black;"><?php echo number_format(floatval($this->page), 5); ?> s.</td>
		</tr>
		<?php if (!empty($this->sql)) { $dbTime = 0; ?><tr>
			<th style="background-color:black;color:white;border:1px solid black;" colspan="3">SQL</th>
		</tr><?php } ?>
		<?php foreach ($this->sql as $item) { $dbTime += $item['time']; ?><tr>
			<td style="background-color:white;border:1px solid black;"><?php echo $item['query']; ?></td>
			<td style="background-color:white;border:1px solid black;"><?php echo $item['params']; ?></td>
			<td style="background-color:white;border:1px solid black;"><?php echo number_format(floatval($item['time']), 5); ?> s.</td>
		</tr><?php } ?>
		<?php if (!empty($this->sql)) { ?><tr>
			<td style="background-color:#cccccc;border:1px solid black;" colspan="2">Total database communication time</td>
			<td style="background-color:#cccccc;border:1px solid black;"><?php echo number_format(floatval($dbTime), 5); ?> s.</td>
		</tr><?php } ?>
		<?php if (!empty($this->files)) { ?><tr>
			<th style="background-color:black;color:white;border:1px solid black;" colspan="3">Autoloaded classes</th>
		</tr><?php } ?>
		<?php foreach ($this->files as $item) { ?><tr>
			<td style="background-color:white;border:1px solid black;"><?php echo $item['class']; ?></td>
			<td style="background-color:white;border:1px solid black;" colspan="2"><?php echo $item['file']; ?></td>
		</tr><?php } ?>
		<?php if (!empty($this->files)) { ?><tr>
			<td style="background-color:#cccccc;border:1px solid black;" colspan="2">Total autoloaded files</td>
			<td style="background-color:#cccccc;border:1px solid black;"><?php echo count($this->files); ?></td>
		</tr><?php } ?>
	</table>
<?php
	}
}
