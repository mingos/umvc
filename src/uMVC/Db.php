<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Base class for all classes requiring the use of a database connection
 *
 * @package Core
 * @subpackage Db
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
trait Db {
	/**
	 * The database adapter
	 * @var \PDO
	 */
	private $db = null;

	/**
	 * Connect to the database or plug into the already established connection
	 *
	 * @param string $config Name of the database configuration key in the config
	 *
	 * @throws \Exception in case there is no database configuration
	 *
	 * @since 0.10.0-dev
	 */
	public function connect($config = 'db')
	{
		// if the database has already been set up, simply return
		if (null !== $this->db) {
			return;
		}

		if (is_string($config)) {
			$this->db = \uMVC\Registry::get("uMVC_Db_Connection_{$config}");
		}

		if (null === $this->db) {
			if (is_string($config)) {
				$dbconfig = \uMVC\Config::get($config);
			} else {
				$dbconfig = [];
			}

			if ($dbconfig) {
				$this->db = $this->getConnection($dbconfig);
				\uMVC\Registry::set("uMVC_Db_Connection_{$config}",$this->db);
			} else {
				throw new \Exception(__METHOD__.': No database configuration.', 500);
			}
		}
	}

	/**
	 * Quote a value for use in a SQL query
	 *
	 * @param mixed $value The value to be quoted
	 * @param int $paramType
	 *
	 * @return string Quoted value
	 *
	 * @since 0.0.0-dev
	 */
	public function quote($value, $paramType = null)
	{
		if ($value instanceof \uMVC\Db\Expr) {
			return strval($value);
		} else {
			$this->connect();

			if (is_array($value)) {
				$return = [];
				foreach ($value as $val) {
					$return[] = $this->db->quote(strval($val), $paramType);
				}
				return implode(",",$return);
			} else {
				return $this->db->quote(strval($value), $paramType);
			}
		}
	}

	/**
	 * Quote a value into a string. The quoted value will be put in place of the "?" placeholder(s).
	 *
	 * @param string $string The string containing at least one "?" placeholder
	 * @param mixed $value The value to replace the placeholder(s) after being quoted
	 * @param int $paramType
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function quoteInto($string, $value, $paramType = null)
	{
		return str_replace("?", $this->quote($value, $paramType), $string);
	}

	/**
	 * Execute a SQL query, with optional parametres
	 *
	 * @param string $sql SQL query, with parametre placeholders (?) or named parametres (":name")
	 * @param array $bind Parametres bound to the query
	 *
	 * @return \PDOStatement
	 *
	 * @since 0.0.0-dev
	 */
	public function query($sql, $bind = [])
	{
		$this->connect();

		$stmt = $this->db->prepare($sql);

		$t = microtime(true);
		$stmt->execute($bind);
		$t = microtime(true) - $t;

		if (uMVC_DEBUG) {
			$debugSql = \uMVC\Request::getInstance()->getSession('uMVC_DEBUG_SQL');
			if (null === $debugSql) {
				$debugSql = [];
			}
			$debugSql[] = ['query' => $stmt->queryString, 'params' => $this->prepareParamsString($bind), 'time' => $t];
			\uMVC\Request::getInstance()->setSession('uMVC_DEBUG_SQL',$debugSql);
		}

		return $stmt;
	}

	/**
	 * Fetch a single row
	 *
	 * @param string|\uMVC\Db\Select $sql The SQL query
	 * @param array $bind [optional] Bind parametres
	 *
	 * @return array
	 *
	 * @since 0.12.0-dev
	 */
	public function fetch($sql, $bind = [])
	{
		$sql = strval($sql);
		return $this->query($sql, $bind)->fetch();
	}

	/**
	 * Fetch a single row
	 *
	 * @param string|\uMVC\Db\Select $sql The SQL query
	 * @param array $bind [optional] Bind parametres
	 *
	 * @return array
	 *
	 * @since 0.12.0-dev
	 */
	public function fetchRow($sql, $bind = [])
	{
		return $this->fetch($sql, $bind);
	}

	/**
	 * Fetch a single column
	 *
	 * @param string|\uMVC\Db\Select $sql The SQL query
	 * @param array $bind [optional] Bind parametres
	 *
	 * @return array
	 *
	 * @since 0.12.0-dev
	 */
	public function fetchOne($sql, $bind = [])
	{
		$sql = strval($sql);
		return $this->query($sql, $bind)->fetchColumn();
	}

	/**
	 * Fetch all rows
	 *
	 * @param string|\uMVC\Db\Select $sql The SQL query
	 * @param array $bind [optional] Bind parametres
	 *
	 * @return array
	 *
	 * @since 0.12.0-dev
	 */
	public function fetchAll($sql, $bind = [])
	{
		$sql = strval($sql);
		return $this->query($sql, $bind)->fetchAll();
	}

	/**
	 * Fetch an array of all tables in the database
	 *
	 * @return array
	 *
	 * @since 0.14.0-dev
	 */
	public function showTables()
	{
		$tables = $this->fetchAll("SHOW TABLES");
		$return = [];
		foreach ($tables as $table) {
			foreach ($table as $name) {
				$return[] = $name;
			}
		}
		return $return;
	}

	/**
	 * Check whether a database table exists
	 *
	 * @param string $name Table name
	 *
	 * @return boolean
	 *
	 * @since 0.14.0-dev
	 */
	public function tableExists($name)
	{
		$table = $this->fetch("SHOW TABLES LIKE :name",["name" => $name]);
		return !empty($table);
	}

	/**
	 * Output a string containing the used bound parametres
	 *
	 * @param array $params Bound parametres
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	private function prepareParamsString($params)
	{
		$this->connect();

		$return = [];
		foreach ($params as $k => $v) {
			$return[] = "{$k} = ".$this->db->quote($v);
		}
		return implode(', ',$return);
	}

	/**
	 * Create a new database connection via PDO
	 *
	 * @param array $config Database configuration array
	 *
	 * @return \PDO The connection
	 *
	 * @throws \Exception if the configuration contains errors
	 *
	 * @since 0.3.2-dev
	 */
	private function getConnection($config)
	{
		if (!array_key_exists('dbname',$config)) {
			throw new \Exception(__METHOD__.": Missing database name in db configuration.",500);
		}
		if (!array_key_exists('host',$config)) {
			throw new \Exception(__METHOD__.": Missing host in db configuration.",500);
		}
		if (!array_key_exists('user',$config)) {
			throw new \Exception(__METHOD__.": Missing user in db configuration.",500);
		}
		if (!array_key_exists('password',$config)) {
			throw new \Exception(__METHOD__.": Missing password in db configuration.",500);
		}

		if (!array_key_exists("encoding",$config)) {
			$config["encoding"] = "UTF8";
		}

		$t = microtime(true);
		$connection = new \PDO("mysql:dbname={$config['dbname']};host={$config['host']}", $config['user'], $config['password'], [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '{$config["encoding"]}'"]);
		$connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
		$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$t = microtime(true) - $t;

		if (uMVC_DEBUG) {
			$debugSql = \uMVC\Request::getInstance()->getSession('uMVC_DEBUG_SQL');
			if (null === $debugSql) {
				$debugSql = [];
			}
			$debugSql[] = ['query' => "Establish connection with {$config['dbname']} @ {$config['host']}", 'params' => '', 'time' => $t];
			\uMVC\Request::getInstance()->setSession('uMVC_DEBUG_SQL',$debugSql);
		}

		return $connection;
	}

	/**
	 * Fetch the PDO instance
	 *
	 * @return \PDO
	 *
	 * @since 0.5.1-dev
	 */
	public function getDb()
	{
		$this->connect();
		return $this->db;
	}

	/**
	 * Fetch the ID of the last inserted row or sequence value
	 *
	 * @param string $name Name of the sequence object from which the ID should be returned.
	 *
	 * @return string
	 *
	 * @since 0.5.1-dev
	 */
	public function lastInsertId($name = null)
	{
		$this->connect();
		return $this->db->lastInsertId($name);
	}
}
