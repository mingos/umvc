<?php
/* uMVC
* Copyright (c) 2012-2013 Dominik Marczuk
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * The name of Dominik Marczuk may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace uMVC\Form;

/**
 * Fieldset form extension
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Fieldset extends \uMVC\Form {
	/**
	 * Fieldset legend
	 * @var string
	 */
	private $legend;

	/**
	 * The index inside the elements array (useful only for derived classes: Fieldset and Array)
	 * @var string
	 */
	private $name = '';

	/**
	 * The constructor
	 *
	 * @param string $name The fieldset's index name in the elements array (must differ from the element names)
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct($name)
	{
		$this->name = strval($name);
		parent::__construct();
	}

	/**
	 * Initialise the properties
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{}

	/**
	 * Set the fieldset legend
	 *
	 * @param string $legend The legend
	 *
	 * @return \uMVC\Form\Fieldset Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setLegend($legend)
	{
		$this->legend = strval($legend);
		return $this;
	}

	/**
	 * Fetch the fieldset's legend
	 *
	 * @return string The legend
	 *
	 * @since 0.0.0-dev
	 */
	public function getLegend()
	{
		return $this->legend;
	}

	/**
	 * Fetch the fieldset's name
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Fetch the fieldset's full name
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getFullName()
	{
		return $this->name;
	}

	/**
	 * Render the fieldset along with all the elements contained within
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function render()
	{
		$view = $this->getView();

		if (null === $view) {
			$view = new \uMVC\View\Form\Fieldset();
		}
		$view->elements = $this->getElements();
		$view->legend   = $this->legend;

		ob_start();
		$view->render();
		return ob_get_clean();
	}
}
