<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Form;

/**
 * Base class for form elements
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
abstract class Element
{
	/**
	 * Array indices the element belongs to
	 * @var array
	 */
	private $arrayIndices = [];

	/**
	 * The element's HTML attributes
	 * @var \uMVC\Html\Attributes
	 */
	private $attributes;

	/**
	 * Element description
	 * @var string
	 */
	private $description = "";

	/**
	 * Array of error messages
	 * @var array
	 */
	private $errors = [];

	/**
	 * Array of filters attached to the element
	 * @var array
	 */
	private $filters = [];

	/**
	 * Element's tag text
	 * @var string
	 */
	private $label = "";

	/**
	 * Element's name
	 * @var string
	 */
	private $name = "";

	/**
	 * Whether the name attribute should be rendered in HTML
	 * @var boolean
	 */
	private $nameRender = true;

	/**
	 * Whether the element is required
	 * @var boolean
	 */
	private $required = false;

	/**
	 * Array of validators attached ot the element
	 * @var array
	 */
	private $validators = [];

	/**
	 * Element's value
	 * @var string
	 */
	private $value = "";

	/**
	 * The view to render the element
	 * @var \uMVC\View\Form\Element
	 */
	private $view = null;

	/**
	 * Element instantiation and initialisation
	 *
	 * @param string $name Element's name attribute
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct($name)
	{
		$this->attributes = new \uMVC\Html\Attributes();
		$this->setName($name);
		$this->_init();
	}

	/**
	 * Render the element
	 *
	 * @return string The prepared HTML
	 *
	 * @since 0.0.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Element intialisation
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{}

	/**
	 * Adds an additional value to an HTML attribute
	 *
	 * @param string $name  The attribute's name
	 * @param string $value The attribute's value
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addAttr($name, $value)
	{
		$this->attributes->add($name, $value);
		return $this;
	}

	/**
	 * Add an error to the errors stack
	 *
	 * @param string $message Error message
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.5.3-dev
	 */
	public function addError($message)
	{
		$this->errors[] = strval($message);
		return $this;
	}

	/**
	 * Add a filter to the element's filters stack
	 *
	 * @param \uMVC\Filter $filter
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addFilter(\uMVC\Filter $filter)
	{
		$this->filters[] = $filter;
		return $this;
	}

	/**
	 * Add a validator to the element's validators set
	 *
	 * @param \uMVC\Validator|string|integer $name Either the validator object or a (named) index
	 * @param \uMVC\Validator $validator If a string name was specified, the validator object
	 *
	 * @throws \Exception if invalid arguments are passed in
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addValidator($name, $validator = null)
	{
		if ($validator === null) {
			$validator = $name;
		}

		if (!($validator instanceof \uMVC\Validator)) {
			throw new \Exception("Invalid arguments: expected a validator object.");
		}

		if (is_string($name) || is_int($name)) {
			$this->validators[$name] = $validator;
		} else {
			$this->validators[] = $validator;
		}

		return $this;
	}

	/**
	 * Remove a validator from the element's validator set
	 *
	 * @param integer|string $index Validator index in the validator set
	 *
	 * @throws \Exception if an invalid index is specified
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function removeValidator($index)
	{
		if (array_key_exists($index, $this->validators)) {
			unset($this->validators[$index]);
		} else {
			throw new \Exception("No validator exists for the specified index value.");
		}

		return $this;
	}

	/**
	 * Fetch a specific validator from the element's validator set
	 *
	 * @param integer|string $index validator index in the validator set
	 *
	 * @throws \Exception if an invalid index is specified
	 *
	 * @return \uMVC\Validator
	 *
	 * @since 0.16.0-dev
	 */
	public function getValidator($index)
	{
		if (array_key_exists($index, $this->validators)) {
			return $this->validators[$index];
		} else {
			throw new \Exception("No validator exists for the specified index value.");
		}
	}

	/**
	 * Fetch the item's HTML attributes as an attributes object
	 *
	 * @return \uMVC\Html\Attributes
	 *
	 * @since 0.0.0-dev
	 */
	public function attr()
	{
		return $this->attributes;
	}

	/**
	 * Fetch the indices of the array the element belongs to
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getArrayIndices()
	{
		return $this->arrayIndices;
	}

	/**
	 * Fetch a single HTML attribute or all attributes from the element.
	 *
	 * @param string|null $name The attribute's name; if null, all attributes will be fetched as an array
	 *
	 * @return array|null
	 *
	 * @since 0.0.0-dev
	 */
	public function getAttr($name = null)
	{
		return $this->attributes->getArray($name);
	}

	/**
	 * Set the element's description
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Prepare the HTML for the form element tag
	 *
	 * @return string The prepared HTML
	 *
	 * @since 0.0.0-dev
	 */
	abstract public function getElementHtml();

	/**
	 * Fetch the element's errors
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Get the number of errors
	 *
	 * @return int
	 *
	 * @since 0.5.3-dev
	 */
	public function getErrorsCount()
	{
		return count($this->errors);
	}

	/**
	 * Fetch the element's name attribute, including possible array braces
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getFullName()
	{
		$name = $this->name;
		if ($this->isArray()) {
			foreach ($this->getArrayIndices() as $index) {
				$name .= "[{$index}]";
			}
		}
		return $name;
	}

	/**
	 * Fetch the element's label text
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * Prepare the HTML for the label tag
	 *
	 * @return string The prepared HTML
	 *
	 * @since 0.0.0-dev
	 */
	public function getLabelHtml()
	{
		$id       = $this->getAttr('id');
		$label    = $this->getLabel();
		$required = $this->isRequired();
		$error    = count($this->getErrors()) > 0;

		$attribs = new \uMVC\Html\Attributes();
		if ($id) {
			$attribs->set('for', $id);
		}
		if ($required) {
			$attribs->add('class', 'required');
		}
		if ($error) {
			$attribs->add('class', 'error');
		}

		return $label ? '<label ' . $attribs->getHtml() . '>' . $label . '</label>' : '';
	}

	/**
	 * Fetch the element's name attribute
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Check whether the name rendering is on or off
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function getNameRender()
	{
		return $this->nameRender;
	}

	/**
	 * Fetch the element's unfiltered value
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getUnfilteredValue()
	{
		return $this->value;
	}

	/**
	 * Fetch the element's value. If the element has filters attached to it, the value returned will be filtered.
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getValue()
	{
		$value = $this->value;
		/**
		 * @var \uMVC\Filter $filter
		 */
		foreach ($this->filters as $filter) {
			$value = $filter->filter($value);
		}
		return $value;
	}

	/**
	 * Check if the element has a given attribute
	 *
	 * @param string $name  The attribute's name
	 * @param string $value The attribute's value. If not omitted, the method will check whether the attribute is defined AND has given value.
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function hasAttrib($name, $value = null)
	{
		return $this->attributes->exists($name, $value);
	}

	/**
	 * Check whether the element is part of an array
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isArray()
	{
		return !empty($this->arrayIndices);
	}

	/**
	 * Check if the element is required
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isRequired()
	{
		return $this->required;
	}

	/**
	 * Validate the element's value using the attached validators
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isValid()
	{
		$return = true;
		$value = $this->getUnfilteredValue();
		if ($this->isRequired() || !empty($value) || strlen($value) > 0) {
			/**
			 * @var \uMVC\Validator $validator
			 */
			foreach ($this->validators as $validator) {
				$valid = $validator->isValid($value);
				if (!$valid) {
					$return         = false;
					$this->errors[] = $validator->getError();
					if ($validator->getBreakOnFailure()) {
						break;
					}
				}
			}
		}

		return $return;
	}

	/**
	 * Remove a HTML attribute
	 *
	 * @param string $name The attribute's name
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function removeAttr($name)
	{
		$this->attributes->remove($name);
		return $this;
	}

	/**
	 * Remove the element's description
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function removeDescription()
	{
		$this->description = "";
		return $this;
	}

	/**
	 * Render the element
	 *
	 * @return string The prepared HTML
	 *
	 * @since 0.0.0-dev
	 */
	public function render()
	{
		$view = $this->getView();

		if (null === $view) {
			$view = new \uMVC\View\Form\Element();
		}
		$view->element = $this;

		ob_start();
		$view->render();
		return ob_get_clean();
	}

	/**
	 * Mark the element as belonging to an array.
	 *
	 * The method accepts any number of parametres. Each parametre will become another index name.
	 * For instance, the code <code>setName('foo')->setArrayIndices('bar','baz','')</code> will
	 * result in the element being named "foo[bar][baz][]". No parametres passed to this method result
	 * in marking the element as not belonging to an array.
	 *
	 * The indices may also be passed as a single array argument.
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setArrayIndices()
	{
		$indices = func_get_args();

		if (count($indices) == 1 && is_array($indices[0])) {
			$indices = $indices[0];
		}

		foreach ($indices as $key => $index) {
			$indices[$key] = strval($index);
		}

		$this->arrayIndices = $indices;

		return $this;
	}

	/**
	 * Sets a single HTML attribute
	 *
	 * @param string	   $name  The attribute's name
	 * @param array|string $value The attribute's value
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAttr($name, $value = [])
	{
		$this->attributes->set($name, $value);
		return $this;
	}

	/**
	 * Set the element's description
	 *
	 * @param string $description The description.
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setDescription($description)
	{
		$this->description = strval($description);
		return $this;
	}

	/**
	 * Set the element's label's text
	 *
	 * @param string $label
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setLabel($label)
	{
		$this->label = strval($label);
		return $this;
	}

	/**
	 * Set the element's name attribute
	 *
	 * @param string $name
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @throws \Exception
	 *
	 * @since 0.0.0-dev
	 */
	public function setName($name)
	{
		$this->name = strval($name);

		if (empty($name) && $name !== '0') {
			throw new \Exception('The parametre must be a non-empty value.', 500);
		}

		return $this;
	}

	/**
	 * Set name attribute rendering to on or off
	 *
	 * @param boolean $nameRender
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setNameRender($nameRender)
	{
		$this->nameRender = (boolean)$nameRender;
		return $this;
	}

	/**
	 * Mark the element as required.
	 *
	 * Note that this method does not influence the element validation.
	 * It is still necessary to add the NotEmpty validator.
	 * The only influence this setting has on the element is the HTML output.
	 *
	 * @param boolean $flag Omit or <code>true</code> to set the element as required
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setRequired($flag = true)
	{
		$this->required = $flag;
		return $this;
	}

	/**
	 * Set the element's value
	 *
	 * @param string $value
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}

	/**
	 * Fetch the element's view
	 *
	 * @return \uMVC\View
	 *
	 * @since 0.4.4-dev
	 */
	public function getView()
	{
		return $this->view;
	}

	/**
	 * Set the view to render the element
	 *
	 * @param \uMVC\View $view The view to render the element
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.4.4-dev
	 */
	public function setView(\uMVC\View $view)
	{
		$this->view = $view;
		return $this;
	}

	/**
	 * Reset the element's value
	 *
	 * @return \uMVC\Form\Element Provides a fluent interface
	 *
	 * @since 0.11.1-dev
	 */
	public function reset()
	{
		$this->setValue("");
		return $this;
	}
}
