<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Form\Element;

/**
 * Single checkbox
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Checkbox extends \uMVC\Form\Element {
	/**
	 * The checkbox's checked status
	 * @var boolean
	 */
	private $checked = false;

	/**
	 * Checkbox value when checked
	 * @var string
	 */
	private $checkedValue = "1";

	/**
	 * Checkbox value when unchecked
	 * @var string
	 */
	private $uncheckedValue = "0";

	/**
	 * Render the element's HTML
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getElementHtml()
	{
		$required = $this->isRequired();
		$error    = count($this->getErrors()) > 0;
		$name     = $this->getFullName();
		$checked  = $this->isChecked();

		$attributes = new \uMVC\Html\Attributes();
		$attributes->set('value', $this->uncheckedValue);
		if ($name) {
			$attributes->set('name', $name);
		}

		$html = "<input type=\"hidden\" {$attributes} />";

		$attributes
			->set($this->attr())
			->set('value', $this->checkedValue);
		if ($required) {
			$attributes
				->add('class', 'required')
				->set('required', 'required');
		}
		if ($error) {
			$attributes->add('class', 'error');
		}
		if ($checked) {
			$attributes->set('checked', 'checked');
		}
		if ($name) {
			$attributes->set('name', $name);
		}

		$html .= "<input type=\"checkbox\" {$attributes} />";

		return $html;
	}

	/**
	 * Mark the checkbox as checked
	 *
	 * @param boolean $flag Omit or <code>true</code> will mark the checkbox as checked. <code>false</code> will mark it as unchecked.
	 *
	 * @return \uMVC\Form\Element\Checkbox Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setChecked($flag = true)
	{
		$this->checked = (boolean)$flag;
		return $this;
	}

	/**
	 * Check whether the checkbox is checked or not
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isChecked()
	{
		return $this->checked;
	}

	/**
	 * Set the element's current value
	 *
	 * @param string $value
	 *
	 * @return \uMVC\Form\Element\Checkbox Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setValue($value)
	{
		if ($value == $this->checkedValue) {
			$this->checked = true;
		} else if ($value == $this->uncheckedValue) {
			$this->checked = false;
		} else {
			throw new \Exception("The checkbox cannot take the value of \"{$value}\".",500);
		}

		return $this;
	}

	/**
	 * Set the element's value when checked
	 *
	 * @param $value
	 *
	 * @return \uMVC\Form\Element\Checkbox Provides a fluent interface
	 *
	 * @since 0.12.2-dev
	 */
	public function setCheckedValue($value)
	{
		$this->checkedValue = strval($value);
		return $this;
	}

	/**
	 * Set the element's value when unchecked
	 *
	 * @param $value
	 *
	 * @return \uMVC\Form\Element\Checkbox Provides a fluent interface
	 *
	 * @since 0.12.2-dev
	 */
	public function setUncheckedValue($value)
	{
		$this->uncheckedValue = strval($value);
		return $this;
	}

	/**
	 * Get the element's value when checked
	 *
	 * @return string
	 *
	 * @since 0.12.2-dev
	 */
	public function getCheckedValue()
	{
		return $this->checkedValue;
	}

	/**
	 * Get the element's value when unchecked
	 *
	 * @return string
	 *
	 * @since 0.12.2-dev
	 */
	public function getUncheckedValue()
	{
		return $this->uncheckedValue;
	}

	/**
	 * Get the checked status
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getValue()
	{
		return $this->checked ? $this->checkedValue : $this->uncheckedValue;
	}

	/**
	 * Get the checked status
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function getUnfilteredValue()
	{
		return $this->checked;
	}
}
