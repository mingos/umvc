<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Form\Element;

/**
 * Select box
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Select extends \uMVC\Form\Element {
	/**
	 * The options and optgroups available in the select
	 * @var array
	 */
	private $options = [];

	/**
	 * All available option values
	 * @var array
	 */
	private $keys = [];

	/**
	 * Add a new option
	 *
	 * @param string|\uMVC\Form\Element\Select\Option $option The name of the option to add or an option object
	 * @param string $value The option's value, in case the first parametre is a string
	 *
	 * @return \uMVC\Form\Element\Select Provides a fluent interface
	 *
	 * @throws \Exception in case an already existent option value is chosen
	 *
	 * @since 0.0.0-dev
	 */
	public function addOption($option, $value = null)
	{
		// add an option object
		if ($option instanceof \uMVC\Form\Element\Select\Option) {
			$key = $option->getValue();
			if (in_array($key,$this->keys)) {
				throw new \Exception(__METHOD__.": The value '{$key}' already exists in the select element.",500);
			} else {
				$this->keys[$key] = $key;
			}
			$this->options[$key] = $option;
		// create an option object
		} else {
			$this->addOption((null === $value) ? new \uMVC\Form\Element\Select\Option(strval($option)) : new \uMVC\Form\Element\Select\Option(strval($option),strval($value)));
		}
		return $this;
	}

	/**
	 * Add an optgroup
	 *
	 * @param string|\uMVC\Form\Element\Select\Optgroup $optgroup The name of the optgroup to add or an optgroup object
	 * @param array $options In case the first parametre is a string, an optional array of options to add to the optgroup (either as value=>name pairs or \uMVC\Form\Element\Select\Option objects).
	 *
	 * @return \uMVC\Form\Element\Select Provides a fluent interface
	 *
	 * @throws \Exception in case one or more options inside the optgroup have values that already exist in the select.
	 *
	 * @since 0.0.0-dev
	 */
	public function addOptgroup($optgroup, $options = [])
	{
		// add an optgroup object
		if ($optgroup instanceof \uMVC\Form\Element\Select\Optgroup) {
			$keys = $optgroup->getValues();
			foreach ($keys as $key) {
				if (array_key_exists($key,$this->keys)) {
					throw new \Exception(__METHOD__.": The value '{$key}' already exists in the select element.",500);
				} else {
					$this->keys[$key] = $key;
				}
			}
			$this->options[] = $optgroup;
			// create an optgroup object
		} else {
			$optgroup = new \uMVC\Form\Element\Select\Optgroup(strval($optgroup));
			$optgroup->addOptions($options);
			$this->addOptgroup($optgroup);
		}

		return $this;
	}

	/**
	 * Add multiple options and/or optgroups
	 *
	 * @param array $options An array of options; the options may be value=>name pairs or \uMVC\Form\Element\Select\Option objects
	 * for standalone options. To create optgroups, place the options (in any of the forms available) inside an array (the array's key
	 * will be the optgroup's label) or simply provide an \uMVC\Form\Element\Select\Optgroup object. Example:
	 * <code>
	 * $select->addOptions(array(
	 *     'foo' => 'bar', //option
	 *     'baz' => [ //optgroup
	 *         'a' => 'b' //option inside the optgroup
	 *     ]
	 * ));
	 * </code>
	 *
	 * @return \uMVC\Form\Element\Select Provides a fluent interface
	 *
	 * @throws \Exception in case the provided parametre is not an array
	 *
	 * @since 0.0.0-dev
	 */
	public function addOptions($options)
	{
		if (!is_array($options)) {
			throw new \Exception(__METHOD__." expects an array as parametre, ".gettype($options)." given.",500);
		}
		foreach ($options as $k => $v) {
			if ($v instanceof \uMVC\Form\Element\Select\Option) {
				$this->addOption($v);
			} else if ($v instanceof \uMVC\Form\Element\Select\Optgroup) {
				$this->addOptgroup($v);
			} else if (is_array($v)) {
				$optgroup = new \uMVC\Form\Element\Select\Optgroup($k);
				$optgroup->addOptions($v);
				$this->addOptgroup($optgroup);
			} else if (is_string($k)) {
				$this->addOption($k, $v);
			} else {
				$this->addOption($v);
			}
		}

		return $this;
	}

	/**
	 * Fetch an option. This will fetch options from the entire select object, regardless whether they are in an optgroup.
	 *
	 * @param string $value The option's value
	 *
	 * @return \uMVC\Form\Element\Select\Option
	 *
	 * @throws \Exception if the provided value does not exist
	 *
	 * @since 0.0.0-dev
	 */
	public function getOption($value)
	{
		$value = strval($value);
		if (!array_key_exists($value, $this->keys)) {
			throw new \Exception(__METHOD__.": Unknown option value '{$value}'.",500);
		} else if (array_key_exists($value, $this->options)) {
			return $this->options[$value];
		} else {
			foreach ($this->options as $v) {
				if ($v instanceof \uMVC\Form\Element\Select\Optgroup) {
					$option = $v->getOption($value);
					if ($option instanceof \uMVC\Form\Element\Select\Option) {
						return $option;
					}
				}
			}
		}
		throw new \Exception(__METHOD__.": Unknown option '{$value}'.",500);
	}

	/**
	 * Fetch options
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Remove an option. This works regardless of whether the option is inside an optgroup or not.
	 *
	 * @param string $value The removed option's value
	 *
	 * @return \uMVC\Form\Element\Select Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function removeOption($value)
	{
		$value = strval($value);
		if (array_key_exists($value, $this->options)) {
			unset($this->options[$value]);
		} else {
			foreach ($this->options as $v) {
				if ($v instanceof \uMVC\Form\Element\Select\Optgroup) {
					if ($v->removeOption($value)) {
						break;
					}
				}
			}
		}

		return $this;
	}

	/**
	 * Render the element's HTML
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getElementHtml()
	{
		$name = $this->getFullName();

		$attributes = new \uMVC\Html\Attributes();
		$attributes
			->set($this->attr())
			->set('name', $name);

		$options = '';
		foreach ($this->options as $option) {
			$options .= $option;
		}

		return "<select {$attributes}>\n{$options}</select>";
	}

	/**
	 * Set the element's value
	 *
	 * @param string $value The element's value. The value must be among the options.
	 *
	 * @return \uMVC\Form\Element\Select Provides a fluent interface
	 *
	 * @throws \Exception if the provided value is not among the select's options
	 *
	 * @since 0.0.0-dev
	 */
	public function setValue($value)
	{
		$curValue = $this->getValue();
		if (!empty($curValue)) {
			$option = $this->getOption($this->getValue());
			if ($option instanceof \uMVC\Form\Element\Select\Option) {
				$option->setSelected(false);
			}
		}

		$value = strval($value);
		$option = $this->getOption($value);
		if ($option instanceof \uMVC\Form\Element\Select\Option) {
			$option->setSelected();
			parent::setValue($value);
		} else {
			throw new \Exception(__METHOD__.": cannot set value '{$value}'.",500);
		}


		return $this;
	}

	/**
	 * Fetch all available option values
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getValues()
	{
		return (array_keys($this->keys));
	}
}
