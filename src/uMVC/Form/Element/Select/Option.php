<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Form\Element\Select;

/**
 * Option in a select box
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Option
{
	/**
	 * HTML attributes
	 * @var \uMVC\Html\Attributes
	 */
	private $attributes;

	/**
	 * The text displayed as the option's name
	 * @var string
	 */
	private $name;

	/**
	 * The option's value
	 * @var string
	 */
	private $value;

	/**
	 * Constructor
	 *
	 * @param string $value The option's value
	 * @param string $name The option's displayed name. If omitted, the value will be used as the name.
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct($value, $name = null)
	{
		$this->attributes = new \uMVC\Html\Attributes();

		$this->value = $this->name = strval($value);

		if (null !== $name) {
			$this->name = strval($name);
		}
	}

	/**
	 * Fetch the option's HTML attributes as an attributes object
	 *
	 * @return \uMVC\Html\Attributes
	 *
	 * @since 0.0.0-dev
	 */
	public function attr()
	{
		return $this->attributes;
	}

	/**
	 * Render the option element
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function render()
	{
		$this->attributes->set('value',$this->value);

		return "\t<option {$this->attributes}>{$this->name}</option>\n";
	}

	/**
	 * Convert the optgroup object to string
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Mark the options as selected or not selected
	 *
	 * @param bool $flag Omit or set to true to mark the option as selected. Set to false to mark the option as NOT selected.
	 *
	 * @return \uMVC\Form\Element\Select\Option Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setSelected($flag = true)
	{
		$flag = (boolean)$flag;
		if ($flag) {
			$this->attributes->set('selected','selected');
		} else {
			$this->attributes->remove('selected');
		}

		return $this;
	}

	/**
	 * Check whether an options is selected
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isSelected()
	{
		return $this->attributes->exists('selected');
	}

	/**
	 * Disable the option
	 *
	 * @param bool $flag Omit or set to true to mark the option as disabled. Set to false to mark the option as enabled.
	 *
	 * @return \uMVC\Form\Element\Select\Option Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setDisabled($flag = true)
	{
		$flag = (boolean)$flag;
		if ($flag) {
			$this->attributes->set('disabled','disabled');
		} else {
			$this->attributes->remove('disabled');
		}

		return $this;
	}

	/**
	 * Sets a single HTML attribute
	 *
	 * @param string	   $name  The attribute's name
	 * @param array|string $value The attribute's value
	 *
	 * @return \uMVC\Form\Element\Select\Option Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAttr($name, $value)
	{
		$this->attributes->set($name, $value);
		return $this;
	}

	/**
	 * Adds an additional value to an HTML attribute
	 *
	 * @param string $name  The attribute's name
	 * @param string $value The attribute's value
	 *
	 * @return \uMVC\Form\Element\Select\Option Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addAttr($name, $value)
	{
		$this->attributes->add($name, $value);
		return $this;
	}

	/**
	 * Remove an HTML attribute
	 *
	 * @param string $name The attribute's name
	 *
	 * @return \uMVC\Form\Element\Select\Option Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function removeAttr($name)
	{
		$this->attributes->remove($name);
		return $this;
	}

	/**
	 * Fetch the option's displayed name
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Fetch the option's value
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getValue()
	{
		return $this->value;
	}
}
