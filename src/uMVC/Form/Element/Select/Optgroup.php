<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Form\Element\Select;

/**
 * Optgroup in a select box
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Optgroup
{
	/**
	 * HTML attributes
	 * @var \uMVC\Html\Attributes
	 */
	private $attributes;

	/**
	 * The optgroup's label
	 * @var string
	 */
	private $label;

	/**
	 * The options available in the optgroup
	 * @var array
	 */
	private $options = [];

	/**
	 * Constructor
	 *
	 * @param string $label The optgroup's label
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct($label)
	{
		$this->attributes = new \uMVC\Html\Attributes();

		$this->label = strval($label);
	}

	/**
	 * Fetch the optgroup's HTML attributes as an attributes object
	 *
	 * @return \uMVC\Html\Attributes
	 *
	 * @since 0.0.0-dev
	 */
	public function attr()
	{
		return $this->attributes;
	}

	/**
	 * Render the optgroup's HTML
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function render()
	{
		$this->attributes->set('label',$this->label);

		$return =  "\t<optgroup {$this->attributes}>\n";
		foreach ($this->options as $option) {
			$return .= "\t{$option}";
		}
		$return .= "\t</optgroup>\n";

		return $return;
	}

	/**
	 * Convert the optgroup object to string
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Mark the optgroup as disabled
	 *
	 * @param bool $flag Omit or set to true to disable the optgroup; set to false to enable it.
	 *
	 * @return \uMVC\Form\Element\Select\Optgroup Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setDisabled($flag = true)
	{
		$flag = (boolean)$flag;
		if ($flag) {
			$this->attributes->set('disabled','disabled');
		} else {
			$this->attributes->remove('disabled');
		}

		return $this;
	}

	/**
	 * Add a new option to the optgroup
	 *
	 * @param string|\uMVC\Form\Element\Select\Option $option The name of the option to add or an option object
	 * @param string $value The option's value, in case the first parametre is a string
	 *
	 * @return \uMVC\Form\Element\Select\Optgroup Provides a fluent interface
	 *
	 * @throws \Exception in case an already existent option value is chosen
	 *
	 * @since 0.0.0-dev
	 */
	public function addOption($option, $value = null)
	{
		if ($option instanceof \uMVC\Form\Element\Select\Option) {
			$key = $option->getValue();
			if (array_key_exists($key,$this->options)) {
				throw new \Exception(get_class().": ".__METHOD__.": The value '{$key}' already exists in the optgroup.",500);
			}
			$this->options[$key] = $option;
		} else {
			$option = strval($option);
			$this->addOption((null === $value) ? new \uMVC\Form\Element\Select\Option($option) : new \uMVC\Form\Element\Select\Option($option,strval($value)));
		}

		return $this;
	}

	/**
	 * Add multiple options
	 *
	 * @param array $options An array of options; the options may either be value=>name pairs or \uMVC\Form\Element\Select\Option objects
	 *
	 * @return \uMVC\Form\Element\Select\Optgroup Provides a fluent interface
	 *
	 * @throws \Exception in case the provided parametre is not an array
	 *
	 * @since 0.0.0-dev
	 */
	public function addOptions($options)
	{
		if (!is_array($options)) {
			throw new \Exception(get_class().": ".__METHOD__." expects an array as parametre, ".gettype($options)." given.",500);
		}
		foreach ($options as $k => $v) {
			if ($v instanceof \uMVC\Form\Element\Select\Option) {
				$this->addOption($v);
			} else {
				$this->addOption($k, $v);
			}
		}

		return $this;
	}

	/**
	 * Fetch an option
	 *
	 * @param string $value The option's value
	 *
	 * @return \uMVC\Form\Element\Select\Option or null if the option is not found
	 *
	 * @since 0.0.0-dev
	 */
	public function getOption($value)
	{
		$value = strval($value);
		if (array_key_exists($value, $this->options)) {
			return $this->options[$value];
		} else {
			return null;
		}
	}

	/**
	 * Fetch all the options
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Remove an option from the optgroup
	 *
	 * @param string $name The option's value
	 *
	 * @return boolean Whether the option has been found and removed.
	 *
	 * @since 0.0.0-dev
	 */
	public function removeOption($name)
	{
		$name = strval($name);
		if (array_key_exists($name, $this->options)) {
			unset($this->options[$name]);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fetch the values of the options contained within the optgroup
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getValues()
	{
		return array_keys($this->options);
	}

	/**
	 * Fetch the optgroup's label
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getName()
	{
		return $this->getLabel();
	}

	/**
	 * Fetch the optgroup's label
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * Sets a single HTML attribute
	 *
	 * @param string	   $name  The attribute's name
	 * @param array|string $value The attribute's value
	 *
	 * @return \uMVC\Form\Element\Select\Optgroup Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setAttr($name, $value)
	{
		$this->attributes->set($name, $value);
		return $this;
	}

	/**
	 * Adds an additional value to an HTML attribute
	 *
	 * @param string $name  The attribute's name
	 * @param string $value The attribute's value
	 *
	 * @return \uMVC\Form\Element\Select\Optgroup Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addAttr($name, $value)
	{
		$this->attributes->add($name, $value);
		return $this;
	}

	/**
	 * Remove a HTML attribute
	 *
	 * @param string $name The attribute's name
	 *
	 * @return \uMVC\Form\Element\Select\Optgroup Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function removeAttr($name)
	{
		$this->attributes->remove($name);
		return $this;
	}
}
