<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Form\Element;

/**
 * Multiple select box
 *
 * @package	Form
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class MultiSelect extends \uMVC\Form\Element\Select {
	/**
	 * Get the indices of the array the element name belongs to
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getArrayIndices()
	{
		$indices = parent::getArrayIndices();
		$last = end($indices); reset($indices);
		if (count($indices) == 0 || $last != '') {
			$indices[] = '';
		}
		return $indices;
	}

	/**
	 * Get the element's full name
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getFullName()
	{
		$name = $this->getName();
		foreach ($this->getArrayIndices() as $index) {
			$name .= "[{$index}]";
		}
		return $name;
	}

	/**
	 * Render the element's HTML
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getElementHtml()
	{
		$name = $this->getFullName();

		$attributes = new \uMVC\Html\Attributes();
		$attributes
			->set($this->attr())
			->set('name', $name)
			->set('multiple','multiple');

		$options = '';
		foreach ($this->getOptions() as $option) {
			$options .= $option;
		}

		return "<select {$attributes}>\n{$options}</select>";
	}

	/**
	 * Set the selected values
	 *
	 * @param array $value
	 *
	 * @return \uMVC\Form\Element\MultiSelect Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setValue($value)
	{
		if (!is_array($value)) {
			$value = [$value];
		}

		foreach ($this->getOptions() as $v) {
			if ($v instanceof \uMVC\Form\Element\Select\Option) {
				$v->setSelected(in_array($v->getValue(), $value));
			} else {
				foreach ($v->getOptions() as $vv) {
					$vv->setSelected(in_array($vv->getValue(), $value));
				}
			}
		}

		return $this;
	}

	/**
	 * Get the element's values
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getValue()
	{
		$value = [];

		foreach ($this->getOptions() as $v) {
			if ($v instanceof \uMVC\Form\Element\Select\Option) {
				if ($v->isSelected()) {
					$value[] = $v->getValue();
				}
			} else {
				foreach ($v->getOptions() as $vv) {
					if ($vv->isSelected()) {
						$value[] = $vv->getValue();
					}
				}
			}
		}

		return $value;
	}
}
