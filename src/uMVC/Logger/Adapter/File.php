<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Logger\Adapter;

/**
 * Log the event data in a file
 *
 * @package Logger
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.9.0-dev
 */
class File extends \uMVC\Logger\Adapter {
	/**
	 * File name where the the log entries should be stored
	 * @var string
	 */
	private $filename = null;

	/**
	 * Set the filename where the log events should be stored
	 *
	 * @param string $filename The file name (with full path)
	 *
	 * @return \uMVC\Logger\Adapter\File Provides a fluent interface
	 *
	 * @since 0.9.0-dev
	 */
	public function setFilename($filename)
	{
		$this->filename = strval($filename);

		if (!is_file($this->filename)) {
			$dir = dirname($this->filename);
			if (!is_dir($dir)) {
				mkdir($dir,octdec('0755'),true);
			}
		}

		return $this;
	}

	/**
	 * Save the event in the log
	 *
	 * @param array $data Array of data to be stored
	 *
	 * @since 0.9.0-dev
	 */
	protected function _log($data)
	{
		if (null === $this->filename) {
			throw new \Exception(__METHOD__.": Undefined file name. Please make sure to use the 'setFilename' method when setting the logger up.",500);
		}

		$handle = fopen($this->filename,'a');
		$datetime = new \DateTime();
		fwrite($handle,"[".$datetime->format("Y-m-d, H:i:s")."]\n");
		foreach($data as $key => $val) {
			fwrite($handle,"\t{$key} = {$val}\n");
		}
		fwrite($handle,"\n");
		fclose($handle);
	}
}
