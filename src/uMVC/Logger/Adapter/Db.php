<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Logger\Adapter;

/**
 * Log the event data in the database
 *
 * @package Logger
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.9.0-dev
 */
class Db extends \uMVC\Logger\Adapter {
	use \uMVC\Db;

	/**
	 * Table name where the the log entries should be stored
	 * @var string
	 */
	private $table = null;

	/**
	 * Set the table name where the log events should be stored
	 *
	 * @param string $table The table name
	 *
	 * @return \uMVC\Logger\Adapter\Db Provides a fluent interface
	 *
	 * @since 0.9.0-dev
	 */
	public function setTable($table)
	{
		$this->table = strval($table);
		return $this;
	}

	/**
	 * Save the event in the log
	 *
	 * @param array $data Array of data to be stored
	 *
	 * @since 0.9.0-dev
	 */
	protected function _log($data)
	{
		if (null === $this->table) {
			throw new \Exception(__METHOD__.": Undefined table name. Please make sure to use the 'setTable' method when setting the logger up.",500);
		}

		$columns = array_keys($data);
		$values = $data;

		foreach ($columns as $key => $value) {
			$columns[$key] = "`{$value}`";
		}
		foreach ($values as $key => $value) {
			$values[$key] = $this->quote($value);
		}

		$columns = implode(',',$columns);
		$values = implode(',',$values);

		$this->query("INSERT INTO `{$this->table}` ({$columns}) VALUES ({$values})");
	}
}
