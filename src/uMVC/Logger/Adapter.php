<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Logger;

/**
 * Abstract log adapter. Base for all event loggers.
 *
 * @package Logger
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.9.0-dev
 */
abstract class Adapter
{
	/**
	 * Field-column map. The field names will need to be declared
	 * @var array
	 */
	protected $map = [];

	/**
	 * Add a new index map
	 *
	 * @param \uMVC\Logger\Map $map Index mapping
	 *
	 * @return \uMVC\Logger\Adapter Provides a fluent interface
	 *
	 * @since 0.9.0-dev
	 */
	public function map(\uMVC\Logger\Map $map)
	{
		$this->map[] = $map;
		return $this;
	}

	/**
	 * Log an event
	 *
	 * @param array $data Array of data to be stored
	 *
	 * @throws \Exception in case the parametre is not an array or a required field is not specified.
	 *
	 * @since 0.9.0-dev
	 */
	public function log($data)
	{
		if (!is_array($data)) {
			throw new \Exception (__METHOD__.": The parametre is expected to be an array.",500);
		}

		// prepare data:
		$event = [];
		foreach ($this->map as $item) {
			if (array_key_exists($item->getIndex(), $data)) {
				$event[$item->getColumn()] = $data[$item->getIndex()];
			} else {
				if ($item->isRequired()) {
					if (null !== $item->getDefault()) {
						$event[$item->getColumn()] = $item->getDefault();
					} else {
						throw new \Exception (__METHOD__.": The required index \"".$item->getIndex()."\" is missing.",500);
					}
				}
			}
		}

		// save the prepared data:
		$this->_log($event);
	}

	/**
	 * Save the event in the log
	 *
	 * @param array $data Array of data to be stored
	 *
	 * @since 0.9.0-dev
	 */
	abstract protected function _log($data);

	/**
	 * Enable the logger
	 *
	 * @since 0.9.0-dev
	 */
	public function enable()
	{
		\uMVC\Registry::set("uMVC_Logger_Adapter", $this);
	}
}
