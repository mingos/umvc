<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Logger;

/**
 * Mapping for a single field in the log data array
 *
 * @package Logger
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.9.0-dev
 */
class Map
{
	/**
	 * String that corresponds to the event data array index name
	 * @var string
	 */
	private $index;

	/**
	 * Whether the field is required
	 * @var boolean
	 */
	private $required;

	/**
	 * Default value in case the index is required, but not defined
	 * @var mixed
	 */
	private $default;

	/**
	 * Column name that the index will be mapped to
	 * @var string
	 */
	private $column;

	/**
	 * Construct the log index mapping
	 *
	 * @param string $index Index name
	 * @param boolean $required [optional] Whether the index is required. Defaults to false.
	 * @param mixed $default [optional] Default value to use when the field is required, but undefined.
	 * @param null|string $column [optional] Column name to map the index name to.
	 *
	 * @since 0.9.0-dev
	 */
	public function __construct($index, $required = false, $default = null, $column = null)
	{
		$this->index = strval($index);
		$this->required = (boolean)$required;
		$this->default = $default;
		$this->column = null === $column ? $$this->index : strval($column);
	}

	/**
	 * Get the index string
	 *
	 * @return string
	 *
	 * @since 0.9.0-dev
	 */
	public function getIndex()
	{
		return $this->index;
	}

	/**
	 * Get the required value
	 *
	 * @return boolean
	 *
	 * @since 0.9.0-dev
	 */
	public function isRequired()
	{
		return $this->required;
	}

	/**
	 * Get the default value
	 *
	 * @return mixed
	 *
	 * @since 0.9.0-dev
	 */
	public function getDefault()
	{
		return $this->default;
	}

	/**
	 * Get the mapped column
	 *
	 * @return string
	 *
	 * @since 0.9.0-dev
	 */
	public function getColumn()
	{
		return $this->column;
	}
}
