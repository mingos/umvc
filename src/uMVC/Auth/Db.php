<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Auth;

/**
 * Authentication support
 *
 * @package Auth
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Db
{
	use \uMVC\Db;

	/**
	 * User name
	 * @var string
	 */
	private $identity;

	/**
	 * Password
	 * @var string
	 */
	private $credential;

	/**
	 * part of the WHERE clause to process the credential
	 * @var string
	 */
	private $where = "?";

	/**
	 * Set the user name
	 *
	 * @param string $identity User name
	 *
	 * @return \uMVC\Auth\Db Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setIdentity($identity)
	{
		$this->identity = strval($identity);
		return $this;
	}

	/**
	 * Set the password
	 *
	 * @param string $credential User password
	 *
	 * @return \uMVC\Auth\Db Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setCredential($credential)
	{
		$this->credential = strval($credential);
		return $this;
	}

	/**
	 * Set the part of the WHERE clause that processes the password.
	 *
	 * The authentication launches a SQL query that looks somewhat like this:
	 * <code>
	 * SELECT * FROM `user_table` WHERE (`identity` = 'foo') AND (`credential` = 'bar') LIMIT 1;
	 * </code>
	 * The 'bar' part of the above query is what this method sets. It defaults to "?" (the ? character will be replaced by the credential string).
	 * If the credential encryption is done by the PHP script and the passed string is already encrypted, using this method is unneeded.
	 * If the encryption is done in the database, the parametre might look somewhat like this:
	 * <code>
	 * MD5(?,salt)
	 * </code>
	 * Additional clauses may be passed as well, for instance, to authenticate only users with a given status:
	 * <code>
	 * SHA1(?,salt) AND active='1'
	 * </code>
	 *
	 * @param string $where The part of the WHERE clause corresponding to the credential treatment
	 *
	 * @return \uMVC\Auth\Db Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setWhere($where)
	{
		$this->where = $where;
		return $this;
	}

	/**
	 * Performs authentication using the given user data.
	 *
	 * @param string $table			   The table name where the user data is stored
	 * @param string $identityColumn   The column name where the user identity (user name) is stored
	 * @param string $credentialColumn The column name where the user credential (encrypted password) is stored
	 *
	 * @return integer Authentication result code
	 *
	 * @since 0.0.0-dev
	 */
	public function authenticate($table, $identityColumn, $credentialColumn)
	{
		$sql = "SELECT * FROM `{$table}` WHERE (`{$identityColumn}` = ?) AND (`{$credentialColumn}` = {$this->where}) LIMIT 1";
		$identity = $this->query($sql,[$this->identity, $this->credential])->fetchAll();
		if (count($identity) == 1) {
			return\uMVC\Auth::AUTHENTICATION_OK;
		} else {
			$sql = "SELECT * FROM `{$table}` WHERE (`{$identityColumn}` = ?)";
			$identity = $this->query($sql,[$this->identity])->fetchAll();
			if (count($identity) > 1) {
				return \uMVC\Auth::AUTHENTICATION_AMBIGUOUS_IDENTITY;
			} else if (count($identity) == 0) {
				return \uMVC\Auth::AUTHENTICATION_WRONG_IDENTITY;
			} else {
				return \uMVC\Auth::AUTHENTICATION_WRONG_CREDENTIAL;
			}
		}
	}
}
