<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Translator\Adapter;

/**
 * Php array-based translator adapter<br>
 * Prepares translations based on an input array with source=>translation string pairs.
 *
 * @package Translator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.0-dev
 */
class PhpArray extends \uMVC\Translator\Adapter {
	private $data;

	public function __construct($data)
	{
		$this->setData($data);
	}

	/**
	 * Set the translation data.
	 *
	 * @param string|array $data Either the filename (with full path) of the translations file to use, or the array with the translations
	 *
	 * @throws \Exception if the data passed is neither an array nor a valid file name.
	 *
	 * @since 0.3.0-dev
	 */
	public function setData($data)
	{
		if (is_array($data)) {
			$this->data = $data;
		} else {
			if (file_exists($data)) {
				ob_start();
				$this->data = include $data;
				ob_end_clean();
				if (!is_array($this->data)) {
					throw new \Exception(__METHOD__.": File {$data} contains invalid content.",500);
				}
			} else {
				throw new \Exception(__METHOD__.": File {$data} not found.",500);
			}
		}
	}

	/**
	 * Translate a string
	 *
	 * @param string $string Original string
	 *
	 * @return string Translated string or the original string if a suitable translation is unavailable
	 *
	 * @since 0.3.0-dev
	 */
	public function translate($string)
	{
		if (func_num_args() > 1) {
			if (is_array(func_get_arg(1))) {
				$args = func_get_arg(1);
			} else {
				$args = array_slice(func_get_args(), 1);
			}
		} else {
			$args = [];
		}

		if (array_key_exists($string,$this->data)) {
			$string = $this->data[$string];
		}

		return vsprintf($string, $args);
	}

	/**
	 * Fetch the translator adapter name
	 *
	 * @return string Translator adapter name
	 *
	 * @since 0.3.0-dev
	 */
	public function getType()
	{
		return 'PhpArray';
	}
}
