<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Zend Framework
 * Copyright (c) 2005-2010, Zend Technologies USA, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *    * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *      contributors may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Translator\Adapter;

/**
 * Gettext adapter<br>
 * Parses a gettext catalogue and fetches data from it. Does not actually require the gettext PHP extension.
 *
 * @package Translator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.0-dev
 */
class Gettext extends \uMVC\Translator\Adapter {
	/**
	 * Translation data
	 * @var array
	 */
	private $data = [];

	/**
	 * The file name (with full path) containing the gettext catalogue
	 * @var string
	 */
	private $filename = null;

	/**
	 * The open file
	 * @var resource
	 */
	private $file = null;

	/**
	 * The gettext catalogue file's endianness
	 * @var bool
	 */
	private $bigEndian = false;

	/**
	 * Constructor
	 *
	 * @param string $filename The file name (with full path) of the desired translation catalogue (the .mo file)
	 *
	 * @since 0.3.0-dev
	 */
	public function __construct($filename = null)
	{
		if (null !== $filename) {
			$this->addTranslations($filename);
		}
	}

	/**
	 * Parse the translation catalogue file name
	 *
	 * @param string $filename The file name (with full path) of the desired translation catalogue (the .mo file)
	 *
	 * @return \uMVC\Translator\Adapter\Gettext Provides a fluent interface
	 *
	 * @throws \Exception when a nonexistent catalogue file name is chosen
	 *
	 * @since 0.3.0-dev
	 */
	public function addTranslations($filename)
	{
		$this->filename = $filename;

		if (!is_file($this->filename)) {
			throw new \Exception (__METHOD__.": File {$this->filename} not found.",500);
		} else if (filesize($filename) < 10) {
			throw new \Exception (__METHOD__.": File {$this->filename} is not a gettext file.",500);
		}

		$this->parseMo();

		return $this;
	}

	/**
	 * Read values from the MO file.
	 *
	 * Function originally found in Zend Framework.
	 *
	 * @param string $bytes
	 *
	 * @return array
	 *
	 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
	 * @license http://framework.zend.com/license/new-bsd New BSD License
	 *
	 * @since 0.3.0-dev
	 */
	private function readMoData($bytes)
	{
		if ($this->bigEndian === false) {
			return unpack('V' . $bytes, fread($this->file, 4 * $bytes));
		} else {
			return unpack('N' . $bytes, fread($this->file, 4 * $bytes));
		}
	}

	/**
	 * Parse the .mo file containing the translation strings.
	 *
	 * Function originally found in Zend Framework
	 *
	 * @throws \Exception in case the parsed file is not a gettext catalogue
	 *
	 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
	 * @license http://framework.zend.com/license/new-bsd New BSD License
	 *
	 * @since 0.3.0-dev
	 */
	private function parseMo()
	{
		$data            = [];
		$this->bigEndian = false;
		$this->file      = fopen($this->filename, 'rb');

		// endianness
		$input = $this->readMoData(1);
		if (strtolower(substr(dechex($input[1]), -8)) == "950412de") {
			$this->bigEndian = false;
		} else if (strtolower(substr(dechex($input[1]), -8)) == "de120495") {
			$this->bigEndian = true;
		} else {
			throw new \Exception (__METHOD__.": File {$this->filename} is not a gettext file.",500);
		}

		// read revision - not supported for now
		$input = $this->readMoData(1);

		// number of bytes
		$input = $this->readMoData(1);
		$total = $input[1];

		// number of original strings
		$input = $this->readMoData(1);
		$OOffset = $input[1];

		// number of translation strings
		$input = $this->readMoData(1);
		$TOffset = $input[1];

		// fill the original table
		fseek($this->file, $OOffset);
		$origtemp = $this->readMoData(2 * $total);
		fseek($this->file, $TOffset);
		$transtemp = $this->readMoData(2 * $total);

		for ($count = 0; $count < $total; ++$count) {
			if ($origtemp[$count * 2 + 1] != 0) {
				fseek($this->file, $origtemp[$count * 2 + 2]);
				$original = fread($this->file, $origtemp[$count * 2 + 1]);
				$original = explode("\0", $original);
			} else {
				$original[0] = '';
			}

			if ($transtemp[$count * 2 + 1] != 0) {
				fseek($this->file, $transtemp[$count * 2 + 2]);
				$translate = fread($this->file, $transtemp[$count * 2 + 1]);
				$translate = explode("\0", $translate);

				if ((count($original) > 1) && (count($translate) > 1)) {
					$data[$original[0]] = $translate;
					array_shift($original);
					foreach ($original as $orig) {
						$data[$orig] = '';
					}
				} else {
					if ($original[0] != '') {
						$data[$original[0]] = $translate[0];
					}
				}
			}
		}

		fclose($this->file);

		$this->data = array_merge($data,$this->data);
	}

	/**
	 * Translate a string
	 *
	 * @param string $string Original string
	 *
	 * @return string Translated string or the original string if a suitable translation is unavailable
	 *
	 * @since 0.3.0-dev
	 */
	public function translate($string)
	{
		if (func_num_args() > 1) {
			if (is_array(func_get_arg(1))) {
				$args = func_get_arg(1);
			} else {
				$args = array_slice(func_get_args(), 1);
			}
		} else {
			$args = [];
		}

		if (array_key_exists($string,$this->data)) {
			$string = $this->data[$string];
		}

		return vsprintf($string, $args);
	}

	/**
	 * Fetch the translator adapter name
	 *
	 * @return string Translator adapter name
	 *
	 * @since 0.3.0-dev
	 */
	public function getType()
	{
		return 'Gettext';
	}
}
