<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Application configuration
 *
 * @package Core
 * @subpackage Config
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Config
{
	/**
	 * The configuration options
	 * @var array
	 */
	private static $data;

	/**
	 * Private constructor - cannot instantiate
	 *
	 * @since 0.0.0-dev
	 */
	private function __construct()
	{
		self::$data = [];
	}

	/**
	 * Set a single parametre or an array of key-value pairs
	 *
	 * @param string|array $key
	 * @param mixed		   $value [optional]
	 */
	public static function set($key, $value = null)
	{
		if (!is_array($key)) {
			$key = [strval($key) => $value];
		}
		foreach ($key as $k => $v) {
			self::$data[$k] = $v;
		}
	}

	/**
	 * Retrieve a single parametre from the config or the entire config.
	 *
	 * @param string $key [optional] If the parametre is absent, the entire config will be returned.<br>
	 * Otherwise, only the requested option is fetched.
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public static function get($key = null)
	{
		$return = self::$data;
		if (null !== $key) {
			$key = strval($key);
			$key = explode('.', $key);
			foreach ($key as $k) {
				if (array_key_exists($k, $return)) {
					$return = $return[$k];
				} else {
					return null;
				}
			}
		}
		return $return;
	}

	/**
	 * Set a parametre or a set thereof using either an XML file or an XML string
	 *
	 * @param string $key
	 * @param string $data Either a filename or an XML string
	 *
	 * @since 0.11.1-dev
	 */
	public static function fromXml($key, $data)
	{
		if (strstr($data, '<?xml')) {
			$data = simplexml_load_string($data);
		} else {
			$data = simplexml_load_file($data);
		}

		self::set($key,json_decode(json_encode($data), true));
	}
}
