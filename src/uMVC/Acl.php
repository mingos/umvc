<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Base Access Control List implementation
 *
 * @package ACL
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Acl
{
	/**#@+
	 * Access markers, only used internally
	 *
	 * @var integer
	 * @since 0.14.0-dev
	 */
	const ACCESS_ALLOWED = 1;
	const ACCESS_DENIED = 0;
	const ACCESS_UNDEFINED = -1;
	/**#@-*/

	/**
	 * Defined roles
	 * @var array
	 */
	private $roles = [];

	/**
	 * Defined resources
	 * @var array
	 */
	private $resources = [];

	/**
	 * A 3D matrix of role-resource-privilege access rules
	 * @var array
	 */
	private $matrix = [[[]]];

	/**
	 * Add a new role
	 *
	 * @param string $role The role name
	 * @param array|string $parents [optional] The name or array of names of roles the role should inherit access rules from.<br>
	 * Omit for no inheritance. If nonexistent parent roles are specified, they will be added first. If an already
	 * existent role is created, its parent roles will be replaced with the ones specified in the 2nd argument.
	 *
	 * @return \uMVC\Acl Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 * @version 2
	 */
	public function addRole($role, $parents = [])
	{
		if (!is_array($parents)) {
			$parents = [$parents];
		}

		foreach ($parents as $parent) {
			if (!array_key_exists($parent,$this->roles)) {
				$this->addRole($parent);
			}
		}

		$this->roles[$role] = $parents;
		return $this;
	}

	/**
	 * Modify a role to add an additional parent role for access rules inheritance,.
	 *
	 * @param string $role The role name to be modified
	 * @param string $parent The parent role name to be added
	 *
	 * @return \uMVC\Acl Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function addParent($role, $parent)
	{
		if (!array_key_exists($parent,$this->roles)) {
			$this->addRole($parent);
		}

		if (!in_array($parent,$this->roles[$role])) {
			$this->roles[$role][] = $parent;
		}

		return $this;
	}

	/**
	 * Add a new resource
	 *
	 * @param string $resource The resource name. If the resource already exists, it will be overwritten.
	 * @param string|array $privileges [optional] Privilege(s) available for the resource.<br>
	 * Omit for no privileges (Only access to the resource itself will be considered).
	 *
	 * @return \uMVC\Acl Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 * @version 2
	 */
	public function addResource($resource, $privileges = [])
	{
		if (!is_array($privileges)) {
			$privileges = [$privileges];
		}

		if (!in_array("",$privileges)) {
			$privileges = array_merge([""],$privileges);
		}

		$this->resources[$resource] = $privileges;
		return $this;
	}

	/**
	 * Add a new privilege to a given resource
	 *
	 * @param string $resource Resource name to be modified. If it doesn't exist, it will be created.
	 * @param string $privilege Privilege name to be added to the resource.
	 *
	 * @return \uMVC\Acl Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function addPrivilege($resource, $privilege)
	{
		if (!array_key_exists($resource,$this->resources)) {
			$this->addResource($resource);
		}

		if (!in_array($privilege,$this->resources[$resource])) {
			$this->resources[$resource][] = $privilege;
		}

		return $this;
	}

	/**
	 * Allow access to a resource or a privilege
	 *
	 * @param string $role The role name
	 * @param string|null $resource [optional] The resource name to be allowed access to. If omitted or null, the role will be
	 * granted access to all resources available.
	 * @param string $privilege [optional] The privilege name to allow access to. If omitted, access will be allowed to the
	 * resource instead of a privilege.
	 *
	 * @return \uMVC\Acl Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 * @version 2
	 */
	public function allow($role, $resource = null, $privilege = "")
	{
		if (null === $resource) {
			foreach ($this->resources as $resource => $privileges) {
				foreach ($privileges as $privilege) {
					$this->matrix[$role][$resource][$privilege] = self::ACCESS_ALLOWED;
				}
			}
		} else {
			$this->matrix[$role][$resource][$privilege] = self::ACCESS_ALLOWED;
		}

		return $this;
	}

	/**
	 * Deny access to a resource or a privilege
	 *
	 * @param string $role The role name
	 * @param string|null $resource [optional] The resource name to be denied access to. If omitted or null, the role will be
	 * denied access to all resources available.
	 * @param string $privilege [optional] The privilege name to deny access to. If omitted, access will be denied to the
	 * resource instead of a privilege.
	 *
	 * @return \uMVC\Acl Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 * @version 2
	 */
	public function deny($role, $resource = null, $privilege = "")
	{
		if (null === $resource) {
			foreach ($this->resources as $resource => $privileges) {
				foreach ($privileges as $privilege) {
					$this->matrix[$role][$resource][$privilege] = self::ACCESS_DENIED;
				}
			}
		} else {
			$this->matrix[$role][$resource][$privilege] = self::ACCESS_DENIED;
		}

		return $this;
	}

	/**
	 * Check whether a given role has access to a given resource/privilege
	 *
	 * @param string $role Role name
	 * @param string $resource Resource name
	 * @param string $privilege [optional] Privilege name
	 *
	 * @throws \Exception if the specified role/resource/privilege do not exist
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 * @version 2
	 */
	public function isAllowed($role, $resource, $privilege = "")
	{
		// check for role, resource and privilege validity
		if (!array_key_exists($role,$this->roles)) {
			throw new \Exception ("No such role: {$role}");
		}
		if (!array_key_exists($resource,$this->resources)) {
			throw new \Exception ("No such resource: {$resource}");
		}
		if (!in_array($privilege,$this->resources[$resource])) {
			throw new \Exception ("No such privilege: {$privilege} in resource: {$resource}");
		}

		$result = $this->_isAllowed($role,$resource,$privilege);

		if ($result === self::ACCESS_UNDEFINED) {
			return false;
		} else {
			return (boolean)$result;
		}
	}

	/**
	 * Check whether a given role has access to a given resource/privilege
	 *
	 * @param string $role Role name
	 * @param string $resource Resource name
	 * @param string $privilege Privilege name
	 *
	 * @return integer
	 *
	 * @since 0.14.0-dev
	 */
	private function _isAllowed($role, $resource, $privilege)
	{
		// if a given privilege is present in the ACL, return it
		if (array_key_exists($role,$this->matrix) && array_key_exists($resource,$this->matrix[$role]) && array_key_exists($privilege,$this->matrix[$role][$resource])) {
			return $this->matrix[$role][$resource][$privilege];
		}

		// if not, check the role's parents
		foreach ($this->roles[$role] as $parent) {
			$result = $this->_isAllowed($parent,$resource,$privilege);
			if ($result != self::ACCESS_UNDEFINED) {
				return $result;
			}
		}

		// if nothing has been found, return an undefined result
		return self::ACCESS_UNDEFINED;
	}

	/**
	 * Check whether a role inherits access rules from a given parent role
	 *
	 * @param string $role Name of the role that inherits access rules
	 * @param string $parent Name of the supposed parent role
	 *
	 * @return boolean
	 *
	 * @throws \Exception if any of the two role names do not exist
	 *
	 * @since 0.14.0-dev
	 */
	public function inherits($role, $parent)
	{
		if (!array_key_exists($role,$this->roles)) {
			throw new \Exception ("No such role: {$role}");
		}

		if (!array_key_exists($parent,$this->roles)) {
			throw new \Exception ("No such role: {$parent}");
		}

		return $this->_inherits($role,$parent);
	}

	/**
	 * Check whether a role inherits access rules from a given parent role
	 *
	 * @param string $role Name of the role that inherits access rules
	 * @param string $parent Name of the supposed parent role
	 *
	 * @return boolean
	 */
	private function _inherits($role, $parent)
	{
		if (in_array($parent,$this->roles[$role])) {
			return true;
		} else {
			foreach ($this->roles[$role] as $item) {
				if ($this->_inherits($item,$parent)) {
					return true;
				}
			}
		}

		return false;
	}
}
