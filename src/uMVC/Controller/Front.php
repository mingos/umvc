<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Controller;

/**
 * The front controller
 *
 * @package Core
 * @subpackage Controller
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Front
{
	use \uMVC\Event;

	/**
	 * A static instance of the front controller object
	 * @var \uMVC\Controller\Front
	 */
	protected static $_instance = null;

	/**
	 * The URI that is going to be resolved
	 * @var string
	 */
	private $uri;

	/**
	 * Set of routers for URI parsing and assembling
	 * @var array
	 */
	private $routers = [];

	/**
	 * Text encoding in the application
	 * @var string
	 */
	private $encoding;

	/**
	 * The doctype
	 * @var string
	 */
	private $doctype = 'HTML5';

	/**
	 * The output object
	 * @var \uMVC\Output
	 */
	private $output = null;

	/**
	 * The controller instantiated after resolving the request
	 * @var \uMVC\Controller
	 */
	private $controller = null;

	/**
	 * Configuration variables
	 * @var array
	 */
	private $config;

	/**
	 * Error controller class name
	 * @var string
	 */
	private $errorController;

	/**
	 * Action name in the error controller
	 * @var string
	 */
	private $errorAction;

	/**
	 * The request object
	 * @var \uMVC\Request
	 */
	private $request = null;

	/**
	 * The application config
	 * @var \uMVC\Config
	 */
	private $appConfig = null;

	/**
	 * The cache adapter
	 * @var \uMVC\Cache
	 */
	private $cache = null;

	/**
	 * Debug mode on/off flag
	 * @var boolean
	 */
	private $debug;

	/**
	 * The response content (the rendered HTML)
	 * @var string
	 */
	private $response = "";

	/**#@+
	 * Triggered event names
	 */
	const EVENT_ROUTE_START = "route start";
	const EVENT_ROUTE_END = "route end";
	const EVENT_DISPATCH_PREP = "dispatch prep";
	const EVENT_DISPATCH_START = "dispatch start";
	const EVENT_DISPATCH_END = "dispatch end";
	const EVENT_RENDER_START = "render start";
	const EVENT_RENDER_END = "render end";
	const EVENT_ERROR = "error";
	/**#@-*/

	/**
	 * Construct the object
	 *
	 * @since 0.10.0-dev
	 */
	private function __construct()
	{
		// define constants
		$request_uri = strstr($_SERVER['REQUEST_URI'],'?') ? strstr($_SERVER['REQUEST_URI'],'?',true) : $_SERVER['REQUEST_URI'];
		defined('DOCROOT_CORRECT') || define('DOCROOT_CORRECT', strpos($request_uri,'index.php') === false ? '' : substr($request_uri,0,strpos($request_uri,'index.php') + strlen('index.php')));
		defined('DOCROOT_CORRECT_FILES') || define('DOCROOT_CORRECT_FILES',str_replace('/index.php','',DOCROOT_CORRECT));
		defined('REQUEST_URI') || define('REQUEST_URI', substr($request_uri,strlen(DOCROOT_CORRECT)));
		defined("QUERY_STRING") || define("QUERY_STRING",$_SERVER["QUERY_STRING"]);
		defined("REQUEST_URI_FULL") || define("REQUEST_URI_FULL", QUERY_STRING != "" ? REQUEST_URI."?".QUERY_STRING : REQUEST_URI);
		defined("PUBLIC_PATH") || define("PUBLIC_PATH",realpath(dirname($_SERVER["SCRIPT_FILENAME"])));
		defined("ROOT_PATH") || define("ROOT_PATH", realpath(PUBLIC_PATH."/.."));
		defined("APPLICATION_PATH") || define("APPLICATION_PATH",realpath(ROOT_PATH."/application"));
		defined("APPLICATION_DOMAIN") || define("APPLICATION_DOMAIN", getenv("APPLICATION_DOMAIN") ? getenv("APPLICATION_DOMAIN") : $_SERVER["SERVER_NAME"]);
		defined("APPLICATION_ENV") || define("APPLICATION_ENV", getenv("APPLICATION_ENV") ? getenv("APPLICATION_ENV") : "production");

		// start session
		session_start();

		// initialise the class autoloader
		\uMVC\Autoloader::getInstance();

		// main configuration file
		if (is_file(APPLICATION_PATH."/Config.php")) {
			include_once APPLICATION_PATH."/Config.php";
		}

		// set debug constant
		defined("uMVC_DEBUG") || define("uMVC_DEBUG", APPLICATION_ENV != "production" || \uMVC\Config::get("debug"));

		// php configuration
		foreach (\uMVC\Config::get("php") as $setting => $value) {
			ini_set($setting, $value);
		}

		// front controller configuration (plus defaults)
		$this->config = \uMVC\Config::get("frontController");
		if (null === $this->config) {
			$this->config = [];
		}
		$this->config += [
			"encoding" => "UTF-8",
			"catchExceptions" => true
		];

		// global configuration
		$this->appConfig = \uMVC\Config::get();
		$this->appConfig += [
			"errorController" => "\\Index\\Controller\\Error"
		];

		// set initial variable values
		$this->setEncoding($this->config["encoding"]);
		$this->setUri(REQUEST_URI);
		$this->request = \uMVC\Request::getInstance();
		$this->cache = \uMVC\Cache::getInstance();

		// make sure the routes exist in the config
		if (!array_key_exists("route", $this->appConfig)) {
			$this->appConfig["route"] = [];
		}

		// take care of exception handling
		if ($this->config["catchExceptions"]) {
			$this->setErrorController($this->appConfig["errorController"]);
			$this->setErrorAction("index");
			set_exception_handler(function($exception) {
				die($this->runException($exception));
			});
		}

		// initialise custom helpers
		if (array_key_exists("helpers",$this->appConfig)) {
			foreach ($this->appConfig["helpers"] as $name => $class) {
				\uMVC\Helper\Broker::getInstance()->register($name, $class);
			}
		}
	}

	/**
	 * Fetch a static instance of the front controller
	 *
	 * @return \uMVC\Controller\Front
	 *
	 * @since 0.0.0-dev
	 */
	public static function getInstance()
	{
		if(null === self::$_instance)
	{
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Resolve the route parametres.<br>
	 * The URI resolving results are (optionally) cached.
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @throws \Exception if the URI could not be resolved
	 *
	 * @since 0.0.0-dev
	 */
	private function resolveRoute()
	{
		$params = $this->cache->fetchUri($this->getUri());

		if (false === $params) {
			// match the URI
			foreach ($this->routers + $this->appConfig["route"] as $name => $config) {
				if ($config instanceof \uMVC\Router) {
					$router = $config;
				} else {
					$router = $this->getRouter($name, $config);
				}

				if ($router
					->setUri($this->getUri())
					->setMethod($this->request->getRequestMethod())
					->match()
				) {
					// set the request parametres
					$params = ["router" => $name] + $router->getParams();

					// store the parametres in cache for future reuse
					$this->cache->storeUri($this->getUri(),$params);

					// break the loop
					break;
				}
			}
			if (false === $params) {
				throw new \Exception("Could not resolve URI ".$this->getUri(), 500);
			}
		}

		$this->request->setParams($params);

		return $this;
	}

	/**
	 * Build the controller class name out of the provided parametre name.
	 *
	 * @param string $controller Controller name, as passed by the GET parametre
	 * @param string|null $module [optional] Module name, as passed by the GE parametre. Null if not using modular structure.
	 *
	 * @return string The full controller class name
	 *
	 * @since 0.4.1-dev
	 */
	public function resolveControllerClass($controller, $module = null)
	{
		// controller class name
		$controller = explode("-",$controller);
		foreach($controller as $k => $v) {
			$controller[$k] = mb_convert_case($v,MB_CASE_TITLE,$this->getEncoding());
		}
		$controller = implode("\\",$controller);

		// module class name
		if (!is_null($module)) {
			$module = explode("-",$module);
			foreach($module as $k => $v) {
				$module[$k] = mb_convert_case($v,MB_CASE_TITLE,$this->getEncoding());
			}
			$module = "\\".implode("\\",$module);
		} else {
			$module = "";
		}

		return $module."\\Controller\\".$controller;
	}

	/**
	 * Create the controller instance
	 *
	 * @param string $controller [optional] Controller parametre to use instead of the request parametre
	 * @param string $module [optional] Module parametre to use instead of the request parametre
	 *
	 * @throws \Exception
	 *
	 * @return string Whatever might be rendered by the controller during object instantiation
	 *
	 * @since 0.0.0-dev
	 */
	private function createController($controller = null, $module = null)
	{
		if (null === $controller) {
			$controller = $this->request->getParam("controller");
		}
		if (null === $module) {
			$module = $this->request->getParam("module",null);
		}

		$controllerClass = $this->resolveControllerClass($controller, $module);

		// check controller existence and attempt to autoload it
		try {
			class_exists(substr($controllerClass,1));
		} catch (\Exception $e) {
			throw new \Exception ("Controller \"{$controller}\" not found",404);
		}

		ob_start();
		$this->setController(new $controllerClass(true));
		return ob_get_clean();
	}

	/**
	 * Set the default application encoding.
	 *
	 * The preset value is UTF-8, so it's not necessary to use this method unless a different encoding is desired.
	 *
	 * @param string $encoding The encoding name
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setEncoding($encoding)
	{
		$this->encoding = strval($encoding);
		return $this;
	}

	/**
	 * Get the default application encoding
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function getEncoding()
	{
		return $this->encoding;
	}

	/**
	 * Fetch a router
	 *
	 * @param string $name   The router's name
	 * @param array  $config The router configuration to use if the requested router hasn't yet been created. If omitted or null and the requested router has not yet been instantiated, the router configuration will be fetched from the application config.
	 *
	 * @return \uMVC\Router
	 *
	 * @since 0.0.0-dev
	 */
	public function getRouter($name, $config = null)
	{
		if (!array_key_exists($name, $this->routers)) {
			$this->addRouter($name, $config);
		}
		return $this->routers[$name];
	}

	/**
	 * Create a router.
	 *
	 * @param string $name The router's name
	 * @param array|\uMVC\Router $config Router configuration or the complete router instance.<br>
	 * If omitted or null, the configuration for the supplied name will be retrieved from the application config.
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @throws \Exception If the router already exists, if there's no available configuration for the requested router
	 * or if the router type in the config is invalid.
	 *
	 * @since 0.0.0-dev
	 */
	public function addRouter($name, $config = null)
	{
		// cannot create an already existing router
		if (array_key_exists($name, $this->routers)) {
			throw new \Exception("The router \"{$name}\" already exists.", 500);
		}

		// if providing a router instance, simply use it
		if ($config instanceof \uMVC\Router) {
			$router = $config;
		} else {
			// make sure there's configuration available
			if (null === $config) {
				$config = array_key_exists($name, $this->appConfig["route"]) ? $this->appConfig["route"][$name] : null;
			}
			if (null === $config) {
				throw new \Exception("No configuration available for the router \"{$name}\".", 500);
			}

			// instantiate the router
			$router = new \uMVC\Router(new \uDispatch\Route());

			// set docroot correct
			$router->setDocrootCorrect(DOCROOT_CORRECT);

			// set config
			$router->setConfig($config);
		}

		// save the router
		$this->routers[$name] = $router;

		// return the front controller to allow chaining
		return $this;
	}

	/**
	 * Run the front controller, fetching all the output.<br>
	 * <br>
	 * The front controller performs the following actions, in this precise order:<br>
	 * 1. Run the main bootstrap, if set<br>
	 * 2. Resolve the route<br>
	 * 3. Run module-specific bootstrap, if there is one<br>
	 * 4. Create an instance of the controller<br>
	 * 5. Execute the controller action<br>
	 * 6. Render the view (and layout), if applicable<br>
	 * 7. Add debug info, if in debug mode<br>
	 * <br>
	 * Along with the above actions, the following events are triggered (and may be observed by any object):<br>
	 * 1. route start<br>
	 * 2. route end<br>
	 * 3. dispatch prep<br>
	 * 4. dispatch start<br>
	 * 5. dispatch end<br>
	 * 6. render start<br>
	 * 7. render end<br>
	 * <br>
	 * Note that virtually only the main bootstrap may contain code that can respond to the first three events, as no
	 * other user-created resources are available at the time they are triggered. Starting from the fourth, the code
	 * may be contained in a controller as well (as the controller is created between the "dispatch prep" and
	 * "dispatch start" events).
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function run()
	{
		// launch the bootstrap, if there is one
		$tStart = microtime(true);
		if (is_file(APPLICATION_PATH."/Bootstrap.php")) {
			(new \Bootstrap())->execute();
		}
		$tBootstrap = microtime(true);

		// resolve route
		$this->triggerEvent(self::EVENT_ROUTE_START);
		$this->resolveRoute();
		$this->triggerEvent(self::EVENT_ROUTE_END);
		$tRoute = microtime(true);

		// run module-specific bootstrap, if in existence
		$module = mb_convert_case($this->request->getParam("module"),MB_CASE_TITLE,"UTF-8");
		if (is_file(APPLICATION_PATH."/{$module}/Bootstrap.php")) {
			$bootstrap = "\\{$module}\\Bootstrap";
			(new $bootstrap())->execute();
		}
		$tModuleBootstrap = microtime(true);

		// dispatch the request
		$this->triggerEvent(self::EVENT_DISPATCH_PREP);
		$this->response = $this->createController();
		$this->triggerEvent(self::EVENT_DISPATCH_START);
		ob_start();
		$this->getController()->dispatch();
		$this->response .= ob_get_clean();
		$this->triggerEvent(self::EVENT_DISPATCH_END);
		$tDispatch = microtime(true);

		// take care of rendering
		$this->triggerEvent(self::EVENT_RENDER_START);
		if (null !== $this->output) {
			$this->response .= $this->output->render();
		}
		$this->triggerEvent(self::EVENT_RENDER_END);
		$tRender = microtime(true);

		// debug
		if (!empty($this->response) && uMVC_DEBUG) {
			$debugSql = \uMVC\Request::getInstance()->getSession("uMVC_DEBUG_SQL");
			if (null === $debugSql) {
				$debugSql = [];
			}

			$debugAutoloader = \uMVC\Registry::get("uMVC_DEBUG_AUTOLOADER");
			if (null === $debugAutoloader) {
				$debugAutoloader = [];
			}

			$debugView = new \uMVC\View\Debug();
			$debugView->bootstrap = $tBootstrap - $tStart;
			$debugView->page = $tRender - $tStart;
			$debugView->route = $tRoute - $tBootstrap;
			$debugView->moduleBootstrap = $tModuleBootstrap - $tRoute;
			$debugView->dispatch = $tDispatch - $tModuleBootstrap;
			$debugView->render = $tRender - $tDispatch;
			$debugView->sql = $debugSql;
			$debugView->memory = memory_get_peak_usage(true);
			$debugView->files = $debugAutoloader;

			\uMVC\Request::getInstance()->setSession("uMVC_DEBUG_SQL",[]);

			ob_start();
			$debugView->render();
			$this->response .= ob_get_clean();
		}

		return $this->response;
	}

	/**
	 * In case an exception has been caught, run the exception controller & action
	 *
	 * @param \Exception $exception The thrown exception
	 *
	 * @return string Rendered output
	 *
	 * @since 0.4.7-dev
	 */
	public function runException(\Exception $exception)
	{
		$this->triggerEvent(self::EVENT_ERROR);

		$errorCode = (is_numeric($exception->getCode())
			? $exception->getCode()
			: 500);

		\uMVC\Registry::set("exception",$exception);
		http_response_code($errorCode); // todo move to the controller, maybe?

		// controller & action
		$this->triggerEvent(self::EVENT_DISPATCH_PREP);
		$controllerClass = $this->getErrorController();
		$this->setController(new $controllerClass());
		$this->triggerEvent(self::EVENT_DISPATCH_START);
		$this->getController()->dispatch($this->getErrorAction());
		$this->triggerEvent(self::EVENT_DISPATCH_END);

		$this->triggerEvent(self::EVENT_RENDER_START);
		$this->response = $this->output->render();
		$this->triggerEvent(self::EVENT_RENDER_END);

		return $this->response;
	}

	/**
	 * Fetch the currently used doctype
	 *
	 * @return string
	 *
	 * @since 0.3.1-dev
	 */
	public function getDoctype()
	{
		return $this->doctype;
	}

	/**
	 * Set the doctype to use
	 *
	 * @param string $doctype
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @throws \Exception in case an invalid doctype string is passed
	 *
	 * @since 0.3.1-dev
	 */
	public function setDoctype($doctype)
	{
		if (in_array($doctype, \uMVC\Helper\Broker::getInstance()->doctype()->getDoctypes())) {
			$this->doctype = $doctype;
			return $this;
		} else {
			throw new \Exception ("Invalid doctype: {$doctype}.",500);
		}
	}

	/**
	 * Fetch the URI that will be resolved
	 *
	 * @return string
	 *
	 * @since 0.3.0-dev
	 */
	public function getUri()
	{
		return $this->uri;
	}

	/**
	 * Set the URI that will be resolved
	 *
	 * @param string $uri The URI
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @since 0.3.0-dev
	 */
	public function setUri($uri)
	{
		$this->uri = strval($uri);
		return $this;
	}

	/**
	 * Fetch the controller instance
	 *
	 * @return \uMVC\Controller
	 *
	 * @since 0.0.0-dev
	 */
	public function getController()
	{
		return $this->controller;
	}

	/**
	 * Set the controller instance
	 *
	 * @param \uMVC\Controller $controller
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setController(\uMVC\Controller $controller)
	{
		$this->controller = $controller;
		return $this;
	}

	public function setOutput(\uMVC\Output $output)
	{
		$this->output = $output;
		return $this;
	}

	public function getOutput()
	{
		return $this->output;
	}

	/**
	 * Fetch the error controller name
	 *
	 * @return string
	 *
	 * @since 0.4.7-dev
	 */
	public function getErrorController()
	{
		return $this->errorController;
	}

	/**
	 * Set the error controller name
	 *
	 * @param string $errorController The error controller name
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @since 0.4.7-dev
	 */
	public function setErrorController($errorController)
	{
		$this->errorController = $errorController;
		return $this;
	}

	/**
	 * Fetch the error action name
	 *
	 * @return string
	 *
	 * @since 0.4.7-dev
	 */
	public function getErrorAction()
	{
		return $this->errorAction;
	}

	/**
	 * Set the erro action name
	 *
	 * @param string $errorAction The error action name
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @since 0.4.7-dev
	 */
	public function setErrorAction($errorAction)
	{
		$this->errorAction = $errorAction;
		return $this;
	}

	/**
	 * Set the response HTML
	 *
	 * @param string $response
	 *
	 * @return \uMVC\Controller\Front Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setResponse($response)
	{
		$this->response = strval($response);
		return $this;
	}

	/**
	 * Fetch the response HTML
	 *
	 * @return string
	 *
	 * @since 0.14.0-dev
	 */
	public function getResponse()
	{
		return $this->response;
	}
}
