<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Acl;

/**
 * Access Control List implementation using an ini file parser
 *
 * @package ACL
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Ini extends \uMVC\Acl {
	/**
	 * Instantiate the ACL using info inside an ACL ini file.
	 *
	 * @param string $filename ACL init file name (with full path)
	 *
	 * @since 0.0.0-dev
	 * @version 2
	 */
	public function __construct($filename)
	{
		$ini    = new \uMVC\Helper\IniFile();
		$config = $ini->parse($filename);

		// parse roles
		foreach ($config['role'] as $role => $parents) {
			if (!empty($parents)) {
				$parents = explode(',', $parents);
			} else {
				$parents = [];
			}
			$this->addRole($role, $parents);
		}

		// parse resources
		foreach ($config['resource'] as $resource => $privileges) {
			if (!empty($privileges)) {
				$privileges = trim($privileges);
				$privileges = explode(",",$privileges);
				foreach ($privileges as $key => $privilege) {
					$privileges[$key] = trim($privilege);
				}
			} else {
				$privileges = [""];
			}
			$this->addResource($resource, $privileges);
		}

		// parse access rules
		if (array_key_exists('allow', $config)) {
			foreach ($config['allow'] as $rule => $roles) {
				$rule = explode(".",$rule);
				$resource = trim($rule[0]);
				if (empty($resource)) {
					$resource = null;
				}
				$privilege = trim(array_key_exists(1,$rule) ? $rule[1] : "");

				$roles = explode(",",trim($roles));
				foreach ($roles as $role) {
					$role = trim($role);
					$this->allow($role, $resource, $privilege);
				}
			}
		}
		if (array_key_exists('deny', $config)) {
			foreach ($config['deny'] as $rule => $roles) {
				$rule = explode(".",$rule);
				$resource = trim($rule[0]);
				if (empty($resource)) {
					$resource = null;
				}
				$privilege = trim(array_key_exists(1,$rule) ? $rule[1] : "");

				$roles = explode(",",trim($roles));
				foreach ($roles as $role) {
					$role = trim($role);
					$this->deny($role, $resource, $privilege);
				}
			}
		}
	}
}
