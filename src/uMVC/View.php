<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * The class for holding and executing the presentational code
 *
 * @package Core
 * @subpackage View
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
abstract class View
{
	/**
	 * The helper broker
	 * @var \uMVC\Helper\Broker
	 */
	protected $_helper;

	/**
	 * Nested views
	 * @var array
	 */
	protected $_views = [];

	/**
	 * Construct the object
	 *
	 * @param array $vars [optional] array of name-value pairs of variables available in the view script
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct($vars = [])
	{
		if (!empty($vars)) {
			$this->addVars($vars);
		}
		$this->_helper = \uMVC\Helper\Broker::getInstance();
		$this->_init();
	}

	/**
	 * Initialise object properties
	 *
	 * @since 0.5.1-dev
	 */
	protected function _init()
	{}

	/**
	 * Add variables to the variable set available to the view script
	 *
	 * @param array $vars An array containing name-value pairs of the variables that are to be made available to the view script
	 *
	 * @return \uMVC\View Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addVars($vars)
	{
		foreach ($vars as $name => $value) {
			$this->$name = $value;
		}
		return $this;
	}

	/**
	 * Set a single variable in the variable set available to the view script
	 *
	 * @param string $name  The name of the variable
	 * @param mixed  $value The variable's value
	 *
	 * @return \uMVC\View Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function addVar($name, $value)
	{
		$this->$name = $value;
		return $this;
	}

	/**
	 * Fetch the view's variables
	 *
	 * @return array Variables passed to the view
	 *
	 * @since 0.0.0-dev
	 */
	public function getVars()
	{
		return get_object_vars($this);
	}

	/**
	 * Add a nested view
	 *
	 * @param string $name The view name
	 * @param string|\uMVC\View $view The view object or view script file name
	 *
	 * @return \uMVC\View Provide a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function addView($name, $view)
	{
		if (!($view instanceof \uMVC\View)) {
			$view = new \uMVC\View\Script($view);
		}
		$this->_views[$name] = $view;
		return $this;
	}

	/**
	 * Add nested views
	 *
	 * @param array $views Views keyed array
	 *
	 * @return \uMVC\View Provide a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function addViews($views)
	{
		foreach ($views as $name => $view) {
			$this->addView($name, $view);
		}
		return $this;
	}

	/**
	 * Fetch a defined nested view
	 *
	 * @param string $name The name of the view to fetch
	 *
	 * @return \uMVC\View
	 *
	 * @throws \Exception if the requested view does not exist
	 *
	 * @since 0.14.0-dev
	 */
	public function getView($name)
	{
		if (array_key_exists($name, $this->_views)) {
			return $this->_views[$name];
		} else {
			throw new \Exception("Cannot find the view named '{$name}'.",500);
		}
	}

	/**
	 * @abstract
	 * Render the view
	 *
	 * @since 0.0.0-dev
	 */
	public abstract function render();

	/**
	 * Convert the object to string
	 *
	 * @return string
	 *
	 * @since 0.4.0-dev
	 */
	public function __toString()
	{
		ob_start();
		$this->render();
		return ob_get_clean();
	}

	/**
	 * Call and render an arbitrary controller and action.
	 *
	 * @param string $action Action name
	 * @param string $controller Controller name, as passed by GET parametres (e.g. "admin-blog")
	 * @param string $module [optional] Moule name, as passed by GET parametres
	 * @param array $params [optional] Parametres passed to the controller
	 *
	 * @return string The action's output
	 *
	 * @since 0.4.1-dev
	 *
	 * @todo Revise the entire mechanism!!! Maybe a secondary front controller instance that will not collide?
	 */
	public function action($action, $controller, $module, $params = [])
	{
		$controller = \uMVC\Controller\Front::getInstance()->resolveControllerClass($controller, $module);
		$controller = new $controller();
		$controller->setParams($params);

		$actionName = "{$action}Action";

		$controller->$actionName();

		ob_start();
		if ($controller->getView() instanceof \uMVC\View) {
			$controller->getView()->render();
		}
		$viewOutput = ob_get_clean();

		return $viewOutput;
	}

	/**
	 * Render an arbitrary view, without calling the controller action
	 *
	 * @param string|\uMVC\View $view An instance of a view object or the name of the view script
	 * @param array $vars Variables created in the view object
	 *
	 * @return string The rendered view
	 *
	 * @since 0.4.1-dev
	 */
	public function view($view, $vars = [])
	{
		if (!($view instanceof \uMVC\View)) {
			if (array_key_exists($view, $this->_views)) {
				$view = $this->_views[$view];
			} else {
				$view = new \uMVC\View\Script($view);
			}
		}

		$view->addVars($this->getVars());
		$view->addVars($vars);

		ob_start();
		$view->render();
		return ob_get_clean();
	}
}
