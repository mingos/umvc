<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Navigator;

/**
 * Defines a page inside a navigation tree
 *
 * @package Navigator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.10.0-dev
 */
class Page implements \Countable, \IteratorAggregate {
	/**
	 * Array of subpages
	 * @var array
	 */
	private $pages = [];

	/**
	 * Reference to the parent page
	 * @var null|\uMVC\Navigator\Page
	 */
	private $parent = null;

	/**
	 * HTML attributes list
	 * @var \uMVC\Html\Attributes
	 */
	private $attributes;

	/**
	 * Page's label
	 * @var string
	 */
	private $label = "";

	/**
	 * The page link's href attribute
	 * @var string
	 */
	private $href = null;

	/**
	 * Indicates whether the page is active
	 * @var boolean
	 */
	private $active = null;

	/**
	 * Indicates whether the page is on the trail to the active page
	 * @var boolean
	 */
	private $activeTrail = null;

	/**
	 * The router name used to generate the page's href
	 * @var string
	 */
	private $router;

	/**
	 * Parametres for the router to generate a link
	 * @var array
	 */
	private $params = [];

	/**
	 * Parametres to generate a query string
	 * @var array
	 */
	private $query = [];

	/**
	 * Flag indicating whether the page has already been prepared
	 * @var boolean
	 */
	private $prepared = false;

	/**
	 * Visibility of the page item. If an item is not visible, rendering it will result in an empty string.
	 * @var boolean
	 */
	private $visible = true;

	/**
	 * Whether the page is just a container or a regular link
	 * @var boolean
	 */
	private $container = false;

	/**
	 * View used to render the page
	 * @var \uMVC\View\Navigator\Page
	 */
	private $view;

	/**
	 * Level of nesting inside other pages
	 * @var integer
	 */
	private $nestLevel = 0;

	/**
	 * Reference to the navigator containing the page
	 * @var \uMVC\Navigator
	 */
	private $navigator = null;

	/**
	 * Construct the object
	 *
	 * @since 0.10.0-dev
	 */
	public function __construct()
	{
		$this->attributes = new \uMVC\Html\Attributes();
		$this->router = \uMVC\Request::getInstance()->getParam("router");
	}

	/**
	 * Add an attribute or an array thereof. If the attribute already exists, the specified values will be added to it, without overwriting the previous ones. Duplicate values are removed.
	 *
	 * @param string|array|\uMVC\Html\Attributes $attribute The name of the attribute to add, a name-value array of attributes or an attributes object
	 * @param string|array $value In case the first parametre is a string, value or array of values for the added attribute
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function addAttr($attribute, $value = [])
	{
		$this->attributes->add($attribute, $value);
		return $this;
	}

	/**
	 * Calculate everything
	 *
	 * @since 0.10.0-dev
	 */
	public function prepare()
	{
		// prepare only once
		if ($this->prepared) {
			return;
		}

		// process subpages
		foreach ($this->pages as $page) {
			if (null === $page->getNavigator()) {
				if (null === $this->getNavigator()) {
					throw new \Exception ("A page needs to have a navigator reference.",500);
				} else {
					$page->setNavigator($this->getNavigator());
				}
			}
			$page
				->setNestLevel($this->nestLevel + 1)
				->prepare()
			;
		}

		// calculate the href
		if (null === $this->href) {
			if ($this->router) {
				$href = \uMVC\Controller\Front::getInstance()->getRouter($this->router)->assemble($this->params);
			} else {
				$href = \uMVC\Controller\Front::getInstance()->getRouter("default")->assemble($this->params);
			}
			if (!empty($this->query)) {
				$query = new \uMVC\Request\QueryString();
				$href .= '?'.$query->setQuery($this->query)->render();
			}
			$this->href = $href;
		}

		// check active trail status
		if (null === $this->activeTrail) {
			foreach ($this->pages as $page) {
				if ($page->isActiveTrail()) {
					$this->activeTrail = true;
				}
			}
		}

		// check active status
		if (null === $this->active) {
			$this->active = true;
			foreach ($this->params + \uMVC\Controller\Front::getInstance()->getRouter($this->router)->getDefaults() as $param => $value) {
				if (\uMVC\Request::getInstance()->getParam($param) != $value) {
					$this->active = false;
					break;
				}
			}
			if ($this->active) {
				foreach ($this->query as $param => $value) {
					if (\uMVC\Request::getInstance()->getQuery($param) != $value) {
						$this->active = false;
						break;
					}
				}
			}
			if ($this->active) {
				foreach ($this->query as $param => $value) {
					if (\uMVC\Request::getInstance()->getQuery($param) != $value) {
						$this->active = false;
						break;
					}
				}
			}
		}

		// set attributes
		if ($this->active) {
			$this->activeTrail = true;
			$this->attributes->add(['class' => 'active']);
		}
		if ($this->activeTrail) {
			$this->attributes->add(['class' => 'active-trail']);
		}
	}

	/**
	 * Check if the page is active
	 *
	 * @return boolean
	 *
	 * @since 0.10.0-dev
	 */
	public function isActive()
	{
		return $this->active;
	}

	/**
	 * Set the active status
	 *
	 * @param boolean $flag [optional] Whether the page should be activated
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setActive($flag = true)
	{
		$this->active = (boolean)$flag;
		return $this;
	}

	/**
	 * Set the active trail status
	 *
	 * @param boolean $flag [optional] Whether the page should be part of the active trail
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setActiveTrail($flag = true)
	{
		$this->activeTrail = (boolean)$flag;
		return $this;
	}

	/**
	 * Check if the page is a trail to the active page
	 *
	 * @return boolean
	 *
	 * @since 0.10.0-dev
	 */
	public function isActiveTrail()
	{
		return $this->activeTrail;
	}

	/**
	 * Fetch the page's label
	 *
	 * @return string
	 *
	 * @since 0.10.0-dev
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * Set the page's label
	 *
	 * @param string $label The label string
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setLabel($label)
	{
		$this->label = strval($label);
		return $this;
	}

	/**
	 * Set the page's parent
	 *
	 * @param \uMVC\Navigator\Page $page The parent page reference
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setParent(\uMVC\Navigator\Page &$page)
	{
		$this->parent = $page;
		return $this;
	}

	/**
	 * Append a new subpage
	 *
	 * @param \uMVC\Navigator\Page $page
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function appendPage(\uMVC\Navigator\Page $page)
	{
		$this->pages[] = $page->setParent($this);
		return $this;
	}

	/**
	 * Render the page item
	 *
	 * @return string
	 *
	 * @since 0.10.0-dev
	 */
	public function render()
	{
		$this->prepare();

		if (!$this->isVisible()) {
			return "";
		}

		$view = $this->getView();
		$view->attributes = $this->attributes;
		$view->pages = $this->getPages();
		$view->page = $this;

		ob_start();
		$view->render();
		return ob_get_clean();
	}

	/**
	 * Convert the page item to string
	 *
	 * @return string
	 *
	 * @since 0.10.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Fetch the page's router params
	 *
	 * @return array Router params
	 *
	 * @since 0.10.0-dev
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * Set the page's router params
	 *
	 * @param array $params Array of router parametres
	 * @param string $router [optional] The name of the router
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setParams($params, $router = null)
	{
		$this->params = $params;
		if (null !== $router) {
			$this->router = strval($router);
		}
		return $this;
	}

	/**
	 * Set the query parametres
	 *
	 * @param array $query Query parametres
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setQuery($query)
	{
		$this->query = $query;
		return $this;
	}

	/**
	 * Fetch the query parametres
	 *
	 * @return array
	 *
	 * @since 0.14.0-dev
	 */
	public function getQuery()
	{
		return $this->query;
	}

	/**
	 * Fetch the page's parent
	 *
	 * @return null|\uMVC\Navigator\Page
	 *
	 * @since
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * Count the number of subpages
	 *
	 * @return integer
	 *
	 * @since 0.10.0-dev
	 */
	public function count()
	{
		return count($this->getPages());
	}

	/**
	 * Fetch a foreach-compatible data array
	 *
	 * @return \uMVC\Iterator
	 *
	 * @since 0.10.0-dev
	 */
	public function getIterator()
	{
		return new \uMVC\Iterator($this->getPages());
	}

	/**
	 * Fetch the page's href
	 *
	 * @return string
	 *
	 * @since 0.10.0-dev
	 */
	public function getHref()
	{
		return $this->href;
	}

	/**
	 * Set the page's href
	 *
	 * @param string $href The href
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setHref($href)
	{
		$this->href = strval($href);
		return $this;
	}

	/**
	 * Fetch a page with a matching href
	 *
	 * @param string $href
	 *
	 * @return null|\uMVC\Navigator\Page The page matching the specified href or null if no such page is found
	 *
	 * @since 0.10.0-dev
	 */
	public function getByHref($href)
	{
		if ($this->href == $href) {
			return $this;
		} else {
			foreach ($this->pages as $page) {
				$return = $page->getByHref($href);
				if (null !== $return) {
					return $return;
				}
			}
		}
		return null;
	}

	/**
	 * Fetch the active page
	 *
	 * @return null|\uMVC\Navigator\Page The active page or null if no active page is present
	 *
	 * @since 0.10.0-dev
	 */
	public function getActive()
	{
		if ($this->active) {
			return $this;
		} else {
			if ($this->activeTrail) {
				foreach ($this->pages as $page) {
					if ($page->isActiveTrail()) {
						$return = $page->getActive();
						if (null !== $return) {
							return $return;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Fetch the router name
	 *
	 * @return string The name of the router
	 *
	 * @since 0.10.0-dev
	 */
	public function getRouter()
	{
		return $this->router;
	}

	/**
	 * Set the router name
	 *
	 * @param string $router Name of the router to use to build the page link's href
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setRouter($router)
	{
		$this->router = $router;
		return $this;
	}

	/**
	 * Check whether the page is visible
	 *
	 * @return boolean
	 *
	 * @since 0.10.0-dev
	 */
	public function isVisible()
	{
		return $this->visible;
	}

	/**
	 * Set the page's visibility
	 *
	 * @param boolean $visible
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setVisible($visible)
	{
		$this->visible = (boolean)$visible;
		return $this;
	}

	/**
	 * Check whether the page is a container
	 *
	 * @return boolean
	 *
	 * @since 0.10.0-dev
	 */
	public function isContainer()
	{
		return $this->container;
	}

	/**
	 * Make the page a container
	 *
	 * @param boolean $container
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 */
	public function setIsContainer($container = true)
	{
		$this->container = (boolean)$container;
		return $this;
	}

	/**
	 * Set the view that will be used to render the navigator page
	 *
	 * @param \uMVC\View\Navigator $view View used to render the object
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setView(\uMVC\View\Navigator\Page $view)
	{
		$this->view = $view;
		return $this;
	}

	/**
	 * Fetch the view used for object rendering
	 *
	 * @return \uMVC\View\Navigator\Page
	 *
	 * @since 0.10.0-dev
	 */
	public function getView()
	{
		if (null === $this->view) {
			$this->view = new \uMVC\View\Navigator\Page();
		}
		return $this->view;
	}

	/**
	 * Set the view used to render child pages
	 *
	 * @param \uMVC\View\Navigator\Page $view View instance
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setPagesView(\uMVC\View\Navigator\Page $view)
	{
		foreach ($this->pages as $page) {
			$page->setView($view);
			$page->setPagesView($view);
		}
		return $this;
	}

	/**
	 * Set the page's nesting level
	 *
	 * @param integer $nestLevel Level of nesting
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setNestLevel($nestLevel)
	{
		$this->nestLevel = intval($nestLevel);
		return $this;
	}

	/**
	 * Fetch the page's level of nesting inside other pages
	 *
	 * @return integer
	 *
	 * @since 0.10.0-dev
	 */
	public function getNestLevel()
	{
		return $this->nestLevel;
	}

	/**
	 * The navigator containing the page
	 *
	 * @param \uMVC\Navigator $navigator
	 *
	 * @return \uMVC\Navigator\Page Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setNavigator(\uMVC\Navigator $navigator)
	{
		$this->navigator = $navigator;
		return $this;
	}

	/**
	 * Get the containing navigator
	 *
	 * @return \uMVC\Navigator
	 *
	 * @since 0.14.0-dev
	 */
	public function getNavigator()
	{
		return $this->navigator;
	}

	/**
	 *
	 * @return array
	 */
	public function getPages()
	{
		$pages = [];

		if (!$this->getNavigator()->getRenderActiveOnly() || $this->activeTrail) {
			foreach ($this->pages as $page) {
				if ($page->isVisible()) {
					$pages[] = $page;
				}
			}
		}

		return $pages;
	}
}
