<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Trait for classes using translation
 *
 * @package Translator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.3.0-dev
 */
trait Translator {
	/**
	 * Returns a translated string.
	 *
	 * @param string $string The original string. It's possible to use placeholders readable by *printf functions.
	 * For each placeholder in the output string, an additional parametre must be passed to the function, eg.:
	 * <code>
	 * echo $translator->translate("Hello %1$s", "world");
	 * </code>
	 *
	 * @return string The translated string
	 *
	 * @since 0.7.1-dev
	 */
	public function translate($string)
	{
		if (func_num_args() > 1) {
			if (is_array(func_get_arg(1))) {
				$args = func_get_arg(1);
			} else {
				$args = array_slice(func_get_args(), 1);
			}
		} else {
			$args = [];
		}

		$translator = \uMVC\Registry::get("uMVC_Translator_Adapter");
		if (null !== $translator) {
			return $translator->translate($string, $args);
		} else {
			return vsprintf($string, $args);
		}
	}

	/**
	 * Fetch the name of the translator adapter type
	 *
	 * @return string
	 *
	 * @since 0.7.1-dev
	 */
	public function getTranslatorType()
	{
		$translator = \uMVC\Registry::get("uMVC_Translator_Adapter");
		if (null !== $translator) {
			return $translator->getType();
		} else {
			return 'None';
		}
	}

	/**
	 * Fetch the translator adapter object
	 *
	 * @return \uMVC\Translator\Adapter
	 *
	 * @since 0.14.0-dev
	 */
	public function getTranslatorAdapter()
	{
		return \uMVC\Registry::get("uMVC_Translator_Adapter");
	}

	/**
	 * Check whether the translator adapter has already been enabled
	 *
	 * @return boolean
	 *
	 * @since 0.14.0-dev
	 */
	public function isTranslatorEnabled()
	{
		return !is_null(\uMVC\Registry::get("uMVC_Translator_Adapter"));
	}
}
