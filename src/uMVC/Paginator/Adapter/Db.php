<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Paginator\Adapter;

/**
 * Pagination of database rows
 *
 * @package Paginator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.8.0-dev
 */
class Db extends \uMVC\Paginator\Adapter {
	use \uMVC\Db;

	/**
	 * The SQL query to be launched
	 * @var string
	 */
	private $sql = null;

	/**
	 * Parametres bound to the query
	 * @var array
	 */
	private $bind;

	/**
	 * Construct the paginator
	 *
	 * @param string|\uMVC\Db\Select|null $sql [optional] The SQL query for the paginator. If omitted, it should be
	 * set using the setSql() method.
	 *
	 * @since 0.12.2-dev
	 */
	public function __construct($sql = null)
	{
		if ($sql !== null) {
			$this->setSql($sql);
		}
	}

	/**
	 * Set the SQL query to be launched.<br>
	 *
	 * The input query must be a SELECT query, and must not contain a LIMIT clause
	 * (ie, it must be the query to select ALL the database records that will appear
	 * on the paginator's pages).
	 *
	 * @param string $sql The query
	 * @param array $bind [optional] Bound parametres
	 *
	 * @return \uMVC\Paginator\Adapter\Db Provides a fluent interface
	 *
	 * @since 0.8.0-dev
	 */
	public function setSql($sql, $bind = [])
	{
		$this->sql = preg_replace("/^(SELECT)/","SELECT SQL_CALC_FOUND_ROWS", trim(strval($sql)));
		$this->bind = $bind;
		return $this;
	}

	/**
	 * Fetch the database records for the paginator
	 *
	 * @return array The fetched records
	 *
	 * @since 0.8.0-dev
	 */
	public function getData()
	{
		if (null === $this->_data) {
			if (null === $this->sql) {
				throw new \Exception("Cannot fetch data without a SQL query.",500);
			}

			$offset = ($this->_page - 1) * $this->_itemsPerPage;
			$sql = $this->sql." LIMIT {$offset},{$this->_itemsPerPage}";

			// get query result
			$this->_data = $this->query($sql, $this->bind)->fetchAll();

			// remaining variables
			$records = $this->query("SELECT FOUND_ROWS() AS rows")->fetch();

			$scrolling = new \uMVC\Paginator\Scrolling\Slider($this->_page,$this->_range,$this->_itemsPerPage,$records['rows']); // @todo add scrolling type choice

			$this->_totalPages = $scrolling->getTotalPages();
			$this->_pagesInRange = $scrolling->getPages();
			$this->_next = $scrolling->getNext();
			$this->_previous = $scrolling->getPrevious();
		}

		return $this->_data;
	}
}
