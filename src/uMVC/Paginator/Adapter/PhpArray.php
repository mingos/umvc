<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Paginator\Adapter;

/**
 * Pagination of a PHP array dataset
 *
 * @package Paginator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.10.1-dev
 */
class PhpArray extends \uMVC\Paginator\Adapter {
	/**
	 * Input data
	 * @var array
	 */
	private $data = null;

	/**
	 * Set the paginator _items
	 *
	 * @param array $data Array of _items for the paginator
	 *
	 * @return \uMVC\Paginator\Adapter\PhpArray Provides a fluent interface
	 *
	 * @throws \Exception If the provided parametr is not an array
	 *
	 * @since 0.10.1-dev
	 */
	public function setData($data)
	{
		if (!is_array($data)) {
			throw new \Exception(__METHOD__.": Parametre is required to be an array.",500);
		}
		$this->data = $data;
		return $this;
	}

	/**
	 * Use the provided data array to build the paginator
	 *
	 * @return array Paginator entries
	 *
	 * @throws \Exception In case no data has been provided.
	 *
	 * @since 0.10.1-dev
	 */
	public function getData()
	{
		if (null === $this->_data) {
			if (null === $this->data) {
				throw new \Exception(__METHOD__.": No data provided.",500);
			}

			// get the correct slice of the input data array
			$this->_data = array_slice($this->data,($this->_page - 1) * $this->_itemsPerPage, $this->_itemsPerPage);

			// set page scrolling
			$scrolling = new \uMVC\Paginator\Scrolling\Slider($this->_page,$this->_range,$this->_itemsPerPage,count($this->data)); // @todo add scrolling type choice

			$this->_totalPages = $scrolling->getTotalPages();
			$this->_pagesInRange = $scrolling->getPages();
			$this->_next = $scrolling->getNext();
			$this->_previous = $scrolling->getPrevious();
		}

		return $this->_data;
	}

	/**
	 * Fetch a single paginator item
	 *
	 * @param integer $index The item's index in the data array
	 *
	 * @return mixed The item in question
	 *
	 * @since 0.11.1-dev
	 */
	public function getItem($index)
	{
		if (null === $this->_data) {
			$this->_data = $this->getData();
		}
		return $this->_data[$index];
	}
}
