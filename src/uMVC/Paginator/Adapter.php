<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Paginator;

/**
 * Pagination control
 *
 * @package Paginator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
abstract class Adapter implements \IteratorAggregate, \Countable, \ArrayAccess {
	/**
	 * The view object
	 * @var \uMVC\View
	 */
	private $view = null;

	/**
	 * Current page number
	 * @var int
	 */
	protected $_page = 1;

	/**
	 * Number of visible pages
	 * @var int
	 */
	protected $_range = 10;

	/**
	 * Number of records displayed on each page
	 * @var int
	 */
	protected $_itemsPerPage = 10;

	/**
	 * Total number of pages
	 * @var int
	 */
	protected $_totalPages = 0;

	/**
	 * Current pages range (an array containing all the page numbers that are currently displayed)
	 * @var array
	 */
	protected $_pagesInRange;

	/**
	 * The first page in the currently displayed pages range
	 * @var int
	 */
	protected $_firstInRange;

	/**
	 * The last page in the currently displayed pages range
	 * @var int
	 */
	protected $_lastInRange;

	/**
	 * The previous page number
	 * @var int
	 */
	protected $_previous;

	/**
	 * The next page number
	 * @var int
	 */
	protected $_next;

	/**
	 * Paginator _items
	 * @var array
	 */
	protected $_data = null;

	/**
	 * Set the view used to render the paginator
	 *
	 * @param \uMVC\View $view The view object
	 *
	 * @return \uMVC\Paginator\Adapter Provides a fluent interface
	 *
	 * @since 0.4.3-dev
	 */
	public function setView(\uMVC\View $view)
	{
		$this->view = $view;
		return $this;
	}

	/**
	 * Fetch the paginator _items
	 *
	 * @return array The fetched records
	 *
	 * @since 0.0.0-dev
	 */
	abstract public function getData();

	/**
	 * Fetch a single paginator item
	 *
	 * @param integer $index The item's index in the data array
	 *
	 * @return mixed The item in question
	 *
	 * @since 0.11.1-dev
	 */
	public function getItem($index)
	{
		if (null === $this->_data) {
			$this->getData();
		}
		return $this->_data[$index];
	}

	/**
	 * Set a new value as the paginator item
	 *
	 * @param integer $index The item's index in the data array
	 * @param mixed $value New value
	 *
	 * @return \uMVC\Paginator\Adapter Provides a fluent interface
	 *
	 * @since 0.11.1-dev
	 */
	public function setItem($index, $value)
	{
		if (null === $this->_data) {
			$this->getData();
		}
		$this->_data[$index] = $value;
		return $this;
	}

	/**
	 * Set the current page
	 *
	 * @param int $page Current page number
	 *
	 * @return \uMVC\Paginator\Adapter Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setPage($page)
	{
		$this->_page = intval($page);
		return $this;
	}

	/**
	 * Set the pages range (ie, how many pages will be displayed in the paginator)
	 *
	 * @param int $range The desired pages range
	 *
	 * @return \uMVC\Paginator\Adapter Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setRange($range)
	{
		$this->_range = intval($range);
		return $this;
	}

	/**
	 * Set the number of _items fetched per each page of the paginator
	 *
	 * @param int $itemsPerPage Number of records per page
	 *
	 * @return \uMVC\Paginator\Adapter Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setItemsPerPage($itemsPerPage)
	{
		$this->_itemsPerPage = intval($itemsPerPage);
		return $this;
	}

	/**
	 * Prepares the HTML code of the paginator
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function render()
	{
		$view = $this->view;

		if (null === $view) {
			$view = new \uMVC\View\Paginator();
		}
		$view->page = $this->_page;
		$view->next = $this->_next;
		$view->previous = $this->_previous;
		$view->totalPages = $this->_totalPages;
		$view->firstInRange = $this->_firstInRange;
		$view->lastInRange = $this->_lastInRange;
		$view->pagesInRange = $this->_pagesInRange;

		$view->query = new \uMVC\Request\QueryString();

		ob_start();
		$view->render();
		return ob_get_clean();
	}

	/**
	 * Conversion to string
	 *
	 * @return string
	 *
	 * @since 0.0.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Fetch a foreach-compatible data array
	 *
	 * @return \uMVC\Iterator
	 *
	 * @since 0.0.0-dev
	 */
	public function getIterator()
	{
		return new \uMVC\Iterator($this->getData());
	}

	/**
	 * Count the elements in the paginator.<br>
	 * Makes it possible to use the count() function on the paginator object.
	 *
	 * @return int Item count on the current page
	 *
	 * @since 0.0.0-dev
	 */
	public function count()
	{
		if (null === $this->_data) {
			$this->getData();
		}
		return count($this->_data);
	}

	/**
	 * Implementation of array_key_exists
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetExists($offset)
	{
		if (null === $this->_data) {
			$this->getData();
		}
		return array_key_exists($offset, $this->_data);
	}

	/**
	 * Implementation of array getter (accessing array elements via array index)
	 *
	 * @return mixed
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetGet($offset)
	{
		if (null === $this->_data) {
			$this->getData();
		}
		return $this->_data[$offset];
	}

	/**
	 * Implementation of array setter (setting array elements via array index)
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetSet($offset, $value)
	{
		if (null === $this->_data) {
			$this->getData();
		}
		if (is_null($offset)) {
			$this->_data[] = $value;
		} else {
			$this->_data[$offset] = $value;
		}
	}

	/**
	 * Implementation of unset
	 *
	 * @since 0.16.0-dev
	 */
	public function offsetUnset($offset)
	{
		if (null === $this->_data) {
			$this->getData();
		}
		unset($this->_data[$offset]);
	}
}
