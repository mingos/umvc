<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Paginator\Scrolling;

/**
 * Base class for all pagination scrolling types.<br>
 * Pagination scrolling calculates the pages that are visible on the paginator.
 *
 * @package Paginator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.8.0-dev
 */
abstract class Base
{
	/**
	 * Current page number
	 * @var integer
	 */
	protected $_page;

	/**
	 * Number of visible pages
	 * @var integer
	 */
	protected $_range;

	/**
	 * Number of records displayed on each page
	 * @var integer
	 */
	protected $_itemsPerPage;

	/**
	 * Total number of pages
	 * @var integer
	 */
	protected $_totalPages;

	/**
	 * Total number of _items
	 * @var integer
	 */
	protected $_totalItems;

	/**
	 * Current pages range (an array containing all the page numbers that are currently displayed)
	 * @var array
	 */
	protected $_pagesInRange;

	/**
	 * The first page in the currently displayed pages range
	 * @var integer
	 */
	protected $_firstInRange;

	/**
	 * The last page in the currently displayed pages range
	 * @var integer
	 */
	protected $_lastInRange;

	/**
	 * The previous page number
	 * @var integer
	 */
	protected $_previous;

	/**
	 * The next page number
	 * @var integer
	 */
	protected $_next;

	/**
	 * Construct the pagination scrolling object.
	 *
	 * @param integer $page Current page number
	 * @param integer $range Number of pages available on the paginator
	 * @param integer $itemsPerPage Number of _items displayed on each page of the paginator
	 * @param integer $totalItems Total number of _items to paginate
	 *
	 * @since 0.8.0-dev
	 */
	public function __construct($page, $range, $itemsPerPage, $totalItems)
	{
		$this->_page = intval($page);
		$this->_range = intval($range);
		$this->_itemsPerPage = intval($itemsPerPage);
		$this->_totalItems = intval($totalItems);
		$this->_calculate();
	}

	/**
	 * Calculate the data.
	 *
	 * @since 0.8.0-dev
	 */
	abstract protected function _calculate();

	/**
	 * Fetch the total number of pages (or the number of the last page)
	 *
	 * @return integer Total number of pages
	 *
	 * @since 0.8.0-dev
	 */
	public function getTotalPages()
	{
		return $this->_totalPages;
	}

	/**
	 * Fetch the page numbers that are within the displayed range
	 *
	 * @return array Array of page numbers displayed on the paginator
	 *
	 * @since 0.8.0-dev
	 */
	public function getPages()
	{
		return $this->_pagesInRange;
	}

	/**
	 * Fetch the number of the next page
	 *
	 * @return integer Next page number
	 *
	 * @since 0.8.0-dev
	 */
	public function getNext()
	{
		return $this->_next;
	}


	/**
	 * Fetch the number of the previous page
	 *
	 * @return integer Previous page number
	 *
	 * @since 0.8.0-dev
	 */
	public function getPrevious()
	{
		return $this->_previous;
	}
}
