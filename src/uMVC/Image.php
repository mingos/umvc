<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Easy to use image processing frontend, interfacing with either ImageMagick or GD2
 *
 * @package Image
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.16.0-dev
 */
class Image
{
	/**#@+
	 * Library types
	 */
	const LIBRARY_IMAGICK = "Imagick";
	const LIBRARY_GD = "Gd";
	/**#@-*/

	/**#@+
	 * Setting names
	 */
	const SETTING_COMPRESSION_QUALITY = "compressionQuality";
	const SETTING_GRAVITY = "gravity";
	/**#@-*/

	/**#@+
	 * Gravity values
	 */
	const GRAVITY_NORTHWEST = 1;
	const GRAVITY_NORTH = 2;
	const GRAVITY_NORTHEAST = 3;
	const GRAVITY_WEST = 4;
	const GRAVITY_CENTRE = 5;
	const GRAVITY_EAST = 6;
	const GRAVITY_SOUTHWEST = 7;
	const GRAVITY_SOUTH = 8;
	const GRAVITY_SOUTHEAST = 9;
	/**#@-*/

	/**
	 * The image processor adapter
	 * @var \uMVC\Image\Adapter
	 */
	private $adapter;

	/**
	 * Create an instance of the uCrop image
	 *
	 * @param string $filename The original image's file name
	 * @param array $settings The settings array
	 *
	 * @throws \Exception in case no image processing library is found
	 *
	 * @since 0.16.0-dev
	 */
	public function __construct($filename, $settings = [])
	{
		if ($filename instanceof \uMVC\Image\Adapter) {
			$this->library = $filename->getType();
			$this->adapter = $filename;
		} else {
			if (array_key_exists("adapter", $settings)) {
				$library = $settings["adapter"];
			} else if (class_exists("\\Imagick")) {
				$library = self::LIBRARY_IMAGICK;
			} else if (extension_loaded("gd")) {
				$library = self::LIBRARY_GD;
			} else {
				throw new \Exception("No adapter specified and neither ImageMagick nor GD libraries found.",500);
			}

			$class = "\\uMVC\\Image\\Adapter\\{$library}";
			$this->adapter = new $class($filename, $settings);
		}
	}

	/**
	 * Fetch the image adapter
	 *
	 * @return \uMVC\Image\Adapter
	 *
	 * @since 0.16.0-dev
	 */
	public function getAdapter()
	{
		return $this->adapter;
	}
}
