<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Authentication frontend
 *
 * @package Auth
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Auth
{
	/**
	 * A static instance of the object
	 * @var \uMVC\Auth
	 */
	protected static $_instance = null;

	/**#@+
	 * Authentication result codes
	 * @var integer
	 */
	const AUTHENTICATION_OK = 0;
	const AUTHENTICATION_WRONG_IDENTITY = 1;
	const AUTHENTICATION_WRONG_CREDENTIAL = 2;
	const AUTHENTICATION_AMBIGUOUS_IDENTITY = 3;
	/**#@-*/

	/**
	 * Fetch a static instance of the front controller
	 *
	 * @return \uMVC\Auth
	 *
	 * @since 0.0.0-dev
	 */
	public static function getInstance()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new \uMVC\Auth();
		}
		return self::$_instance;
	}

	/**
	 * Prevent non-static instantiation
	 *
	 * @since 0.14.0-dev
	 */
	protected function __construct()
	{}

	/**
	 * Check whether the user is successfully authenticated
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function hasIdentity()
	{
		if (!array_key_exists('identity', $_SESSION) || empty($_SESSION['identity'])) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Fetch the authenticated user's data.
	 *
	 * @param string $field [optional] The name of the identity column to fetch.
	 *
	 * @return mixed User data.<br>
	 * If no user is authenticated, the method returns NULL.<br>
	 * If a parametre is specified and the user data is an array, the returned value will be the field from the user identity.
	 *
	 * @since 0.0.0-dev
	 */
	public function getIdentity($field = null)
	{
		if ($this->hasIdentity()) {
			$identity = $_SESSION['identity'];
			if (null === $field) {
				return $identity;
			} else {
				if (is_array($identity) && array_key_exists($field, $identity)) {
					return $identity[$field];
				} else {
					return null;
				}
			}
		} else {
			return null;
		}
	}

	/**
	 * Set the authenticated user's data.
	 *
	 * @param mixed $identity User identity data
	 *
	 * @return \uMVC\Auth Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setIdentity($identity)
	{
		$_SESSION['identity'] = $identity;
		return $this;
	}

	/**
	 * Erase the user identity (log out)
	 *
	 * @return \uMVC\Auth Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function removeIdentity()
	{
		if (array_key_exists('identity', $_SESSION)) {
			$_SESSION['identity'] = [];
		}
		return $this;
	}
}
