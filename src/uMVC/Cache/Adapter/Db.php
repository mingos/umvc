<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Cache\Adapter;

/**
 * Database caching adapter
 *
 * @package Core
 * @subpackage Cache
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.14.0-dev
 */
class Db extends \uMVC\Cache\Adapter {
	use \uMVC\Db {
		\uMVC\Db::fetch as dbFetch;
	}

	/**#@+
	 * Available configuration array keys
	 */
	const TABLE = "table";
	const PREFIX = "prefix";
	const NSPACE = "namespace";
	const KEY = "key";
	const VALUE = "value";
	const EXPIRES = "expires";
	/**#@-*/

	/**
	 * Cache database table configuration
	 * @var array
	 */
	private $config;

	/**
	 * Construct the object
	 *
	 * @param string $prefix Cache key prefix
	 * @param int $ttl Time to live (in seconds)
	 * @param array $config [optional] Cache db table config (table name and column mapping)
	 * @param string $db [optional] Name of the database configuration key in the config
	 *
	 * @throws \Exception in case connecting to the memcached server failed for some reason
	 *
	 * @since 0.14.0-dev
	 */
	public function __construct($prefix, $ttl, $config = [], $db = "db")
	{
		// config
		$this->config = $config + [
			self::TABLE => "cache",
			self::PREFIX => "prefix",
			self::NSPACE => "namespace",
			self::KEY => "key",
			self::VALUE => "value",
			self::EXPIRES => "expires"
		];

		// connect to the requested db
		$this->connect($db);

		// setup database
		$this->setupDb();

		// clear old entries
		$this->query(<<<EOS
			DELETE FROM `{$this->config[self::TABLE]}`
			WHERE `{$this->config[self::EXPIRES]}` < :time
EOS
			,
			["time" => time()]
		);


		$this->setPrefix($prefix);
		$this->setTtl($ttl);
	}

	/**
	 * Create the db table if necessary
	 *
	 * @since 0.14.0-dev
	 */
	private function setupDb()
	{
		$this->query(<<<EOS
			CREATE TABLE IF NOT EXISTS `{$this->config[self::TABLE]}` (
				`id` INT(11) NOT NULL AUTO_INCREMENT,
				`{$this->config[self::PREFIX]}` VARCHAR(32) NOT NULL,
				`{$this->config[self::NSPACE]}` VARCHAR(32) NOT NULL,
				`{$this->config[self::KEY]}` VARCHAR(32) NOT NULL,
				`{$this->config[self::VALUE]}` MEDIUMTEXT NOT NULL,
				`{$this->config[self::EXPIRES]}` INT(11) NOT NULL,
				PRIMARY KEY (`id`),
				UNIQUE KEY `pnk` (`{$this->config[self::PREFIX]}`,`{$this->config[self::NSPACE]}`,`{$this->config[self::KEY]}`)
			)
EOS
		);
	}

	/**
	 * Fetch a cache entry
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @return mixed
	 *
	 * @since 0.14.0-dev
	 */
	public function get($key, $namespace = '')
	{
		$result = $this->dbFetch(<<<EOS
			SELECT `{$this->config[self::VALUE]}`
			FROM `{$this->config[self::TABLE]}`
			WHERE `{$this->config[self::KEY]}` = :key AND
			`{$this->config[self::NSPACE]}` = :nspace AND
			`{$this->config[self::PREFIX]}` = :prefix
EOS
			,
			[
				"key" => $key,
				"nspace" => $namespace,
				"prefix" => $this->getPrefix()
			]
		);
		if (!empty($result)) {
			return unserialize($result[$this->config[self::VALUE]]);
		} else {
			return false;
		}
	}

	/**
	 * Store a value in the cache
	 *
	 * @param mixed $value The value to be stored
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.14.0-dev
	 */
	public function set($value, $key, $namespace = '')
	{
		$this->query(<<<EOS
			INSERT INTO `{$this->config[self::TABLE]}`
			(`{$this->config[self::PREFIX]}`,`{$this->config[self::NSPACE]}`,`{$this->config[self::KEY]}`,`{$this->config[self::VALUE]}`,`{$this->config[self::EXPIRES]}`)
			VALUES (:prefix, :nspace, :key, :value, :expires)
			ON DUPLICATE KEY UPDATE
			`{$this->config[self::VALUE]}` = VALUES({$this->config[self::VALUE]}),
			`{$this->config[self::EXPIRES]}` = VALUES({$this->config[self::EXPIRES]})
EOS
			,
			[
				"key" => $key,
				"nspace" => $namespace,
				"prefix" => $this->getPrefix(),
				"value" => serialize($value),
				"expires" => time() + $this->getTtl()
			]
		);
	}

	/**
	 * Delete a cache key
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.14.0-dev
	 */
	public function delete($key, $namespace = '')
	{
		$this->query(<<<EOS
			DELETE FROM `{$this->config[self::TABLE]}`
			WHERE `{$this->config[self::KEY]}` = :key AND
			`{$this->config[self::NSPACE]}` = :nspace AND
			`{$this->config[self::PREFIX]}` = :prefix
EOS
			,
			[
				"key" => $key,
				"nspace" => $namespace,
				"prefix" => $this->getPrefix()
			]
		);
	}

	/**
	 * Clear the entire cache or an entire namespace
	 *
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.14.0-dev
	 */
	public function clear($namespace = '')
	{
		$this->query(<<<EOS
			DELETE FROM `{$this->config[self::TABLE]}`
			WHERE `{$this->config[self::NSPACE]}` = :nspace AND
			`{$this->config[self::PREFIX]}` = :prefix
EOS
			,
			[
				"nspace" => $namespace,
				"prefix" => $this->getPrefix()
			]
		);
	}

	/**
	 * Check the cache adapter
	 *
	 * @return string The name of the cache adapter
	 *
	 * @since 0.14.0-dev
	 */
	public function getType()
	{
		return "Db";
	}
}
