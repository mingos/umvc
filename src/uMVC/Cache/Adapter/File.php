<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Cache\Adapter;

/**
 * File-based caching mechanism
 *
 * @package Core
 * @subpackage Cache
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class File extends \uMVC\Cache\Adapter {
	/**
	 * Cache directory
	 * @var string
	 */
	private $directory;

	/**
	 * Cache keys
	 * @var array
	 */
	private $keys;

	/**
	 * Construct the cache object
	 *
	 * @param string $prefix Cache key prefix
	 * @param int $ttl Time to live, in seconds
	 * @param string $directory The directory (with full path) where the cached data should be stored
	 *
	 * @throws \Exception if the cache directory either doesn't exist or is not writeable
	 *
	 * @since 0.2.0-dev
	 */
	public function __construct($prefix, $ttl, $directory)
	{
		$this->setPrefix($prefix);
		$this->setTtl($ttl);
		if (is_dir($directory) && is_writeable($directory)) {
			$this->directory = rtrim($directory,'/');
		} else {
			throw new \Exception ("Directory {$directory} doesn't exist or is not writeable.",500);
		}
	}

	/**
	 * Fetch a cache entry
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @return mixed
	 *
	 * @since 0.2.0-dev
	 */
	public function get($key, $namespace = '')
	{
		$file = $this->prepareKey($key, $namespace);
		if (file_exists($this->directory."/{$file}.cache")) {
			$data = unserialize(file_get_contents($this->directory."/{$file}.cache"));
			if (!isset($data['expire']) || ($data['expire'] > 0 && $data['expire'] <= time())) {
				$this->delete($key, $namespace);
				return false;
			}
			return $data['data'];
		}

		return false;
	}

	/**
	 * Store a value in the cache
	 *
	 * @param mixed $value The value to be stored
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.2.0-dev
	 */
	public function set($value, $key, $namespace = '')
	{
		$expire = time();
		if ($this->getTtl() > 0) {
			$expire += $this->getTtl();
		} else {
			$expire = 0;
		}

		$key = $this->prepareKey($key, $namespace);
		file_put_contents($this->directory."/{$key}.cache",serialize([
			'expire' => $expire,
			'data' => $value
		]));
	}

	/**
	 * Delete a cache key
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.2.0-dev
	 */
	public function delete($key, $namespace = '')
	{
		$key = $this->prepareKey($key, $namespace);
		unlink($this->directory."/{$key}.cache");
	}

	/**
	 * Clear the entire cache or an entire namespace
	 *
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.2.0-dev
	 */
	public function clear($namespace = '')
	{
		$handle = opendir($this->directory);
		while (false !== ($file = readdir($handle))) {
			if (substr($file,-6) == '.cache') {
				if ('' === $namespace || strstr($file, "___{$namespace}___")) {
					unlink($this->directory."/{$file}");
					unset($this->keys[$file]);
				}
			}
		}
		closedir($handle);
	}

	/**
	 * Check the cache adapter
	 *
	 * @return string The name of the cache adapter
	 *
	 * @since 0.7.0-dev
	 */
	public function getType()
	{
		return 'File';
	}
}
