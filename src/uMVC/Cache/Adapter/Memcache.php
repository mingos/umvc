<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Cache\Adapter;

/**
 * Memcache caching adapter
 *
 * @package Core
 * @subpackage Cache
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.14-0-dev
 */
class Memcache extends \uMVC\Cache\Adapter {
	/**
	 * Cache keys
	 * @var array
	 */
	private $keys = [];

	/**
	 * The memcached server instance
	 * @var \Memcache
	 */
	private $server = null;

	/**
	 * Construct the object
	 *
	 * @param string $prefix Cache key prefix
	 * @param int $ttl Time to live (in seconds)
	 * @param string $host [optional] Host name for the memcached server
	 * @param string $port [optional] Port on which memcached is listening
	 *
	 * @throws \Exception in case connecting to the memcached server failed for some reason
	 *
	 * @since 0.14-0-dev
	 */
	public function __construct($prefix, $ttl, $host = "127.0.0.1", $port = "11211")
	{
		$this->server = new \Memcache();

		// attempt to connect
		if (!$this->server->connect(strval($host),strval($port))) {
			throw new \Exception ("Memcache does not seem to be available at {$host}:{$port}. Please enable it, set the correct host and port or use a different caching mechanism.", 500);
		}

		$this->setPrefix($prefix);
		$this->setTtl($ttl);

		$this->keys = $this->server->get($this->prepareKey("cache_keys", "umvc_memcache"));
		if (empty($this->keys)) $this->keys = [];
	}

	/**
	 * Fetch a cache entry
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @return mixed
	 *
	 * @since 0.14-0-dev
	 */
	public function get($key, $namespace = "")
	{
		return $this->server->get($this->prepareKey($key, $namespace));
	}

	/**
	 * Store a value in the cache
	 *
	 * @param mixed $value The value to be stored
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.14-0-dev
	 */
	public function set($value, $key, $namespace = "")
	{
		$key = $this->prepareKey($key, $namespace);
		$this->server->set($key,$value,MEMCACHE_COMPRESSED,$this->getTtl());
		$this->keys[$key] = $key;
		$this->server->set($this->prepareKey("cache_keys", "umvc_memcache"),$this->keys,MEMCACHE_COMPRESSED,0);
	}

	/**
	 * Delete a cache key
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.14-0-dev
	 */
	public function delete($key, $namespace = "")
	{
		$key = $this->prepareKey($key, $namespace);
		$this->server->delete($key);
		unset($this->keys[$key]);
		$this->server->set($this->prepareKey("cache_keys", "umvc_memcache"),$this->keys,MEMCACHE_COMPRESSED,0);
	}

	/**
	 * Clear the entire cache or an entire namespace
	 *
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.14-0-dev
	 */
	public function clear($namespace = "")
	{
		if ("" === $namespace) {
			$this->server->flush();
			$this->keys = [];
		} else {
			foreach ($this->keys as $key) {
				if (strstr("___{$namespace}___",$key)) {
					$this->server->delete($key);
				}
			}
		}
		$this->server->set($this->prepareKey("cache_keys", "umvc_memcache"),$this->keys,MEMCACHE_COMPRESSED,0);
	}

	/**
	 * Check the cache adapter
	 *
	 * @return string The name of the cache adapter
	 *
	 * @since 0.14-0-dev
	 */
	public function getType()
	{
		return "Memcache";
	}
}
