<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Cache\Adapter;

/**
 * APC-based caching, circumventing the possible issues with
 *
 * @package Core
 * @subpackage Cache
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
class Apc extends \uMVC\Cache\Adapter {
	/**
	 * Cache keys
	 * @var array
	 */
	private $keys = [];

	/**
	 * Construct the object
	 *
	 * @param string $prefix Cache key prefix
	 * @param int $ttl Time to live (in seconds)
	 *
	 * @throws \Exception if APC is not enabled
	 *
	 * @since 0.2.0-dev
	 */
	public function __construct($prefix, $ttl)
	{
		// check if APC is at all available
		if (!function_exists("apc_fetch") || !function_exists("apc_store") || !function_exists("apc_delete")) {
			throw new \Exception ("APC does not seem to be enabled. Please enable it or use a different caching mechanism.",500);
		}

		$this->setPrefix($prefix);
		$this->setTtl($ttl);

		$this->keys = apc_fetch($this->prepareKey("cache_keys", "umvc_apc"));
		if (empty($this->keys)) $this->keys = [];
	}

	/**
	 * Fetch a cache entry
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @return mixed
	 *
	 * @since 0.2.0-dev
	 */
	public function get($key, $namespace = '')
	{
		return apc_fetch($this->prepareKey($key, $namespace));
	}

	/**
	 * Store a value in the cache
	 *
	 * @param mixed $value The value to be stored
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.2.0-dev
	 */
	public function set($value, $key, $namespace = '')
	{
		$key = $this->prepareKey($key, $namespace);
		apc_store($key,$value,$this->getTtl());
		$this->keys[$key] = $key;
		apc_store($this->prepareKey("cache_keys", "umvc_apc"),$this->keys,0);
	}

	/**
	 * Delete a cache key
	 *
	 * @param string $key Cache key
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.2.0-dev
	 */
	public function delete($key, $namespace = '')
	{
		$key = $this->prepareKey($key, $namespace);
		apc_delete($key);
		unset($this->keys[$key]);
		apc_store($this->prepareKey("cache_keys", "umvc_apc"),$this->keys,0);
	}

	/**
	 * Clear the entire cache or an entire namespace
	 *
	 * @param string $namespace [optional] Cache namespace
	 *
	 * @since 0.2.0-dev
	 */
	public function clear($namespace = '')
	{
		if ('' === $namespace) {
			apc_clear_cache("user");
		} else {
			foreach ($this->keys as $key) {
				if (strstr("___{$namespace}___",$key)) {
					apc_delete($key);
				}
			}
		}
		apc_store($this->prepareKey("cache_keys", "umvc_apc"),$this->keys,0);
	}

	/**
	 * Check the cache adapter
	 *
	 * @return string The name of the cache adapter
	 *
	 * @since 0.7.0-dev
	 */
	public function getType()
	{
		return "Apc";
	}
}
