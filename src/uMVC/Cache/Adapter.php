<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Cache;

/**
 * The base class for all cache adapters
 *
 * @package Core
 * @subpackage Cache
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
abstract class Adapter
{
	/**
	 * Key name prefix
	 * @var string
	 */
	private $prefix = "";

	/**
	 * Time To Live - number of seconds for which the cache will be kept
	 * @var int
	 */
	private $ttl = 0;

	/**
	 * Whether to cache resolved URIs (speeds up URI resolving)
	 * @var bool
	 */
	private $cacheUri = true;

	/**
	 * Whether to cache database table metadata (speeds up maps execution)
	 * @var bool
	 */
	private $cacheMetadata = true;

	/**
	 * Check if resolved URI caching is on
	 *
	 * @return boolean
	 *
	 * @since 0.5.3-dev
	 */
	public function getCacheUri()
	{
		return $this->cacheUri;
	}

	/**
	 * Check if table metadata caching is on
	 *
	 * @return boolean
	 *
	 * @since 0.5.3-dev
	 */
	public function getCacheMetadata()
	{
		return $this->cacheMetadata;
	}

	/**
	 * Turn resolved URI caching on or off
	 *
	 * @param boolean $cacheUri
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.5.3-dev
	 */
	public function setCacheUri($cacheUri)
	{
		$this->cacheUri = (boolean)$cacheUri;
		return $this;
	}

	/**
	 * Turn table metadata caching on or off
	 *
	 * @param boolean $cacheMetadata
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.5.3-dev
	 */
	public function setCacheMetadata($cacheMetadata)
	{
		$this->cacheMetadata = (boolean)$cacheMetadata;
		return $this;
	}

	/**
	 * Fetch the parametres of a resolved URI
	 *
	 * @param string $uri
	 *
	 * @return array|boolean Parametres array or false if they haven't been stored or if URI caching is disabled
	 *
	 * @since 0.5.3-dev
	 */
	public function fetchUri($uri)
	{
		if (!$this->cacheUri || uMVC_DEBUG) {
			return false;
		}

		$routes = $this->get('resolved_routes','uMVC_INTERNAL_DATA');
		if (is_array($routes) && array_key_exists($uri,$routes)) {
			return $routes[$uri];
		} else {
			return false;
		}
	}

	/**
	 * Store the parametres of a resolved URI
	 *
	 * @param string $uri The URI that has been resolved
	 * @param array $params Resolved parametres
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.5.3-dev
	 */
	public function storeUri($uri, $params)
	{
		if (!$this->cacheUri || uMVC_DEBUG) {
			return $this;
		}

		$routes = $this->get('resolved_routes','uMVC_INTERNAL_DATA');
		$routes[strval($uri)] = $params;
		$this->set($routes, 'resolved_routes', 'uMVC_INTERNAL_DATA');

		return $this;
	}

	/**
	 * Fetch a database table's metadata
	 *
	 * @param string $table Table name
	 *
	 * @return array|boolean Requested table metadata or false if either the metadata isn't stored or metadata caching is disabled
	 *
	 * @since 0.5.3-dev
	 */
	public function fetchMetadata($table)
	{
		if (!$this->cacheMetadata || uMVC_DEBUG) {
			return false;
		}

		return $this->get("table_metadata_{$table}",'uMVC_INTERNAL_DATA');
	}

	/**
	 * Store a table's metadata
	 *
	 * @param string $table Table name
	 * @param array $metadata The table's metadata
	 *
	 * @return \uMVC\Cache\Adapter Provides a fluent interface
	 *
	 * @since 0.5.3-dev
	 */
	public function storeMetadata($table, $metadata)
	{
		if (!$this->cacheMetadata || uMVC_DEBUG) {
			return $this;
		}

		$this->set($metadata,"table_metadata_{$table}",'uMVC_INTERNAL_DATA');

		return $this;
	}

	/**
	 * Set the cache key prefix
	 *
	 * @param string $prefix
	 *
	 * @since 0.2.0-dev
	 */
	public function setPrefix($prefix)
	{
		$this->prefix = strval($prefix);
	}

	/**
	 * Fetch the cache key prefix
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function getPrefix()
	{
		return $this->prefix;
	}

	/**
	 * Fetch the cache entries Time To Live
	 *
	 * @return int TTL in seconds; 0 for unlimited
	 *
	 * @since 0.2.0-dev
	 */
	public function getTtl()
	{
		return $this->ttl;
	}

	/**
	 * Set the cache entries Time To Live
	 *
	 * @param int $ttl TTL in seconds; 0 for unlimited
	 *
	 * @since 0.2.0-dev
	 */
	public function setTtl($ttl)
	{
		$this->ttl = intval($ttl);
	}

	/**
	 * Assemble the key name
	 *
	 * @param string $key
	 * @param string $namespace
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	protected function prepareKey($key, $namespace)
	{
		return $this->getPrefix()."___{$namespace}___{$key}";
	}

	/**
	 * Fetch a cached index
	 *
	 * @param string $key
	 * @param string $namespace [optional]
	 *
	 * @return mixed
	 *
	 * @since 0.2.0-dev
	 */
	abstract public function get($key, $namespace = '');

	/**
	 * Store a value in cache
	 *
	 * @param mixed $value
	 * @param string $key
	 * @param string $namespace [optional]
	 *
	 * @since 0.2.0-dev
	 */
	abstract public function set($value, $key, $namespace = '');

	/**
	 * Remove a cached index
	 *
	 * @param string $key
	 * @param string $namespace [optional]
	 *
	 * @since 0.2.0-dev
	 */
	abstract public function delete($key, $namespace = '');

	/**
	 * Clear the entire cache or a given namespace
	 *
	 * @param string $namespace [optional]
	 *
	 * @since 0.2.0-dev
	 */
	abstract public function clear($namespace = '');

	/**
	 * Check the cache adapter
	 *
	 * @return string The name of the cache adapter
	 *
	 * @since 0.7.0-dev
	 */
	abstract public function getType();

	/**
	 * Enable the cache adapter for the application
	 *
	 * @since 0.7.1-dev
	 */
	public function enable()
	{
		\uMVC\Registry::set("uMVC_Cache_Adapter",$this);
	}
}