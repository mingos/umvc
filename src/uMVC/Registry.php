<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Registry class, used for holding any globally available information
 *
 * @package Core
 * @subpackage Registry
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Registry
{
	/**
	 * The stored data
	 * @var array
	 */
	private static $data = [];

	/**
	 * Private constructor - prevent instantiation
	 *
	 * @since 0.10.0-dev
	 */
	private function __construct()
	{}

	/**
	 * Set a registry key
	 *
	 * @param string $key The registry key
	 * @param mixed  $value The value to assign to the key
	 *
	 * @since 0.0.0-dev
	 */
	public static function set($key, $value)
	{
		self::$data[$key] = $value;
	}

	/**
	 * Fetch a value from the registry
	 *
	 * @param string $key The registry key to retrieve
	 *
	 * @return mixed The value assigned to the registry key.<br>
	 * If the key does not exist, null is returned instead.
	 *
	 * @since 0.0.0-dev
	 */
	public static function get($key)
	{
		return array_key_exists($key, self::$data) ? self::$data[$key] : null;
	}

	/**
	 * Remove a value from the registry
	 *
	 * @param string $key The registry key to remove
	 *
	 * @since 0.14.0-dev
	 */
	public static function remove($key)
	{
		if (array_key_exists($key, self::$data)) {
			unset(self::$data[$key]);
		}
	}

	/**
	 * Check if a given key has been defined in the registry
	 *
	 * @param string $key The registry key name
	 *
	 * @return boolean Whether the key exists
	 *
	 * @since 0.15.0-dev
	 */
	public static function exists($key)
	{
		return array_key_exists($key, self::$data);
	}
}
