<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Request;

/**
 * Handling the query string rendering<br>
 * This class is capable of rendering the query string based on the current GET parametres or any arbitrary data array.
 *
 * @package Core
 * @subpackage Request
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.8.0-dev
 */
class QueryString
{
	/**
	 * GET parametres
	 * @var array
	 */
	private $query;

	/**
	 * Construct the query string renderer
	 *
	 * @since 0.8.0-dev
	 */
	public function __construct()
	{
		parse_str(QUERY_STRING, $this->query);
	}

	/**
	 * Render the query string
	 *
	 * @param array $with [optional] additional query string parametres to render.
	 * @param array $without [optional] query string parametres to omit.
	 *
	 * @return string
	 *
	 * @since 0.8.0-dev
	 */
	public function render($with = [], $without = [])
	{
		$query = $this->query;

		// additional query parametres
		if (is_array($with)) {
			$query = $with + $query;
		}

		// query parametres to omit
		if (is_array($without) && count($without)) {
			foreach ($without as $key) {
				if (array_key_exists($query[$key])) {
					unset($query[$key]);
				}
			}
		}

		// return the output query string
		return http_build_query($query);
	}

	/**
	 * Render the query string without a given parametre or set of parametres
	 *
	 * @param string|array $keys Key or array of keys that should be removed from the query string
	 *
	 * @return string
	 *
	 * @since 0.8.0-dev
	 * @deprecated
	 */
	public function renderWithout($keys = [])
	{
		return $this->render([], $keys);
	}

	/**
	 * Render the query string with an extra parametre or a set of extra parametres
	 *
	 * @param array $array Array of key-value pairs
	 *
	 * @return string
	 *
	 * @since 0.8.0-dev
	 * @deprecated
	 */
	public function renderWith($array = [])
	{
		return $this->render($array);
	}

	/**
	 * Set the query to an arbitrary array of data.
	 *
	 * @param array $query An n-dimensional array of key-value pairs
	 *
	 * @return \uMVC\Request\QueryString Provides a fuent interface
	 *
	 * @throws \Exception when the parametre is of wrong type
	 *
	 * @since 0.8.0-dev
	 */
	public function setQuery($query)
	{
		if (!is_array($query)) {
			throw new \Exception("Parametre is required to be an array, ".gettype($query)." given instead.",500);
		}
		$this->query = $query;
		return $this;
	}
}
