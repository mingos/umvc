<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Foreach-compatible data return type
 *
 * @package Core
 * @subpackage Misc
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Iterator extends \ArrayIterator {
	private $data;

	/**
	 * Constructor
	 *
	 * @param array $data Data over which to iterate
	 *
	 * @since 0.0.0-dev
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Return the current element
	 *
	 * @return mixed Can return any type.
	 *
	 * @since 0.0.0-dev
	 */
	public function current()
	{
		return current($this->data);
	}

	/**
	 * Move forward to next element
	 *
	 * @return void Any returned value is ignored.
	 *
	 * @since 0.0.0-dev
	 */
	public function next()
	{
		next($this->data);
	}

	/**
	 * Return the key of the current element
	 *
	 * @return string|int scalar on success, integer 0 on failure.
	 *
	 * @since 0.0.0-dev
	 */
	public function key()
	{
		$key = key($this->data);
		if (empty($key) && $key !== '0') {
			return 0;
		} else {
			return $key;
		}
	}

	/**
	 * Checks if current position is valid
	 *
	 * @return boolean The return value will be casted to boolean and then evaluated.<br>
	 * Returns true on success or false on failure.
	 *
	 * @since 0.0.0-dev
	 */
	public function valid()
	{
		$key = key($this->data);
		return $key !== null && $key !== false;
	}

	/**
	 * Rewind the Iterator to the first element
	 *
	 * @return void Any returned value is ignored.
	 *
	 * @since 0.0.0-dev
	 */
	public function rewind()
	{
		reset($this->data);
	}
}
