<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Base class for XML classes
 *
 * @package Xml
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.2.0-dev
 */
abstract class Xml
{
	/**
	 * Render the XML
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	abstract public function render();

	/**
	 * Convert the Xml object to string
	 *
	 * @return string
	 *
	 * @since 0.2.0-dev
	 */
	public function __toString()
	{
		return $this->render();
	}

	/**
	 * Save the Xml to a file
	 *
	 * @param string $filename The target file name, with full path
	 *
	 * @since 0.2.0-dev
	 */
	public function saveToFile($filename)
	{
		file_put_contents($filename,$this->render());
	}

	/**
	 * Parse an XML string
	 *
	 * @param string $xml The XML string
	 *
	 * @since 0.14.0-dev
	 */
	abstract public function parse($xml);
}
