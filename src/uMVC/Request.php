<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Zend Framework
 * Copyright (c) 2005-2010, Zend Technologies USA, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *    * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *      contributors may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC;

/**
 * Class for handling the request information
 *
 * @package Core
 * @subpackage Request
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Request
{
	/**
	 * A singleton instance of the request object
	 * @var \uMVC\Request
	 */
	protected static $_instance = null;

	/**
	 * User-defined parametres
	 * @var array
	 */
	protected $_params = [];

	/**
	 * A fixed copy of the $_FILES array
	 * @var array
	 */
	protected $_files = null;

	/**
	 * Contents of the POST request body
	 * @var string
	 */
	protected $_body = null;

	/**
	 * Prevent request object instantiation
	 *
	 * @since 0.0.0-dev
	 */
	protected function __construct()
	{}

	/**
	 * Retrieve an instance of the request object
	 *
	 * @return \uMVC\Request
	 *
	 * @since 0.0.0-dev
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Retrieve the POST body, passed through json_decode.
	 * 
	 * It is possible to specify a field to retrieve, either with a default value or without one.
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.16.0-dev
	 */
	public function getBody($param = null, $default = null)
	{
		$body = json_decode($this->getBodyRaw());

		if (null === $param) {
			return $body;
		} else {
			return is_object($body) && isset($body->$param) ? $body->$param : $default;
		}
	}

	/**
	 * Fetch the raw contents of the POST data, as a string.
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function getBodyRaw()
	{
		if (null === $this->_body) {
			$this->_body = file_get_contents("php://input");
		}
		return $this->_body;
	}

	/**
	 * Get either a single post value, or the entire $_POST array
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getPost($param = null, $default = null)
	{
		if (null === $param) {
			return $_POST;
		} else {
			$param = strval($param);
			return array_key_exists($param, $_POST) ? $_POST[$param] : $default;
		}
	}

	/**
	 * Get either a single cookie value, or the entire $_COOKIE array
	 *
	 * @param string|null $param   [optional] the parametre to retrieve
	 * @param mixed|null  $default [optional] default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getCookie($param = null, $default = null)
	{
		if (null === $param) {
			return $_COOKIE;
		} else {
			$param = strval($param);
			return array_key_exists($param, $_COOKIE) ? $_COOKIE[$param] : $default;
		}
	}

	/**
	 * Get either a single env value, or the entire $_ENV array
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getEnv($param = null, $default = null)
	{
		if (null === $param) {
			return $_ENV;
		} else {
			$param = strval($param);
			return array_key_exists($param, $_ENV) ? $_ENV[$param] : $default;
		}
	}

	/**
	 * Get either a single query value, or the entire $_GET array
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getQuery($param = null, $default = null)
	{
		if (null === $param) {
			return $_GET;
		} else {
			$param = strval($param);
			return array_key_exists($param, $_GET) ? $_GET[$param] : $default;
		}
	}

	/**
	 * Get either a single server value, or the entire $_SERVER array
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getServer($param = null, $default = null)
	{
		if (null === $param) {
			return $_SERVER;
		} else {
			$param = strval($param);
			return array_key_exists($param, $_SERVER) ? $_SERVER[$param] : $default;
		}
	}

	/**
	 * Get either a single session value, or the entire $_SESSION array
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getSession($param = null, $default = null)
	{
		if (null === $param) {
			return $_SESSION;
		} else {
			$param = strval($param);
			return array_key_exists($param, $_SESSION) ? $_SESSION[$param] : $default;
		}
	}

	/**
	 * Get either a single files value, or the entire $_FILES array
	 *
	 * @param string|null $param   [optional] The parametre to retrieve
	 * @param mixed|null  $default [optional] Default value, in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getFiles($param = null, $default = null)
	{
		if (null === $this->_files) {
			$files = [];
			foreach ($_FILES as $key => $val) {
				$files[$key] = $this->fixFiles($_FILES[$key]);
			}

			$this->_files = $files;
		}

		if (null === $param) {
			return $this->_files;
		} else {
			$param = strval($param);
			return array_key_exists($param, $this->_files) ? $this->_files[$param] : $default;
		}
	}

	/**
	 * Recursively fix the $_FILES array for multiple uploads.
	 *
	 * @param array $files The files array to fix
	 *
	 * @return array Fixed array
	 *
	 * @since 0.10.0-dev
	 */
	private function fixFiles($files)
	{
		$ret = [];

		if(array_key_exists("tmp_name",$files)) {
			if (is_array($files["tmp_name"])) {
				foreach($files["name"] as $idx => $name) {
					$ret[$idx] = $this->fixFiles([
						"name" => $name,
						"tmp_name" => $files["tmp_name"][$idx],
						"size" => $files["size"][$idx],
						"type" => $files["type"][$idx],
						"error" => $files["error"][$idx]
					]);
				}
			} else {
				$ret = $files;
			}
		} else {
			foreach ($files as $key => $value) {
				$ret[$key] = $this->fixFiles($value);
			}
		}

		return $ret;
	}

	/**
	 * Set the post values
	 *
	 * @param string|array $param Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param mixed		   $value [optional] The value to be set in case $param is a string
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setPost($param, $value = null)
	{
		if (is_string($param)) {
			$param = [$param => $value];
		}
		foreach ($param as $k => $v) {
			$_POST[$k] = $v;
		}
		return $this;
	}

	/**
	 * Set the query values
	 *
	 * @param string|array $param Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param mixed		   $value [optional] The value to be set in case $param is a string
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setQuery($param, $value = null)
	{
		if (is_string($param)) {
			$param = [$param => $value];
		}
		foreach ($param as $k => $v) {
			$_GET[$k] = $v;
		}
		return $this;
	}

	/**
	 * Set the server values
	 *
	 * @param string|array $param Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param mixed		   $value [optional] The value to be set in case $param is a string
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setServer($param, $value = null)
	{
		if (is_string($param)) {
			$param = [$param => $value];
		}
		foreach ($param as $k => $v) {
			$_SERVER[$k] = $v;
		}
		return $this;
	}

	/**
	 * Set the env values
	 *
	 * @param string|array $param Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param mixed		   $value [optional] The value to be set in case $param is a string
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setEnv($param, $value = null)
	{
		if (is_string($param)) {
			$param = [$param => $value];
		}
		foreach ($param as $k => $v) {
			$_ENV[$k] = $v;
		}
		return $this;
	}

	/**
	 * Set the session values
	 *
	 * @param string|array $param Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param mixed		   $value [optional] The value to be set in case $param is a string
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setSession($param, $value = null)
	{
		if (is_string($param)) {
			$param = [$param => $value];
		}
		foreach ($param as $k => $v) {
			$_SESSION[$k] = $v;
		}
		return $this;
	}

	/**
	 * Set the files values
	 *
	 * @param string|array $param Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param mixed		   $value [optional] The value to be set in case $param is a string
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setFiles($param, $value = null)
	{
		if (is_string($param)) {
			$param = [$param => $value];
		}
		foreach ($param as $k => $v) {
			$_FILES[$k] = $v;
		}
		return $this;
	}

	/**
	 * Set a cookie.<br>
	 * This is a limited wrapper for setcookie(). If finer control over cookie settings is needed, setcookie() should be used directly.
	 *
	 * @param string $name   Either a set of key-value pairs to be set, or the name of the parametre to set
	 * @param string $value  The value to be set in case $param is a string
	 * @param int    $expire [optional] Cookie expiry timestamp
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setCookie($name, $value, $expire = 0)
	{
		$name   = strval($name);
		$value  = strval($value);
		$expire = intval($expire);
		setcookie($name, $value, $expire);
		return $this;
	}

	/**
	 * Set a custom parametre.
	 *
	 * @param string $name  The name of the parametre
	 * @param mixed  $value The parametre's value
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setParam($name, $value)
	{
		$this->_params[strval($name)] = $value;
		return $this;
	}

	/**
	 * Set multiple custom parametres.
	 *
	 * @param array $params Key-value pairs of the parametres to set
	 *
	 * @return \uMVC\Request Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setParams($params)
	{
		foreach ($params as $name => $value) {
			$this->_params[$name] = $value;
		}
		return $this;
	}

	/**
	 * Retrieve a parametre, in order of preference.<br>
	 * The order of precedence is as follows: custom parametres, GET, POST. If no parametre is found, null or the specified default value is returned.
	 *
	 * @param string $name	  Parametre name to retrieve
	 * @param mixed  $default [optional] Default value in case the parametre is not set
	 *
	 * @return mixed
	 *
	 * @since 0.0.0-dev
	 */
	public function getParam($name, $default = null)
	{
		if (array_key_exists($name, $this->_params)) {
			return $this->_params[$name];
		}
		if (array_key_exists($name, $_GET)) {
			return $_GET[$name];
		}
		if (array_key_exists($name, $_POST)) {
			return $_POST[$name];
		}
		return $default;
	}

	/**
	 * Retrieve all parametres.<br>
	 * The order of precedence is as follows: custom parametres, GET, POST.
	 *
	 * @return array
	 *
	 * @since 0.0.0-dev
	 */
	public function getParams()
	{
		return $this->_params + $_GET + $_POST;
	}

	/**
	 * Return the value of the given HTTP header. Pass the header name as the
	 * plain, HTTP-specified header name. Ex.: Ask for 'Accept' to get the
	 * Accept header, 'Accept-Encoding' to get the Accept-Encoding header.
	 *
	 * @param string $header HTTP header name
	 *
	 * @return string|null HTTP header value, or false if not found
	 *
	 * @throws \Exception in case no header name has been provided
	 *
	 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
	 * @license http://framework.zend.com/license/new-bsd New BSD License
	 *
	 * @since 0.4.1-dev
	 */
	public function getHeader($header)
	{
		if (empty($header)) {
			throw new \Exception("No header name provided.",500);
		}

		// Try to get it from the $_SERVER array first
		$temp = 'HTTP_' . strtoupper(str_replace('-', '_', $header));
		if (isset($_SERVER[$temp])) {
			return $_SERVER[$temp];
		}

		// This seems to be the only way to get the Authorization header on Apache
		if (function_exists("apache_request_headers")) {
			$headers = apache_request_headers();
			if (isset($headers[$header])) {
				return $headers[$header];
			}
			$header = strtolower($header);
			foreach ($headers as $key => $value) {
				if (strtolower($key) == $header) {
					return $value;
				}
			}
		}

		return null;
	}

	/**
	 * Get the request method
	 *
	 * @return string|null
	 *
	 * @since 0.4.1-dev
	 */
	public function getRequestMethod()
	{
		return $this->getServer("REQUEST_METHOD");
	}

	/**
	 * Check whether the request has been made via the POST method
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isPost()
	{
		return $this->getRequestMethod() == "POST";
	}

	/**
	 * Check whether the request has been made via the GET method
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isGet()
	{
		return $this->getRequestMethod() == "GET";
	}

	/**
	 * Check whether the request has been made via the PUT method
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function isPut()
	{
		return $this->getRequestMethod() == "PUT";
	}

	/**
	 * Check whether the request has been made via the DELETE method
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function isDelete()
	{
		return $this->getRequestMethod() == "DELETE";
	}

	/**
	 * Check whether the request has been made via the OPTIONS method
	 *
	 * @return boolean
	 *
	 * @since 0.16.0-dev
	 */
	public function isOptions()
	{
		return $this->getRequestMethod() == "OPTIONS";
	}

	/**
	 * Is the request a Javascript XMLHttpRequest?<br>
	 * Should work with Prototype/Script.aculo.us, jQuery, possibly others.
	 *
	 * @return boolean
	 *
	 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
	 * @license http://framework.zend.com/license/new-bsd New BSD License
	 *
	 * @since 0.16.0-dev
	 */
	public function isXMLHttpRequest()
	{
		return ($this->getHeader("X_REQUESTED_WITH") == "XMLHttpRequest");
	}

	/**
	 * Alias for isXMLHttpRequest
	 *
	 * @return boolean
	 *
	 * @deprecated
	 *
	 * @since 0.4.1-dev
	 */
	public function isAjax()
	{
		return $this->isXMLHttpRequest();
	}

	/**
	 * Check whether the request is using HTTP Secure.
	 *
	 * @return boolean
	 *
	 * @since 0.4.1-dev
	 */
	public function isHttps()
	{
		return $this->getServer("HTTPS") == "on";
	}

	/**
	 * Get the client's IP addres
	 *
	 * @param  boolean $checkProxy
	 *
	 * @return string
	 *
	 * @copyright Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
	 * @license http://framework.zend.com/license/new-bsd New BSD License
	 *
	 * @since 0.4.1-dev
	 */
	public function getIp($checkProxy = true)
	{
		if ($checkProxy && $this->getServer("HTTP_CLIENT_IP") != null) {
			$ip = $this->getServer("HTTP_CLIENT_IP");
		} else if ($checkProxy && $this->getServer("HTTP_X_FORWARDED_FOR") != null) {
			$ip = $this->getServer("HTTP_X_FORWARDED_FOR");
		} else {
			$ip = $this->getServer("REMOTE_ADDR");
		}

		return $ip;
	}

	/**
	 * Redirect to anothe page
	 *
	 * @param string $url New URL
	 * @param integer $type [optional] Redirect type
	 *
	 * @throws \Exception if invalid redirect type is used
	 * 
	 * @since 0.14.0-dev
	 */
	public function redirect($url, $type = 302)
	{
		if (substr($url,0,4) != "http" && substr($url,0,strlen(DOCROOT_CORRECT)) != DOCROOT_CORRECT) {
			$url = DOCROOT_CORRECT.'/'.trim($url,'/');
		}
		switch ($type) {
			case 300: $status = "300 Multiple Choices"; break;
			case 301: $status = "301 Moved Permanently"; break;
			case 302: $status = "302 Found"; break;
			case 303: $status = "303 See Other"; break;
			case 304: $status = "304 Not Modified"; break;
			case 305: $status = "305 Use Proxy"; break;
			case 307: $status = "307 Temporary Redirect"; break;
			default: throw new \Exception("Invalid redirect code: {$type}",500);
		}

		header("HTTP/1.1 {$status}");
		header("Location: {$url}");
		die;
	}
}
