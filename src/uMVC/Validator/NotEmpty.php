<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

/**
 * A validator checking whether values are considered empty
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class NotEmpty extends \uMVC\Validator {
	/**
	 * Empty value
	 */
	const INVALID_IS_EMPTY = 'isEmpty';

	/**
	 * Whether the string "0" should be treated as a non-empty empty value
	 * @var bool
	 */
	private $excludeStringZero = false;

	/**
	 * Initialise the messages
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_IS_EMPTY => 'The value is empty.'
		];
	}

	/**
	 * Check if value is empty
	 *
	 * @param mixed $value
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);
		if ($value === '0' && $this->excludeStringZero) {
			return true;
		} else {
			if (empty($value)) {
				$this->setError($this->_messages[self::INVALID_IS_EMPTY]);
				return false;
			} else {
				return true;
			}
		}
	}

	/**
	 * Define how to treat the string '0' value.
	 *
	 * If string zero exclusion is set to true, whenever a string '0' is passed to the validator, it will be treated as not empty and thus will not trigger an error. Otherwise, it will also be treated as an empty value (which is the default behaviour).
	 *
	 * @param boolean $flag true (or omit the parametre completely) to enable string sero exclusion, false to disable it
	 *
	 * @return \uMVC\Validator\NotEmpty Provides a fluent interface
	 */
	public function excludeStringZero($flag = true)
	{
		$this->excludeStringZero = (bool)$flag;
		return $this;
	}
}
