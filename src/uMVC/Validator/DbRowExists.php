<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

/**
 * A validator checking whether a given row exists in the database
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.4.8-dev
 */
class DbRowExists extends \uMVC\Validator {
	use \uMVC\Db;

	/**
	 * The database query checking the input value's existence
	 * @var string
	 */
	private $sql = null;

	/**
	 * Malformed e-mail address
	 */
	const INVALID_DB_ROWNOTEXISTS = 'rowNotExists';

	/**
	 * @param string $sql [optional] The query checking the input value's existence
	 *
	 * @since 0.5.2-dev
	 */
	public function __construct($sql = null)
	{
		$this->sql = $sql;
		parent::__construct();
	}

	/**
	 * Initialise the messages
	 *
	 * @since 0.4.8-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_DB_ROWNOTEXISTS => 'The row does not exist in the database.'
		];
	}

	/**
	 * Set the SQL query to be launched. Replace the checked value with a question mark placeholder:
	 * <code>
	 * "SELECT * FROM users WHERE email = ?"
	 * </code>
	 *
	 * @param string $sql The query
	 *
	 * @return \uMVC\Validator\DbRowExists Provides a fluent interface
	 *
	 * @since 0.4.8-dev
	 */
	public function setQuery($sql)
	{
		$this->sql = strval($sql);
		return $this;
	}

	/**
	 * Check if the value exists in the database
	 *
	 * @param string $value
	 *
	 * @return boolean
	 *
	 * @since 0.4.8-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);

		$result = $this->query($this->sql,[$value])->fetchAll();

		if (count($result) > 0) {
			return true;
		} else {
			$this->setError($this->_messages[self::INVALID_DB_ROWNOTEXISTS]);
			return false;
		}
	}
}
