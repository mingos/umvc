<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

/**
 * Validator checking whether values represent numeric values
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class Numeric extends \uMVC\Validator {
	/**
	 * Not numeric value
	 */
	const INVALID_NOT_NUMERIC = 'notNumeric';

	/**#@+
	 * Values for matched numeric types indices
	 */
	const TYPE_INT        = 'int';
	const TYPE_FLOAT      = 'float';
	const TYPE_HEX        = 'hex';
	const TYPE_OCT        = 'oct';
	const TYPE_BINARY     = 'binary';
	const TYPE_SCIENTIFIC = 'scientific';
	/**#@-*/

	/**
	 * Allowed numeric types
	 * @var array
	 */
	private $types;

	/**
	 * Initialise the messages and default checked number types
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_NOT_NUMERIC => 'The value is not numeric'
		];
		$this->types = [
			self::TYPE_INT        => true,
			self::TYPE_FLOAT      => true,
			self::TYPE_HEX        => false,
			self::TYPE_OCT        => false,
			self::TYPE_BINARY     => false,
			self::TYPE_SCIENTIFIC => false
		];
	}

	/**
	 * Check if the provided value is a number
	 *
	 * @param mixed $value
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isValid($value)
	{
		$value  = strval($value);
		$valid  = false;
		$locale = localeconv();

		$this->setError(null);

		// match integer
		if ($this->types[self::TYPE_INT]) {
			$valid |= (boolean)preg_match('/^[+\-]?[1-9][0-9]*$/', $value);
		}

		// match floating point
		if (!$valid && $this->types[self::TYPE_FLOAT]) {
			$decimalSeparator = $locale['decimal_point'];
			$valid |= (boolean)preg_match('/[+\\-]?[0-9]*\\' . $decimalSeparator . '[0-9]+$/', $value);
		}

		// match binary
		if (!$valid && $this->types[self::TYPE_BINARY]) {
			$valid |= (boolean)preg_match('/^[01]+$/', $value);
		}

		// match octal
		if (!$valid && $this->types[self::TYPE_OCT]) {
			$valid |= (boolean)preg_match('/^0?[0-7]+$/', $value);
		}

		// match hexadecimal
		if (!$valid && $this->types[self::TYPE_HEX]) {
			$valid |= (boolean)preg_match('/^(0x|0X)?[0-9a-fA-F]+$/', $value);
		}

		// match scientific notation
		if (!$valid && $this->types[self::TYPE_SCIENTIFIC]) {
			$decimalSeparator = $locale['decimal_point'];
			$valid |= (boolean)preg_match('/[+\\-]?[0-9]*\\' . $decimalSeparator . '?[0-9]+([eE][+\\-]?[0-9]+)?$/', $value);
		}

		if (!$valid) {
			$this->setError($this->_messages[self::INVALID_NOT_NUMERIC]);
		}
		return $valid;
	}

	/**
	 * Set which numeric types are checked for. The default checked types areinteger and floating point.
	 * Hexadecimal, octal, binary and scientific notation, or any combination thereof, may be checked for.<br>
	 *
	 * The parametre should be a keyed array, setting desired numeric types to either true (check match) or false (ignore).
	 * For instance, the following code will only check for scientific notation:
	 * <code>
	 * $numeric = (new \uMVC\Validator\Numeric())->setTypes([
	 *	 \uMVC\Validator\Numeric::TYPE_INT => false,
	 *	 \uMVC\Validator\Numeric::TYPE_FLOAT => false,
	 *	 \uMVC\Validator\Numeric::TYPE_BINARY => false,
	 *	 \uMVC\Validator\Numeric::TYPE_OCT => false,
	 *	 \uMVC\Validator\Numeric::TYPE_HEX => false,
	 *	 \uMVC\Validator\Numeric::TYPE_SCIENTIFIC => true
	 * ]);
	 * </code>
	 *
	 * @param array $types An array of type-value pairs.
	 *
	 * @return \uMVC\Validator\Numeric
	 *
	 * @throws \Exception in case the provided parametre is not an array
	 *
	 * @since 0.0.0-dev
	 */
	public function setTypes($types)
	{
		if (is_array($types)) {
			foreach ($types as $type => $accept) {
				$this->types[$type] = (bool)$accept;
			}
		} else {
			throw new \Exception('setTypes expects an array as parametre.', 500);
		}
		return $this;
	}
}
