<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

use uMVC\Validator;

/**
 * A validator checking whether a string conforms to a given DateTime format (compatible with PHP's date() format)
 *
 * @package Validator
 * @author  Dominik Marczuk <mingos.nospam@gmail.com>
 * @since   0.14.0-dev
 */
class DateTime extends Validator
{
	/**
	 * Format does not match
	 */
	const INVALID_WRONG_FORMAT = "wrongFormat";

	/**
	 * The datetime format
	 * @var string
	 */
	private $format;

	/**
	 * Construct the validator
	 *
	 * @param string $format The regular expression
	 *
	 * @since 0.14.0-dev
	 */
	public function __construct($format = "Y-m-d H:i:s")
	{
		$this->format = strval($format);
		parent::__construct();
	}

	/**
	 * Initialise the messages
	 *
	 * @since 0.14.0-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_WRONG_FORMAT => "Wrong date/time format."
		];
	}

	/**
	 * Check if the datetime format is correct
	 *
	 * @param string $value
	 *
	 * @return boolean
	 *
	 * @since 0.14.0-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);

		$date = \DateTime::createFromFormat($this->format, $value);

		if ($date && $date->format($this->format) === $value) {
			return true;
		} else {
			$this->setError($this->_messages[self::INVALID_WRONG_FORMAT]);
			return false;
		}
	}

	/**
	 * Set the datetime format for use with the validator
	 *
	 * @param string $format The regular expression to use
	 *
	 * @return DateTime Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setFormat($format)
	{
		$this->format = strval($format);
		return $this;
	}

	/**
	 * Get the current datetime format
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function getFormat()
	{
		return $this->format;
	}
}
