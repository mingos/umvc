<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

/**
 * A validator checking whether values match a given regular expression
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.1.1-dev
 */
class Regex extends \uMVC\Validator {
	/**
	 * Regex does not match
	 */
	const INVALID_NO_MATCH = 'noMatch';

	/**
	 * The regular expression
	 * @var string
	 */
	private $regex;

	/**
	 * Construct the validator
	 *
	 * @param string $regex The regular expression
	 *
	 * @since 0.5.2-dev
	 */
	public function __construct($regex = '/^.*$/')
	{
		$this->regex = strval($regex);
		parent::__construct();
	}

	/**
	 * Initialise the messages
	 *
	 * @since 0.1.1-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_NO_MATCH => 'The value does not match the regular expression.'
		];
	}

	/**
	 * Check if the value matches a user-defnied regular expression
	 *
	 * @param string $value
	 *
	 * @return boolean
	 *
	 * @since 0.1.1-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);

		if (preg_match($this->regex,$value)) {
			return true;
		} else {
			$this->setError($this->_messages[self::INVALID_NO_MATCH]);
			return false;
		}
	}

	/**
	 * Set the regular expression to be used by the validator
	 *
	 * @param string $regex The regular expression to use
	 *
	 * @return \uMVC\Validator\Regex Provides a fluent interface
	 *
	 * @since 0.1.1-dev
	 */
	public function setRegex($regex)
	{
		$this->regex = strval($regex);
		return $this;
	}
}
