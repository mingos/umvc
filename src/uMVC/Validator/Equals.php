<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

/**
 * A validator checking whether the value is equal to an arbitrary value
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.14.0-dev
 */
class Equals extends \uMVC\Validator {
	/**
	 * Value does not match
	 */
	const INVALID_NOT_EQUAL = 'notEqual';

	/**
	 * The value that the equality will be checked against
	 * @var string
	 */
	private $value;

	/**
	 * Construct the validator
	 *
	 * @param string|callable $value The value to be compared with
	 *
	 * @since 0.14.0-dev
	 */
	public function __construct($value = "")
	{
		$this->setValue($value);
		parent::__construct();
	}

	/**
	 * Initialise the messages
	 *
	 * @since 0.14.0-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_NOT_EQUAL => "The value is not valid."
		];
	}

	/**
	 * Check if the defined value and the checked value match
	 *
	 * @param string $value
	 *
	 * @return boolean
	 *
	 * @since 0.14.0-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);

		$comparison = $this->value;
		if (is_callable($comparison)) {
			$comparison = $comparison();
		}

		if ($value == $comparison) {
			return true;
		} else {
			$this->setError($this->_messages[self::INVALID_NOT_EQUAL]);
			return false;
		}
	}

	/**
	 * Set the value that the equality will be checked against
	 *
	 * @param string|callable $value The regular expression to use
	 *
	 * @return \uMVC\Validator\Equals Provides a fluent interface
	 *
	 * @since 0.14.0-dev
	 */
	public function setValue($value)
	{
		if (is_callable($value)) {
			$this->value = $value;
		} else {
			$this->value = strval($value);
		}
		return $this;
	}
}
