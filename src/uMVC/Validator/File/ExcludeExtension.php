<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator\File;

/**
 * Validate the file extensions type of the upoaded files by disallowing certain extensions
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.11.1-dev
 */
class ExcludeExtension extends \uMVC\Validator {
	/**
	 * Disallowed file extension
	 */
	const INVALID_DISALLOWED_EXTENSION = 'wrongExtension';

	/**
	 * Disallowed file extensions
	 * @var array
	 */
	private $extension = [];

	/**
	 * Initialise the messages
	 *
	 * @since 0.11.1-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_DISALLOWED_EXTENSION => "Wrong file extension."
		];
	}

	/**
	 * Check whether the files have the correct extension
	 *
	 * @param array|mixed $value
	 *
	 * @return boolean
	 *
	 * @since 0.11.1-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);

		foreach ($value as $file) {
			$extension =  pathinfo($file["name"])['extension'];
			$extension = mb_convert_case($extension,MB_CASE_LOWER,$this->_frontController->getEncoding());
			if (!in_array($extension, $this->getExtension())) {
				$this->setError($this->_messages[self::INVALID_DISALLOWED_EXTENSION]);
				return false;
			}
		}

		return true;
	}

	/**
	 * Set all file extensions, overwriting any previously set ones.
	 *
	 * @param array|string $extension array of all file extensions or a string containing one file extension.
	 *
	 * @return \uMVC\Validator\File\Extension provides a fluent interface
	 *
	 * @since 0.11.1-dev
	 */
	public function setExtension($extension)
	{
		$this->extension = [];
		return $this->addExtension($extension);
	}

	/**
	 * Add one or multiple file extensions to the ones already defined
	 *
	 * @param array|string $extension array of all file extensions or a string containing one file extension.
	 *
	 * @return \uMVC\Validator\File\Extension provides a fluent interface
	 *
	 * @since 0.11.1-dev
	 */
	public function addExtension($extension)
	{
		if (!is_array($extension)) {
			$extension = [$extension];
		}
		foreach ($extension as $fileExtension) {
			$fileExtension = trim($fileExtension,".");
			$fileExtension = mb_convert_case($fileExtension,MB_CASE_LOWER,$this->_frontController->getEncoding());
			$this->extension[$fileExtension] = $fileExtension;
		}
		return $this;
	}

	/**
	 * Fetch all allowed extensions
	 *
	 * @return array array of allowed extensions
	 *
	 * @since 0.11.1-dev
	 */
	public function getExtension()
	{
		return $this->extension;
	}
}
