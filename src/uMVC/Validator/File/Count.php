<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator\File;

/**
 * Validate the number of upoaded files
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.10.0-dev
 */
class Count extends \uMVC\Validator {
	/**
	 * Too few files
	 */
	const INVALID_TOO_FEW = 'tooFew';

	/**
	 * Too many files
	 */
	const INVALID_TOO_MANY = 'tooMany';

	/**
	 * Minimum file size in bytes
	 * @var integer
	 */
	private $min = null;

	/**
	 * Maximum file size in bytes
	 * @var integer
	 */
	private $max = null;

	/**
	 * Files count
	 * @var int
	 */
	private $count = 0;

	/**
	 * Initialise the messages
	 *
	 * @since 0.10.0-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_TOO_FEW => 'Too few files uploaded.',
			self::INVALID_TOO_MANY   => 'Too many files uploaded.'
		];
	}

	/**
	 * Check whether min <= value <= max
	 *
	 * @param array|mixed $value
	 *
	 * @return boolean
	 *
	 * @since 0.10.0-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);
		$this->count = 0;
		$this->countFiles($value);

		if (null !== $this->min && $this->count < $this->min) {
			$this->setError($this->_messages[self::INVALID_TOO_FEW]);
			return false;
		}
		if (null !== $this->max && $this->count > $this->max) {
			$this->setError($this->_messages[self::INVALID_TOO_MANY]);
			return false;
		}

		return true;
	}

	/**
	 * Count the upoaded files
	 *
	 * @param array $value Files array
	 *
	 * @since 0.10.0-dev
	 */
	private function countFiles($value)
	{
		if (array_key_exists('tmp_name',$value)) {
			$this->count++;
		} else {
			foreach ($value as $file) {
				$this->countFiles($file);
			}
		}
	}

	/**
	 * Set the minimum number of files
	 *
	 * @param integer $min The minimum file size, in bytes
	 *
	 * @return \uMVC\Validator\File\Count Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setMin($min)
	{
		$this->min = intval($min);
		return $this;
	}

	/**
	 * Fetch the minimum file size
	 *
	 * @return int
	 *
	 * @since 0.10.0-dev
	 */
	public function getMin()
	{
		return $this->min;
	}

	/**
	 * Set the maximum file size
	 *
	 * @param integer $max The maximum file size, in bytes
	 *
	 * @return \uMVC\Validator\File\Size Provides a fluent interface
	 *
	 * @since 0.10.0-dev
	 */
	public function setMax($max)
	{
		$this->max = intval($max);
		return $this;
	}

	/**
	 * Fetch the maximum file size
	 *
	 * @return int
	 *
	 * @since 0.10.0-dev
	 */
	public function getMax()
	{
		return $this->max;
	}
}
