<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator\File;

/**
 * Validate the MIME type of the upoaded files
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.11.1-dev
 */
class Mime extends \uMVC\Validator {
	/**
	 * Wrong MIME type
	 */
	const INVALID_DISALLOWED_MIME = 'wrongMime';

	/**
	 * MIME types
	 * @var array
	 */
	private $mime = [];

	/**
	 * Initialise the messages
	 *
	 * @since 0.11.1-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_DISALLOWED_MIME => "Wrong MIME type."
		];
	}

	/**
	 * Check whether the files have the correct MIME type
	 *
	 * @param array|mixed $value
	 *
	 * @return boolean
	 *
	 * @since 0.11.1-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);

		foreach ($value as $file) {
			if (!in_array($file["type"], $this->getMime())) {
				$this->setError($this->_messages[self::INVALID_DISALLOWED_MIME]);
				return false;
			}
		}

		return true;
	}

	/**
	 * Set all MIME types, overwriting any previously set ones.
	 *
	 * @param array|string $mime array of all MIME types or a string containing one MIME type.
	 *
	 * @return \uMVC\Validator\File\Mime provides a fluent interface
	 *
	 * @since 0.11.1-dev
	 */
	public function setMime($mime)
	{
		$this->mime = [];
		return $this->addMime($mime);
	}

	/**
	 * Add one or multiple MIME types to the ones already defined
	 *
	 * @param array|string $mime array of multiple MIME types or a string containing one MIME type.
	 *
	 * @return \uMVC\Validator\File\Mime provides a fluent interface
	 *
	 * @since 0.11.1-dev
	 */
	public function addMime($mime)
	{
		if (!is_array($mime)) {
			$mime = [$mime];
		}
		foreach ($mime as $mimeType) {
			$this->mime[$mimeType] = $mimeType;
		}
		return $this;
	}

	/**
	 * Fetch all defined MIME types
	 *
	 * @return array array of MIME types
	 *
	 * @since 0.11.1-dev
	 */
	public function getMime()
	{
		return $this->mime;
	}
}
