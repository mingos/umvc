<?php
/* uMVC
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
namespace uMVC\Validator;

use uMVC\Validator;

/**
 * Validator used to make sure a string's length is contained within a range
 *
 * @package Validator
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 0.0.0-dev
 */
class StringLength extends Validator
{
	/**
	 * Value is too short
	 */
	const INVALID_TOO_SHORT = 'tooShort';

	/**
	 * Value is too long
	 */
	const INVALID_TOO_LONG = 'tooLong';

	/**
	 * Value is not a string
	 */
	const INVALID_NOT_STRING = 'notString';

	/**
	 * Minimum string length
	 * @var integer
	 */
	private $min = null;

	/**
	 * Maximum string length
	 * @var integer
	 */
	private $max = null;

	/**
	 * Encoding to use when cchecking string length
	 * @var string
	 */
	private $encoding = "UTF-8";

	/**
	 * Initialise the messages
	 *
	 * @since 0.0.0-dev
	 */
	protected function _init()
	{
		$this->_messages = [
			self::INVALID_TOO_SHORT  => 'The value is too short.',
			self::INVALID_TOO_LONG   => 'The value is too long.',
			self::INVALID_NOT_STRING => 'The value is not a string.'
		];
	}

	/**
	 * Check whether min <= value <= max
	 *
	 * @param mixed $value
	 *
	 * @return boolean
	 *
	 * @since 0.0.0-dev
	 */
	public function isValid($value)
	{
		$this->setError(null);
		if (!is_string($value)) {
			$this->setError($this->_messages[self::INVALID_NOT_STRING]);
			return false;
		} else {
			if (!is_null($this->min) && mb_strlen($value, $this->getEncoding()) < $this->min) {
				$this->setError($this->_messages[self::INVALID_TOO_SHORT]);
				return false;
			} else {
				if (!is_null($this->max) && mb_strlen($value, $this->getEncoding()) > $this->max) {
					$this->setError($this->_messages[self::INVALID_TOO_LONG]);
					return false;
				} else {
					return true;
				}
			}
		}
	}

	/**
	 * Set the minimum string length
	 *
	 * @param integer $min The minimum string length
	 *
	 * @return \uMVC\Validator\StringLength Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setMin($min)
	{
		$this->min = intval($min);
		return $this;
	}

	/**
	 * Fetch the minimum string length
	 *
	 * @return int
	 *
	 * @since 0.5.1-dev
	 */
	public function getMin()
	{
		return $this->min;
	}

	/**
	 * Set the maximum string length
	 *
	 * @param integer $max The maximum string length
	 *
	 * @return \uMVC\Validator\StringLength Provides a fluent interface
	 *
	 * @since 0.0.0-dev
	 */
	public function setMax($max)
	{
		$this->max = intval($max);
		return $this;
	}

	/**
	 * Fetch the maximum string length
	 *
	 * @return int
	 *
	 * @since 0.5.1-dev
	 */
	public function getMax()
	{
		return $this->max;
	}

	/**
	 * Set the character encoding
	 *
	 * @param string $encoding The character encoding
	 *
	 * @return \uMVC\Validator\StringLength Provides a fluent interface
	 *
	 * @since 0.16.0-dev
	 */
	public function setEncoding($encoding)
	{
		$this->encoding = $encoding;
		return $this;
	}

	/**
	 * Get the validator's character encoding
	 *
	 * @return string
	 *
	 * @since 0.16.0-dev
	 */
	public function getEncoding()
	{
		return $this->encoding;
	}
}
